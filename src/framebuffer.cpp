#include "framebuffer.h"

Framebuffer::Framebuffer()
{

}

GrResult Framebuffer::Init(std::shared_ptr<Instance> pInstance, VkExtent2D extent, VkRenderPass renderpass, std::vector<VkImageView>& attachments)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    VkFramebufferCreateInfo framebufferInfo{};
    framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferInfo.renderPass = renderpass;
    framebufferInfo.attachmentCount = attachments.size();
    framebufferInfo.pAttachments = attachments.data();
    framebufferInfo.width = extent.width;
    framebufferInfo.height = extent.height;
    framebufferInfo.layers = 1;

    if (vkCreateFramebuffer(m_pInstance->GetVkDevice(), &framebufferInfo, nullptr, &m_framebuffer) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create framebuffer!");
    }

    return result;
}

GrResult Framebuffer::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDestroyFramebuffer(m_pInstance->GetVkDevice(), m_framebuffer, nullptr);

    return result;
}

VkFramebuffer Framebuffer::GetVkFramebuffer()
{
    return m_framebuffer;
}
