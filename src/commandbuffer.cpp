#include "commandbuffer.h"

CommandBuffer::CommandBuffer()
{

}

GrResult CommandBuffer::Init(std::shared_ptr<Instance> pInstance, Queue queue)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = queue.GetVkCommandPool();
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;

    if (vkAllocateCommandBuffers(m_pInstance->GetVkDevice(), &allocInfo, &m_commandBuffer) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to allocate command buffers!");
    }

    return result;
}

GrResult CommandBuffer::Cleanup(Queue queue)
{
    GrResult result = GR_SUCCESS;
    vkFreeCommandBuffers(m_pInstance->GetVkDevice(), queue.GetVkCommandPool(), 1, &m_commandBuffer);
    return result;
}

GrResult CommandBuffer::Begin()
{
    GrResult result = GR_SUCCESS;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = 0; // Optional
    beginInfo.pInheritanceInfo = nullptr; // Optional

    if (vkBeginCommandBuffer(m_commandBuffer, &beginInfo) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to begin recording command buffer!");
    }

    return result;
}

GrResult CommandBuffer::End()
{
    GrResult result = GR_SUCCESS;

    if (vkEndCommandBuffer(m_commandBuffer) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to record command buffer!");
    }

    return result;
}

VkCommandBuffer CommandBuffer::GetVkCommandBuffer()
{
    return m_commandBuffer;
}
