#include "buffer.h"

Buffer::Buffer()
{

}

GrResult Buffer::Init(std::shared_ptr<Instance> pInstance,
    VkDeviceSize size,
    VkBufferUsageFlags bufferUsage,
    VkSharingMode sharingMode,
    VkDeviceSize minAllocationAlignment,
    VmaMemoryUsage memoryUsage,
    VmaAllocationCreateFlags flags)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    m_size = size;

    VkBufferCreateInfo bufferCreateInfo{};
    bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferCreateInfo.size = size;
    bufferCreateInfo.usage = bufferUsage;
    bufferCreateInfo.sharingMode = sharingMode;

    /*if (vkCreateBuffer(m_pInstance->GetVkDevice(), &bufferInfo, nullptr, &m_buffer) != VK_SUCCESS) {
        throw std::runtime_error("failed to create buffer!");
    }

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(m_pInstance->GetVkDevice(), m_buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = m_pInstance->FindMemoryType(memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(m_pInstance->GetVkDevice(), &allocInfo, nullptr, &m_bufferMemory) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate buffer memory!");
    }*/

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = memoryUsage;
    allocInfo.flags = flags;
    if (minAllocationAlignment == 0)
    {
        vmaCreateBuffer(m_pInstance->GetVmaAllocator(), &bufferCreateInfo, &allocInfo, &m_buffer, &m_allocation, nullptr);
    }
    else
    {
        vmaCreateBufferWithAlignment(m_pInstance->GetVmaAllocator(), &bufferCreateInfo, &allocInfo, minAllocationAlignment, &m_buffer, &m_allocation, nullptr);
    }

    //vkBindBufferMemory(m_pInstance->GetVkDevice(), m_buffer, m_bufferMemory, 0);

    return result;
}

GrResult Buffer::Update(const char* const newData, size_t offset, size_t size)
{
    GrResult result = GR_SUCCESS;

    void* mappedData;
    vmaMapMemory(m_pInstance->GetVmaAllocator(), m_allocation, &mappedData);
    memcpy((char*)mappedData + offset, newData, (size_t)size);
    vmaUnmapMemory(m_pInstance->GetVmaAllocator(), m_allocation);

    return result;
}

GrResult Buffer::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vmaDestroyBuffer(m_pInstance->GetVmaAllocator(), m_buffer, m_allocation);

    return result;
}

uint8_t* Buffer::Map()
{
    if (!m_persistent && !Mapped())
    {
        if (vmaMapMemory(m_pInstance->GetVmaAllocator(), m_allocation, reinterpret_cast<void**>(&m_mappedData)) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to map buffer!");
        }
        assert(m_mappedData);
    }
    return m_mappedData;
}

void Buffer::Unmap()
{
    if (!m_persistent && Mapped())
    {
        vmaUnmapMemory(m_pInstance->GetVmaAllocator(), m_allocation);
        m_mappedData = nullptr;
    }
    return;
}

void Buffer::Flush()
{
    vmaFlushAllocation(m_pInstance->GetVmaAllocator(), m_allocation, 0, m_size);
}
