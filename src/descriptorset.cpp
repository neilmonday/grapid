#include "descriptorset.h"

DescriptorSet::DescriptorSet()
{
}

GrResult DescriptorSet::Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    m_descriptorPool = descriptorPool;
    return result;
}

GrResult DescriptorSet::Cleanup()
{
    GrResult result = GR_SUCCESS;
    return result;
}

VkDescriptorSetLayout DescriptorSet::GetVkDescriptorSetLayout()
{
    return m_descriptorSetLayout;
}

VkDescriptorSet DescriptorSet::GetVkDescriptorSet()
{
    return m_descriptorSet;
}
