#include "instance.h"

Instance::Instance() 
{
    m_instance = VK_NULL_HANDLE;
    m_device = VK_NULL_HANDLE;
    m_physicalDevice = VK_NULL_HANDLE;
}

GrResult Instance::SetVkDevice(VkDevice device)
{
    GrResult result = GR_SUCCESS;

    assert(m_device == VK_NULL_HANDLE);
    m_device = device;

    return result;
}

GrResult Instance::SetVkInstance(VkInstance instance)
{
    GrResult result = GR_SUCCESS;

    assert(m_instance == VK_NULL_HANDLE);
    m_instance = instance;

    return result;
}

GrResult Instance::SetVkPhysicalDevice(VkPhysicalDevice physicalDevice)
{
    GrResult result = GR_SUCCESS;

    assert(m_physicalDevice == VK_NULL_HANDLE);
    m_physicalDevice = physicalDevice;

    return result;
}

GrResult Instance::SetVmaAllocator(VmaAllocator allocator)
{
    GrResult result = GR_SUCCESS;

    assert(m_allocator == VK_NULL_HANDLE);
    m_allocator = allocator;

    return result;
}

VkDevice Instance::GetVkDevice()
{
    assert(m_device != VK_NULL_HANDLE);
    return m_device;
}

VkInstance Instance::GetVkInstance()
{
    assert(m_instance != VK_NULL_HANDLE);
    return m_instance;
}

VkPhysicalDevice Instance::GetVkPhysicalDevice()
{
    assert(m_physicalDevice != VK_NULL_HANDLE);
    return m_physicalDevice;
}

VmaAllocator Instance::GetVmaAllocator()
{
    assert(m_allocator != VK_NULL_HANDLE);
    return m_allocator;
}

uint32_t Instance::FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(GetVkPhysicalDevice(), &memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    throw std::runtime_error("failed to find suitable memory type!");
}

EXPORT VkFormat Instance::FindSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(GetVkPhysicalDevice(), format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            return format;
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }

    throw std::runtime_error("failed to find supported format!");
}
EXPORT VkFormat Instance::FindDepthFormat() {
    return FindSupportedFormat(
        { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}