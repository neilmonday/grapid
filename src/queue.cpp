#include "queue.h"

Queue::Queue() :
    m_queue{ },
    m_commandPool{ }
{

}

GrResult Queue::Init(std::shared_ptr<Instance> pInstance, uint32_t queueFamilyIndex)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    vkGetDeviceQueue(m_pInstance->GetVkDevice(), queueFamilyIndex, 0, &m_queue);

    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndex;
    poolInfo.flags = 0; // Optional

    if (vkCreateCommandPool(m_pInstance->GetVkDevice(), &poolInfo, nullptr, &m_commandPool) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create command pool!");
    }

    return result;
}

GrResult Queue::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDestroyCommandPool(m_pInstance->GetVkDevice(), m_commandPool, nullptr);

    return result;
}

VkQueue Queue::GetVkQueue()
{
    return m_queue;
}

VkCommandPool Queue::GetVkCommandPool()
{
    return m_commandPool;
}
