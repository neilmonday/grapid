#include "graphicspipeline.h"

GraphicsPipeline::GraphicsPipeline() : 
    Pipeline()
{
}

GrResult GraphicsPipeline::Init(
    std::shared_ptr<Instance> pInstance, 
    Renderpass renderpass,
    VkExtent2D extent,
    Program* pProgram, 
    std::shared_ptr<std::vector<VkVertexInputAttributeDescription> > attributeDescriptions, 
    std::shared_ptr<std::vector<VkVertexInputBindingDescription> > bindingDescription,
    std::vector<std::shared_ptr<DescriptorSet> > descriptorSets,
    VkPrimitiveTopology topology)
{
    GrResult result = Pipeline::Init(pInstance);

    for (auto shader : pProgram->GetShaders()) 
    {
        VkPipelineShaderStageCreateInfo shaderStageCreateInfo{};
        shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCreateInfo.stage = shader.second.GetVkStage();
        shaderStageCreateInfo.module = shader.second.GetShaderModule();
        shaderStageCreateInfo.pName = "main";
        m_pipelineParameters.shaderStageCreateInfos.push_back(shaderStageCreateInfo);
    }

    m_pipelineParameters.renderpass = renderpass;

    if (bindingDescription)
    {
        m_pipelineParameters.bindingDescription = *bindingDescription;
    }

    if (attributeDescriptions)
    {
        m_pipelineParameters.attributeDescriptions = *attributeDescriptions;
    }

    m_pipelineParameters.vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    m_pipelineParameters.vertexInputInfo.vertexBindingDescriptionCount = m_pipelineParameters.bindingDescription.size();
    m_pipelineParameters.vertexInputInfo.vertexAttributeDescriptionCount = m_pipelineParameters.attributeDescriptions.size();
    m_pipelineParameters.vertexInputInfo.pVertexBindingDescriptions = m_pipelineParameters.bindingDescription.data();
    m_pipelineParameters.vertexInputInfo.pVertexAttributeDescriptions = m_pipelineParameters.attributeDescriptions.data();

    m_pipelineParameters.inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    m_pipelineParameters.inputAssembly.topology = topology;
    m_pipelineParameters.inputAssembly.primitiveRestartEnable = VK_FALSE;

    m_pipelineParameters.tessellation.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
    m_pipelineParameters.tessellation.patchControlPoints = 3;

    m_pipelineParameters.viewport.x = 0.0f;
    m_pipelineParameters.viewport.y = 0.0f;
    m_pipelineParameters.viewport.width = extent.width;
    m_pipelineParameters.viewport.height = extent.height;
    m_pipelineParameters.viewport.minDepth = 0.0f;
    m_pipelineParameters.viewport.maxDepth = 1.0f;

    m_pipelineParameters.scissor.offset = { 0, 0 };
    m_pipelineParameters.scissor.extent = extent;

    m_pipelineParameters.viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    m_pipelineParameters.viewportState.viewportCount = 1;
    m_pipelineParameters.viewportState.pViewports = &(m_pipelineParameters.viewport);
    m_pipelineParameters.viewportState.scissorCount = 1;
    m_pipelineParameters.viewportState.pScissors = &(m_pipelineParameters.scissor);

    m_pipelineParameters.rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    m_pipelineParameters.rasterizer.depthClampEnable = VK_FALSE;
    m_pipelineParameters.rasterizer.rasterizerDiscardEnable = VK_FALSE;
    m_pipelineParameters.rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    m_pipelineParameters.rasterizer.lineWidth = 1.0f;
    m_pipelineParameters.rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;// VK_CULL_MODE_FRONT_BIT;
    m_pipelineParameters.rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
    m_pipelineParameters.rasterizer.depthBiasEnable = VK_FALSE;
    m_pipelineParameters.rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    m_pipelineParameters.rasterizer.depthBiasClamp = 0.0f; // Optional
    m_pipelineParameters.rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    m_pipelineParameters.multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    m_pipelineParameters.multisampling.sampleShadingEnable = VK_FALSE;
    m_pipelineParameters.multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    m_pipelineParameters.multisampling.minSampleShading = 1.0f; // Optional
    m_pipelineParameters.multisampling.pSampleMask = nullptr; // Optional
    m_pipelineParameters.multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    m_pipelineParameters.multisampling.alphaToOneEnable = VK_FALSE; // Optional

    m_pipelineParameters.depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    m_pipelineParameters.depthStencil.depthTestEnable = VK_TRUE;
    m_pipelineParameters.depthStencil.depthWriteEnable = VK_TRUE;
    m_pipelineParameters.depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    m_pipelineParameters.depthStencil.depthBoundsTestEnable = VK_FALSE;
    m_pipelineParameters.depthStencil.stencilTestEnable = VK_FALSE;

    //if (blendEnable) {
    //    finalColor.rgb = (srcColorBlendFactor * newColor.rgb) < colorBlendOp > (dstColorBlendFactor * oldColor.rgb);
    //    finalColor.a = (srcAlphaBlendFactor * newColor.a) < alphaBlendOp > (dstAlphaBlendFactor * oldColor.a);
    //}
    //else {
    //    finalColor = newColor;
    //}
    //finalColor = finalColor & colorWriteMask;
    m_pipelineParameters.colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    m_pipelineParameters.colorBlendAttachment.blendEnable = VK_FALSE;
    m_pipelineParameters.colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    m_pipelineParameters.colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    m_pipelineParameters.colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    m_pipelineParameters.colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    m_pipelineParameters.colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    m_pipelineParameters.colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    /*
    //finalColor.rgb = newAlpha * newColor + (1 - newAlpha) * oldColor;
    //finalColor.a = newAlpha.a;
    colorBlendAttachment.blendEnable = VK_TRUE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;*/
    m_pipelineParameters.colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    m_pipelineParameters.colorBlending.logicOpEnable = VK_FALSE;
    m_pipelineParameters.colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
    m_pipelineParameters.colorBlending.attachmentCount = 1;
    m_pipelineParameters.colorBlending.pAttachments = &(m_pipelineParameters.colorBlendAttachment);
    m_pipelineParameters.colorBlending.blendConstants[0] = 0.0f; // Optional
    m_pipelineParameters.colorBlending.blendConstants[1] = 0.0f; // Optional
    m_pipelineParameters.colorBlending.blendConstants[2] = 0.0f; // Optional
    m_pipelineParameters.colorBlending.blendConstants[3] = 0.0f; // Optional

    std::vector<VkDynamicState> dynamicStates =
    {
        //VK_DYNAMIC_STATE_VIEWPORT,
        //VK_DYNAMIC_STATE_LINE_WIDTH//,
        //VK_DYNAMIC_STATE_VERTEX_INPUT_EXT
    };

    m_pipelineParameters.dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    m_pipelineParameters.dynamicState.dynamicStateCount = dynamicStates.size();
    m_pipelineParameters.dynamicState.pDynamicStates = dynamicStates.data();

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
    for (auto descriptorSet : descriptorSets)
    {
        descriptorSetLayouts.push_back(descriptorSet->GetVkDescriptorSetLayout());
    }

    m_pipelineParameters.pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    m_pipelineParameters.pipelineLayoutInfo.setLayoutCount = descriptorSetLayouts.size(); // Optional
    m_pipelineParameters.pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data(); // Optional
    m_pipelineParameters.pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    m_pipelineParameters.pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

    if (vkCreatePipelineLayout(m_pInstance->GetVkDevice(), &(m_pipelineParameters.pipelineLayoutInfo), nullptr, &m_pipelineLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create pipeline layout!");
    }

    return result;
}

GrResult GraphicsPipeline::Create()
{
    GrResult result = GR_SUCCESS;

    VkGraphicsPipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.pNext = nullptr;
    pipelineInfo.stageCount = m_pipelineParameters.shaderStageCreateInfos.size();
    pipelineInfo.pStages = m_pipelineParameters.shaderStageCreateInfos.data();
    pipelineInfo.pVertexInputState = &(m_pipelineParameters.vertexInputInfo);
    pipelineInfo.pInputAssemblyState = &(m_pipelineParameters.inputAssembly);
    pipelineInfo.pTessellationState = &(m_pipelineParameters.tessellation);
    pipelineInfo.pViewportState = &(m_pipelineParameters.viewportState);
    pipelineInfo.pRasterizationState = &(m_pipelineParameters.rasterizer);
    pipelineInfo.pMultisampleState = &(m_pipelineParameters.multisampling);
    pipelineInfo.pDepthStencilState = &(m_pipelineParameters.depthStencil); // Optional
    pipelineInfo.pColorBlendState = &(m_pipelineParameters.colorBlending);
    pipelineInfo.pDynamicState = &(m_pipelineParameters.dynamicState); // Optional
    pipelineInfo.layout = m_pipelineLayout;
    pipelineInfo.renderPass = m_pipelineParameters.renderpass.GetVkRenderPass();
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    if (vkCreateGraphicsPipelines(m_pInstance->GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &m_pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create graphics pipeline!");
    }

    return result;
}

GrResult GraphicsPipeline::Bind(CommandBuffer* pCommandBuffer)
{
    GrResult result = GR_SUCCESS;

    vkCmdBindPipeline(pCommandBuffer->GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline);

    return result;
}