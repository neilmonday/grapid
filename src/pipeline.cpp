#include "pipeline.h"

Pipeline::Pipeline()
{
}

GrResult Pipeline::Init(std::shared_ptr<Instance> pInstance)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;
    return result;
}

GrResult Pipeline::Cleanup()
{
    GrResult result = GR_SUCCESS;
    return result;
}

VkPipeline Pipeline::GetVkPipeline()
{
    return m_pipeline;
}

VkPipelineLayout Pipeline::GetVkPipelineLayout()
{
    return m_pipelineLayout;
}