#include "raytracingpipeline.h"

RayTracingPipeline::RayTracingPipeline() : Pipeline()
{
}

GrResult RayTracingPipeline::Init(
    std::shared_ptr<Instance> pInstance,
    Program* pProgram,
    std::vector<std::shared_ptr<DescriptorSet> > descriptorSets)
{
    GrResult result = Pipeline::Init(pInstance);

    // Get the ray tracing pipeline properties, which we'll need later on in the sample
    m_rayTracingPipelineProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;
    VkPhysicalDeviceProperties2 device_properties{};
    device_properties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    device_properties.pNext = &m_rayTracingPipelineProperties;
    vkGetPhysicalDeviceProperties2(m_pInstance->GetVkPhysicalDevice(), &device_properties);

    for (auto shader : pProgram->GetShaders()) 
    {
        VkPipelineShaderStageCreateInfo shaderStageCreateInfo{};
        shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCreateInfo.stage = shader.second.GetVkStage();
        shaderStageCreateInfo.module = shader.second.GetShaderModule();
        shaderStageCreateInfo.pName = "main";
        m_pipelineParameters.shaderStageCreateInfos.push_back(shaderStageCreateInfo);

        switch (shader.second.GetStage())
        {
            case GrShaderStage::GR_RAYGEN:
            {
                VkRayTracingShaderGroupCreateInfoKHR raygen_group_ci{};
                raygen_group_ci.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
                raygen_group_ci.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
                raygen_group_ci.generalShader = static_cast<uint32_t>(m_pipelineParameters.shaderStageCreateInfos.size()) - 1;
                raygen_group_ci.closestHitShader = VK_SHADER_UNUSED_KHR;
                raygen_group_ci.anyHitShader = VK_SHADER_UNUSED_KHR;
                raygen_group_ci.intersectionShader = VK_SHADER_UNUSED_KHR;
                m_pipelineParameters.m_shaderGroups.push_back(raygen_group_ci);
                break;
            }
            case GrShaderStage::GR_ANY_HIT:
            {
                throw std::runtime_error("Any hit shader not implemented!");
                break;
            }
            case GrShaderStage::GR_CLOSEST_HIT:
            {
                VkRayTracingShaderGroupCreateInfoKHR closes_hit_group_ci{};
                closes_hit_group_ci.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
                closes_hit_group_ci.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
                closes_hit_group_ci.generalShader = VK_SHADER_UNUSED_KHR;
                closes_hit_group_ci.closestHitShader = static_cast<uint32_t>(m_pipelineParameters.shaderStageCreateInfos.size()) - 1;
                closes_hit_group_ci.anyHitShader = VK_SHADER_UNUSED_KHR;
                closes_hit_group_ci.intersectionShader = VK_SHADER_UNUSED_KHR;
                m_pipelineParameters.m_shaderGroups.push_back(closes_hit_group_ci);
                break;
            }
            case GrShaderStage::GR_MISS:
            {
                VkRayTracingShaderGroupCreateInfoKHR miss_group_ci{};
                miss_group_ci.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
                miss_group_ci.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
                miss_group_ci.generalShader = static_cast<uint32_t>(m_pipelineParameters.shaderStageCreateInfos.size()) - 1;
                miss_group_ci.closestHitShader = VK_SHADER_UNUSED_KHR;
                miss_group_ci.anyHitShader = VK_SHADER_UNUSED_KHR;
                miss_group_ci.intersectionShader = VK_SHADER_UNUSED_KHR;
                m_pipelineParameters.m_shaderGroups.push_back(miss_group_ci);
                break;
            }
            case GrShaderStage::GR_INTERSECTION:
            {
                throw std::runtime_error("Intersection shader not implemented!");
                break;
            }
            case GrShaderStage::GR_CALLABLE:
            {
                throw std::runtime_error("Callable shader not implemented!");
                break;
            }
            default:
            {
                throw std::runtime_error("Unknown shader stage!");
                break;
            }
        }
    }

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
    for (auto descriptorSet : descriptorSets)
    {
        descriptorSetLayouts.push_back(descriptorSet->GetVkDescriptorSetLayout());
    }

    m_pipelineParameters.pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    m_pipelineParameters.pipelineLayoutInfo.setLayoutCount = descriptorSetLayouts.size(); // Optional
    m_pipelineParameters.pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data(); // Optional
    m_pipelineParameters.pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    m_pipelineParameters.pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

    if (vkCreatePipelineLayout(m_pInstance->GetVkDevice(), &(m_pipelineParameters.pipelineLayoutInfo), nullptr, &m_pipelineLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create pipeline layout!");
    }

    return result;
}

GrResult RayTracingPipeline::Create()
{
    GrResult result = GR_SUCCESS;

    VkRayTracingPipelineCreateInfoKHR pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
    pipelineInfo.stageCount = m_pipelineParameters.shaderStageCreateInfos.size();
    pipelineInfo.pStages = m_pipelineParameters.shaderStageCreateInfos.data();
    pipelineInfo.groupCount = static_cast<uint32_t>(m_pipelineParameters.m_shaderGroups.size());
    pipelineInfo.pGroups = m_pipelineParameters.m_shaderGroups.data();
    pipelineInfo.maxPipelineRayRecursionDepth = 1;
    pipelineInfo.layout = m_pipelineLayout;

    if (vkCreateRayTracingPipelinesKHR(m_pInstance->GetVkDevice(), VK_NULL_HANDLE, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &m_pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create graphics pipeline!");
    }

    const uint32_t           handle_size = m_rayTracingPipelineProperties.shaderGroupHandleSize;
    const uint32_t           handle_size_aligned = aligned_size(m_rayTracingPipelineProperties.shaderGroupHandleSize, m_rayTracingPipelineProperties.shaderGroupHandleAlignment);
    const uint32_t           handle_alignment = m_rayTracingPipelineProperties.shaderGroupHandleAlignment;
    const uint32_t           group_count = static_cast<uint32_t>(m_pipelineParameters.m_shaderGroups.size());
    const uint32_t           sbt_size = group_count * handle_size_aligned;
    const VkBufferUsageFlags sbt_buffer_usage_flags = VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    const VmaMemoryUsage     sbt_memory_usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

    // Raygen
    // Create binding table buffers for each shader type
    m_raygenShaderBindingTable = std::make_unique<Buffer>();
    m_missShaderBindingTable = std::make_unique<Buffer>();
    m_hitShaderBindingTable = std::make_unique<Buffer>();

    m_raygenShaderBindingTable->Init(m_pInstance, handle_size, sbt_buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, m_rayTracingPipelineProperties.shaderGroupBaseAlignment, sbt_memory_usage);
    m_missShaderBindingTable->Init(m_pInstance, handle_size, sbt_buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, m_rayTracingPipelineProperties.shaderGroupBaseAlignment, sbt_memory_usage);
    m_hitShaderBindingTable->Init(m_pInstance, handle_size, sbt_buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, m_rayTracingPipelineProperties.shaderGroupBaseAlignment, sbt_memory_usage);

    // Copy the pipeline's shader handles into a host buffer
    std::vector<uint8_t> shader_handle_storage(sbt_size);
    if (vkGetRayTracingShaderGroupHandlesKHR(m_pInstance->GetVkDevice(), m_pipeline, 0, group_count, sbt_size, shader_handle_storage.data()) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to get ray tracing shader group handles!");
    }

    // Copy the shader handles from the host buffer to the binding tables
    uint8_t* data = static_cast<uint8_t*>(m_raygenShaderBindingTable->Map());
    memcpy(data, shader_handle_storage.data(), handle_size);
    data = static_cast<uint8_t*>(m_missShaderBindingTable->Map());
    memcpy(data, shader_handle_storage.data() + handle_size_aligned, handle_size);
    data = static_cast<uint8_t*>(m_hitShaderBindingTable->Map());
    memcpy(data, shader_handle_storage.data() + handle_size_aligned * 2, handle_size);
    m_raygenShaderBindingTable->Unmap();
    m_missShaderBindingTable->Unmap();
    m_hitShaderBindingTable->Unmap();

    return result;
}

GrResult RayTracingPipeline::Bind(CommandBuffer* pCommandBuffer)
{
    GrResult result = GR_SUCCESS;

    vkCmdBindPipeline(pCommandBuffer->GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, m_pipeline);

    return result;
}

GrResult RayTracingPipeline::GetShaderBindingTableEntries(
    VkStridedDeviceAddressRegionKHR& raygen_shader_sbt_entry,
    VkStridedDeviceAddressRegionKHR& miss_shader_sbt_entry,
    VkStridedDeviceAddressRegionKHR& hit_shader_sbt_entry,
    VkStridedDeviceAddressRegionKHR& callable_shader_sbt_entry)
{
    GrResult result = GR_SUCCESS;

    const uint32_t handle_size_aligned = aligned_size(m_rayTracingPipelineProperties.shaderGroupHandleSize, m_rayTracingPipelineProperties.shaderGroupHandleAlignment);

    //Reusing this might be sketchy.
    VkBufferDeviceAddressInfoKHR buffer_device_address_info{};
    buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;

    buffer_device_address_info.buffer = m_raygenShaderBindingTable->GetVkBuffer();
    raygen_shader_sbt_entry.deviceAddress = vkGetBufferDeviceAddressKHR(m_pInstance->GetVkDevice(), &buffer_device_address_info);
    raygen_shader_sbt_entry.stride = handle_size_aligned;
    raygen_shader_sbt_entry.size = handle_size_aligned;

    buffer_device_address_info.buffer = m_raygenShaderBindingTable->GetVkBuffer();
    miss_shader_sbt_entry.deviceAddress = vkGetBufferDeviceAddressKHR(m_pInstance->GetVkDevice(), &buffer_device_address_info);
    miss_shader_sbt_entry.stride = handle_size_aligned;
    miss_shader_sbt_entry.size = handle_size_aligned;

    buffer_device_address_info.buffer = m_raygenShaderBindingTable->GetVkBuffer();
    hit_shader_sbt_entry.deviceAddress = vkGetBufferDeviceAddressKHR(m_pInstance->GetVkDevice(), &buffer_device_address_info);
    hit_shader_sbt_entry.stride = handle_size_aligned;
    hit_shader_sbt_entry.size = handle_size_aligned;

    return result;
}
