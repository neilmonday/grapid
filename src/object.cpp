#include "object.h"

Object::Object()
{

}

GrResult Object::Import(const std::string& file)
{
    GrResult result = GR_SUCCESS;

    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile(file,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_SortByPType);

    if (nullptr == scene)
    {
        std::cout << importer.GetErrorString() << std::endl;
        result = GR_ERROR;
        return result;
    }
    aiMesh* mesh = nullptr;
    if (scene->mNumMeshes > 0)
    {
        mesh = scene->mMeshes[0];
    }

    if(mesh != nullptr && mesh->mNumVertices > 0)
    {
        for (uint32_t i = 0; i< mesh->mNumVertices; i++)
        {
            aiVector3D vertex = mesh->mVertices[i];
            glm::vec3 glm_vertex = {};
            glm_vertex.x = vertex.x;
            glm_vertex.y = vertex.y;
            glm_vertex.z = vertex.z;
            vertices.push_back(glm_vertex);
        }
    }

    // We're done. Everything will be cleaned up by the importer destructor
    return result;

}

std::vector<glm::vec3> Object::GetVertices()
{
    return vertices;
}
