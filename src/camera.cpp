#include "camera.h"

Camera::Camera()
{

}

GrResult Camera::Init(GLFWwindow* window)
{
    GrResult result = GR_SUCCESS;
    m_window = window;

    m_pUniformBufferObject = std::make_shared<UniformBufferObject>();

    glfwGetCursorPos(m_window, &(m_initialCursor.x), &(m_initialCursor.y));

    return result;
}

GrResult Camera::Update()
{
    GrResult result = GR_SUCCESS;
    glm::vec3 right = glm::cross(m_up, m_forward);
    glm::vec2 rotation = CalculateRotation();
    glm::mat4 rotation_matrix = glm::rotate(glm::identity<glm::mat4>(), rotation.y, m_up);
    rotation_matrix = glm::rotate(rotation_matrix, rotation.x, right);
    glm::vec4 forward4{ m_forward.x, m_forward.y, m_forward.z, 1.0f };
    forward4 = rotation_matrix * forward4;
    m_forward = glm::vec3(forward4.x, forward4.y, forward4.z);
    CalculateTranslation(rotation, &m_position);

    //0.785398 radians == 45 degrees.
    //1.5708 radians == 90 degrees.
    m_pUniformBufferObject->projection = glm::perspective(1.5708f, m_aspectRatio, 0.05f, 10000.0f);
    glm::vec3 position_plus_forward = { m_position.x + m_forward.x, m_position.y + m_forward.y, m_position.z + m_forward.z };
    glm::vec3 position_minus_forward = { m_position.x - m_forward.x, m_position.y - m_forward.y, m_position.z - m_forward.z };
    m_pUniformBufferObject->view = glm::lookAt(m_position, position_plus_forward, m_up);
    m_pUniformBufferObject->model = glm::identity<glm::mat4>();
    /*example code does model * view * projection in the shader
    glm::mat4 model_view = model * view;
    glm::mat4 model_view_projection = model_view * projection;*/
    m_pUniformBufferObject->cameraPosition = glm::vec4(m_position.x, m_position.y, m_position.z, 1.0f);
    m_pUniformBufferObject->projectionInverse = glm::inverse(m_pUniformBufferObject->projection);
    m_pUniformBufferObject->viewInverse = glm::inverse(m_pUniformBufferObject->view);
    return result;
}

GrResult Camera::Cleanup()
{
    GrResult result = GR_SUCCESS;
    return result;
}

glm::vec2 Camera::CalculateRotation()
{
    glm::dvec2 cursor = { 0.0, 0.0 };
    glm::vec2 rotation = { 0.0f, 0.0f };

    glfwGetCursorPos(m_window, &(cursor.x), &(cursor.y));

    cursor.x -= m_initialCursor.x;
    cursor.y -= m_initialCursor.y;

    rotation.y = -cursor.x / 300.0;
    rotation.x = -cursor.y / 300.0;

    if (rotation.x > 1.570)
        rotation.x = 1.570;
    if (rotation.x < -1.570)
        rotation.x = -1.570;
    //printf("*****************\n");
    //printf("Mouse Up:\n");
    //printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
    //printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
    //printf("*****************\n");

    return rotation;
}

GrResult Camera::CalculateTranslation(const glm::vec2 rotation, glm::vec3* position)
{
    GrResult result = GR_SUCCESS;

    GLdouble sensitivity = 0.2;
    if (glfwGetKey(m_window, GLFW_KEY_S) == GLFW_PRESS)
    {
        position->x += sensitivity * sin(rotation.y) * cos(rotation.x);
        position->y += sensitivity * 1 * -sin(rotation.x);
        position->z += sensitivity * -cos(rotation.y) * cos(rotation.x);
    }
    if (glfwGetKey(m_window, GLFW_KEY_A) == GLFW_PRESS)
    {
        position->x -= sensitivity * cos(rotation.y);
        //*y -=;
        position->z -= sensitivity * sin(rotation.y);
    }
    if (glfwGetKey(m_window, GLFW_KEY_W) == GLFW_PRESS)
    {
        position->x -= sensitivity * sin(rotation.y) * cos(rotation.x);
        position->y -= sensitivity * 1 * -sin(rotation.x);
        position->z -= sensitivity * -cos(rotation.y) * cos(rotation.x);
    }
    if (glfwGetKey(m_window, GLFW_KEY_D) == GLFW_PRESS)
    {
        position->x += sensitivity * cos(rotation.y);
        //*y += ;
        position->z += sensitivity * sin(rotation.y);
    }
    //printf("*****************\n");
    //printf("Position: %f, %f, %f\n", position->x, position->y, position->z);
    //printf("*****************\n");

    return result;
}
