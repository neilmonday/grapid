#include "swapchain.h"

GrResult Swapchain::Init(std::shared_ptr<Instance> pInstance, VkExtent2D windowSize, VkSurfaceKHR surface)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_pInstance->GetVkPhysicalDevice(), surface, &capabilities);

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(m_pInstance->GetVkPhysicalDevice(), surface, &formatCount, nullptr);
    assert(formatCount > 0);
    std::vector<VkSurfaceFormatKHR> formats(formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(m_pInstance->GetVkPhysicalDevice(), surface, &formatCount, formats.data());

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(m_pInstance->GetVkPhysicalDevice(), surface, &presentModeCount, nullptr);
    assert(presentModeCount > 0);
    std::vector<VkPresentModeKHR> presentModes(presentModeCount);
    vkGetPhysicalDeviceSurfacePresentModesKHR(m_pInstance->GetVkPhysicalDevice(), surface, &presentModeCount, presentModes.data());

    VkSurfaceFormatKHR chosenFormat = formats[0];
    for (const auto& availableFormat : formats)
    {
        if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            chosenFormat = availableFormat;
            break;
        }
    }

    VkPresentModeKHR chosenPresentMode = VK_PRESENT_MODE_FIFO_KHR;
    for (const auto& presentMode : presentModes)
    {
        if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            chosenPresentMode = presentMode;
            break;
        }
    }

    VkExtent2D chosenExtent;
    if (capabilities.currentExtent.width != UINT32_MAX)
    {
        chosenExtent = capabilities.currentExtent;
    }
    else
    {
        //something about the includes in GLFW/VOLK/VMA make min/max macros, as opposed to the std::min and std::max
        windowSize.width = max(capabilities.minImageExtent.width, min(capabilities.maxImageExtent.width, windowSize.width));
        windowSize.height = max(capabilities.minImageExtent.height, min(capabilities.maxImageExtent.height, windowSize.height));

        chosenExtent = windowSize;
    }

    uint32_t imageCount = capabilities.minImageCount + 1;
    if (capabilities.maxImageCount > 0 && imageCount > capabilities.maxImageCount) {
        imageCount = capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = chosenFormat.format;
    createInfo.imageColorSpace = chosenFormat.colorSpace;
    createInfo.imageExtent = chosenExtent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

    /* If we want to have separate graphics and present queue families, we can enable this if, for now we are just using 1 for graphics and present, so use the else
    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }
    else {*/
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.queueFamilyIndexCount = 0; // Optional
    createInfo.pQueueFamilyIndices = nullptr; // Optional
//}
    createInfo.preTransform = capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = chosenPresentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(m_pInstance->GetVkDevice(), &createInfo, nullptr, &m_swapchainKHR) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create swap chain!");
    }

    //save the swapchain images
    vkGetSwapchainImagesKHR(m_pInstance->GetVkDevice(), m_swapchainKHR, &imageCount, nullptr);
    std::vector<VkImage> vkImages(imageCount);
    vkGetSwapchainImagesKHR(m_pInstance->GetVkDevice(), m_swapchainKHR, &imageCount, vkImages.data());

    m_images.resize(imageCount);
    for (int i = 0; i<imageCount; i++)
    {
        result = m_images[i].Import(m_pInstance, vkImages[i], chosenFormat.format, VK_IMAGE_ASPECT_COLOR_BIT);
    }

    m_imageViews.resize(imageCount);
    for (size_t i = 0; i < m_imageViews.size(); i++)
    {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = m_images[i].GetVkImage();
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = chosenFormat.format;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        if (vkCreateImageView(m_pInstance->GetVkDevice(), &createInfo, nullptr, &m_imageViews[i]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to create image views!");
        }
    }

    //save the swapchain format and extent
    m_imageFormat = chosenFormat.format;
    m_extent = chosenExtent;

    return result;
}

GrResult Swapchain::Cleanup()
{
    GrResult result = GR_SUCCESS;

    for (auto imageView : m_imageViews)
    {
        vkDestroyImageView(m_pInstance->GetVkDevice(), imageView, nullptr);
    }
    vkDestroySwapchainKHR(m_pInstance->GetVkDevice(), m_swapchainKHR, nullptr);

    return result;
}

size_t Swapchain::GetImageViewCount()
{
    return m_imageViews.size();
}

Image Swapchain::GetImage(uint32_t i)
{
    return m_images[i];
}

VkImageView Swapchain::GetVkImageView(uint32_t i)
{
    return m_imageViews[i];
}

VkFormat Swapchain::GetVkFormat()
{
    return m_imageFormat;
}

VkExtent2D Swapchain::GetVkExtent2D()
{
    return m_extent;
}

VkSwapchainKHR Swapchain::GetVkSwapchain()
{
    return m_swapchainKHR;
}
