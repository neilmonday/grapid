#include "application.h"

Application::Application()
{
}

Application::Application(std::string name, uint32_t apiVersion) :
    m_name(name),
    m_apiVersion(apiVersion),
    m_windowSize{ 1280, 720 },
    m_window(nullptr)
{
    //Instance Extensions
#ifdef _DEBUG
    m_requiredInstanceExtensionNames.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    m_requiredInstanceExtensionNames.push_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);

    //Device Extensions
    m_requiredDeviceExtensionNames.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

    //Layers
#ifdef _DEBUG
    m_requiredLayerNames.push_back("VK_LAYER_KHRONOS_validation");
#endif

    PopulateDebugMessengerCreateInfo();
}

GrResult Application::Init()
{
    m_pInstance = std::make_shared<Instance>();
    GrResult result = GR_SUCCESS;


    result = InitDependencies();
    result = CreateOSWindow();
    result = CreateInstance();
    result = CreateDebugUtilsMessenger();
    result = CreateSurface();
    result = LoadVolkInstance();
    result = PickPhysicalDevice();
    result = CreateLogicalDevice();
    result = LoadVolkDevice();
    result = CreateAllocator();
    result = CreateSwapchain();
    result = CreateSemaphores();

    return result;
}

GrResult Application::Cleanup()
{
    GrResult result = GR_SUCCESS;

    DestroySemaphores();
    DestroySwapchain();
    DestroyAllocator();
    DestroyLogicalDevice();
    DestroySurface();
    DestroyDebugUtilsMessenger();
    DestroyInstance();
    DestroyWindow();

    return result;
}

void Application::PopulateDebugMessengerCreateInfo()
{
    m_debugCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    m_debugCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    m_debugCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    m_debugCreateInfo.pfnUserCallback = DebugCallback;
    m_debugCreateInfo.pUserData = nullptr; // Optional
}

Swapchain Application::GetSwapchain()
{
    return m_swapchain;
}

GrResult Application::InitDependencies()
{
    GrResult result = GR_SUCCESS;

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult Application::CreateOSWindow()
{
    GrResult result = GR_SUCCESS;

    //For Vulkan, no need to create context, so set GLFW_NO_API
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    //Allow the user to set this
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    glfwWindowHint(GLFW_RED_BITS, 10);
    glfwWindowHint(GLFW_GREEN_BITS, 10);
    glfwWindowHint(GLFW_BLUE_BITS, 10);

    // Create a fullscreen mode window and its OpenGL context
    //m_window = glfwCreateWindow(m_windowSize.width, m_windowSize.height, m_name.c_str(), glfwGetPrimaryMonitor(), NULL);
    // Create a windowed mode window and its OpenGL context 
    m_window = glfwCreateWindow(m_windowSize.width, m_windowSize.height, m_name.c_str(), NULL, NULL);
    if (!m_window)
    {
        glfwTerminate();
        result = GR_ERROR;
        return result;
    }

    //glfwSetWindowSizeCallback(window, &window_size_callback);

    // Make the window's context current
    glfwMakeContextCurrent(m_window);

    return result;
}

void Application::DestroyWindow()
{
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

GrResult Application::CreateInstance()
{
    GrResult result = GR_SUCCESS;

    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = m_name.c_str();
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = m_apiVersion;

    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    for (auto extension : m_requiredInstanceExtensionNames)
    {
        extensions.push_back(extension);
    }

    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, NULL);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (const char* requiredLayerName : m_requiredLayerNames)
    {
        for (const auto& availableLayer : availableLayers)
        {
            if (strcmp(requiredLayerName, availableLayer.layerName) == 0)
            {
                m_enabledLayerNames.push_back(requiredLayerName);
            }
        }
    }

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();
    createInfo.enabledLayerCount = static_cast<uint32_t>(m_enabledLayerNames.size());
    createInfo.ppEnabledLayerNames = m_enabledLayerNames.data();

#ifdef _DEBUG
    createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&m_debugCreateInfo;
#endif

    VkInstance instance{};
    if (vkCreateInstance(&createInfo, NULL, &instance) != VK_SUCCESS)
    {
        throw std::runtime_error("vkCreateInstance has failed!");
    }
    else
    {
        m_pInstance->SetVkInstance(instance);
    }

    return result;
}

void Application::DestroyInstance()
{
    vkDestroyInstance(m_pInstance->GetVkInstance(), nullptr);
}

GrResult Application::CreateDebugUtilsMessenger()
{
    GrResult result = GR_SUCCESS;

    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_pInstance->GetVkInstance(), "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr)
    {
        if (func(m_pInstance->GetVkInstance(), &m_debugCreateInfo, nullptr, &m_debugMessenger) != VK_SUCCESS)
        {
            throw std::runtime_error("CreateDebugUtilsMessenger has failed!");
        }
    }
    else
    {
        result = GR_ERROR;
    }

    return result;
}

void Application::DestroyDebugUtilsMessenger()
{
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_pInstance->GetVkInstance(), "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr)
    {
        func(m_pInstance->GetVkInstance(), m_debugMessenger, nullptr);
    }
}

GrResult Application::CreateSurface()
{
    GrResult result = GR_SUCCESS;

    if (glfwCreateWindowSurface(m_pInstance->GetVkInstance(), m_window, nullptr, &m_surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface!");
        result = GR_ERROR;
    }

    return result;
}

void Application::DestroySurface()
{
    vkDestroySurfaceKHR(m_pInstance->GetVkInstance(), m_surface, nullptr); //created with glfw, not directly with vulkan
}

GrResult Application::LoadVolkInstance()
{
    GrResult result = GR_SUCCESS;
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult Application::PickPhysicalDevice()
{
    GrResult result = GR_SUCCESS;

    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(m_pInstance->GetVkInstance(), &deviceCount, nullptr);

    if (deviceCount == 0)
    {
        throw std::runtime_error("failed to find GPUs with Vulkan support!");
    }

    std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
    vkEnumeratePhysicalDevices(m_pInstance->GetVkInstance(), &deviceCount, physicalDevices.data());

    // Use an ordered map to automatically sort candidates by increasing score
    std::multimap<int, VkPhysicalDevice> candidates;

    for (const auto& physicalDevice : physicalDevices)
    {
        int score = RateDeviceSuitability(physicalDevice);
        candidates.insert(std::make_pair(score, physicalDevice));
    }

    // Check if the best candidate is suitable at all
    if (candidates.rbegin()->first > 0)
    {
        m_pInstance->SetVkPhysicalDevice(candidates.rbegin()->second);
    }
    else
    {
        throw std::runtime_error("failed to find a suitable GPU!");
    }

    return result;
}

int Application::RateDeviceSuitability(VkPhysicalDevice physicalDevice)
{
    int score = 0;

    //check if device supports required extensions.
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, nullptr);

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, availableExtensions.data());

    std::set<std::string> requiredExtensions(m_requiredDeviceExtensionNames.begin(), m_requiredDeviceExtensionNames.end());

    for (const auto& extension : availableExtensions) {
        requiredExtensions.erase(extension.extensionName);
    }

    if(!requiredExtensions.empty())
    {
        return 0;
    }

    VkPhysicalDeviceProperties deviceProperties{};
    vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);

    VkPhysicalDeviceFeatures deviceFeatures{};
    vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);

    //This is just example logic. Probably allow user to override this
    // Discrete GPUs have a significant performance advantage
    if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
        score += 1000;
    }

    // Maximum possible size of textures affects graphics quality
    score += deviceProperties.limits.maxImageDimension2D;

    return score;
}

GrResult Application::CreateLogicalDevice()
{
    GrResult result = GR_SUCCESS;
    float queuePriority = 1.0f;
    std::vector<uint32_t> queueFamilyIndices = GetQueueFamilyIndices();

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = queueFamilyIndices[0];
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = nullptr;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(m_requiredDeviceExtensionNames.size());
    createInfo.ppEnabledExtensionNames = m_requiredDeviceExtensionNames.data();

    VkDevice device{};
    if (vkCreateDevice(m_pInstance->GetVkPhysicalDevice(), &createInfo, nullptr, &device) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create logical device!");
    }
    else
    {
        m_pInstance->SetVkDevice(device);
    }

    return result;
}

GrResult Application::LoadVolkDevice()
{
    GrResult result = GR_SUCCESS;
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

void Application::DestroyLogicalDevice()
{
    vkDestroyDevice(m_pInstance->GetVkDevice(), nullptr);
}

GrResult Application::CreateAllocator()
{
    GrResult result = GR_SUCCESS;

    VmaAllocatorCreateInfo allocatorCreateInfo = {};

    allocatorCreateInfo.physicalDevice = m_pInstance->GetVkPhysicalDevice();
    allocatorCreateInfo.device = m_pInstance->GetVkDevice();
    allocatorCreateInfo.instance = m_pInstance->GetVkInstance();
    allocatorCreateInfo.vulkanApiVersion = m_apiVersion;

    if (VK_KHR_dedicated_allocation_enabled)
    {
        allocatorCreateInfo.flags |= VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT;
    }
    if (VK_KHR_bind_memory2_enabled)
    {
        allocatorCreateInfo.flags |= VMA_ALLOCATOR_CREATE_KHR_BIND_MEMORY2_BIT;
    }
#if !defined(VMA_MEMORY_BUDGET) || VMA_MEMORY_BUDGET == 1
    if (VK_EXT_memory_budget_enabled && (
        m_apiVersion >= VK_API_VERSION_1_1 || VK_KHR_get_physical_device_properties2_enabled))
    {
        allocatorCreateInfo.flags |= VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;
    }
#endif
    if (VK_AMD_device_coherent_memory_enabled)
    {
        allocatorCreateInfo.flags |= VMA_ALLOCATOR_CREATE_AMD_DEVICE_COHERENT_MEMORY_BIT;
    }
    if (VK_KHR_buffer_device_address_enabled)
    {
        allocatorCreateInfo.flags |= VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    }
#if !defined(VMA_MEMORY_PRIORITY) || VMA_MEMORY_PRIORITY == 1
    if (VK_EXT_memory_priority_enabled)
    {
        allocatorCreateInfo.flags |= VMA_ALLOCATOR_CREATE_EXT_MEMORY_PRIORITY_BIT;
    }
#endif

    /*if (USE_CUSTOM_CPU_ALLOCATION_CALLBACKS)
    {
        allocatorCreateInfo.pAllocationCallbacks = &g_CpuAllocationCallbacks;
    }*/

#if VMA_DYNAMIC_VULKAN_FUNCTIONS
    static VmaVulkanFunctions vulkanFunctions = {};
    vulkanFunctions.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
    vulkanFunctions.vkGetDeviceProcAddr = vkGetDeviceProcAddr;
    allocatorCreateInfo.pVulkanFunctions = &vulkanFunctions;
#endif

// Uncomment to enable recording to CSV file.
/*
static VmaRecordSettings recordSettings = {};
recordSettings.pFilePath = "VulkanSample.csv";
outInfo.pRecordSettings = &recordSettings;
*/

// Uncomment to enable HeapSizeLimit.
/*
static std::array<VkDeviceSize, VK_MAX_MEMORY_HEAPS> heapSizeLimit;
std::fill(heapSizeLimit.begin(), heapSizeLimit.end(), VK_WHOLE_SIZE);
heapSizeLimit[0] = 512ull * 1024 * 1024;
outInfo.pHeapSizeLimit = heapSizeLimit.data();
*/

    VmaAllocator allocator{};
    vmaCreateAllocator(&allocatorCreateInfo, &allocator);

    m_pInstance->SetVmaAllocator(allocator);

    return result;
}

void Application::DestroyAllocator()
{
    vmaDestroyAllocator(m_pInstance->GetVmaAllocator());
}

GrResult Application::CreateSwapchain()
{
    GrResult result = GR_SUCCESS;
 
    m_swapchain.Init(m_pInstance, m_windowSize, m_surface);

    return result;
}

void Application::DestroySwapchain()
{
    m_swapchain.Cleanup();
}

std::vector<uint32_t> Application::GetQueueFamilyIndices()
{
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(m_pInstance->GetVkPhysicalDevice(), &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(m_pInstance->GetVkPhysicalDevice(), &queueFamilyCount, queueFamilyProperties.data());

    std::vector<uint32_t> queueFamilies;
    uint32_t currentQueueFamilyIndex = 0;
    for (const auto& queueFamily : queueFamilyProperties)
    {
        if (queueFamily.queueFlags & (VK_QUEUE_GRAPHICS_BIT))
        {
            //Its not necessarily required that the graphics queue has present capability, but for us, we will check to make sure the queue supports both
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(m_pInstance->GetVkPhysicalDevice(), currentQueueFamilyIndex, m_surface, &presentSupport);
            if (presentSupport)
            {
                queueFamilies.push_back(currentQueueFamilyIndex);
            }
        }
        currentQueueFamilyIndex++;
    }
    return queueFamilies;
}

GrResult Application::CreateSemaphores()
{
    GrResult result = GR_SUCCESS;

    m_imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    m_renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    m_inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    m_inFlightImages.resize(m_swapchain.GetImageViewCount(), VK_NULL_HANDLE);

    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(m_pInstance->GetVkDevice(), &semaphoreInfo, nullptr, &m_imageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(m_pInstance->GetVkDevice(), &semaphoreInfo, nullptr, &m_renderFinishedSemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(m_pInstance->GetVkDevice(), &fenceInfo, nullptr, &m_inFlightFences[i]) != VK_SUCCESS) {

            throw std::runtime_error("failed to create semaphores for a frame!");
        }
    }

    return result;
}

void Application::DestroySemaphores()
{
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(m_pInstance->GetVkDevice(), m_renderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(m_pInstance->GetVkDevice(), m_imageAvailableSemaphores[i], nullptr);
        vkDestroyFence(m_pInstance->GetVkDevice(), m_inFlightFences[i], nullptr);
    }
}

GrResult Application::BindVertexBuffers(CommandBuffer* pCommandBuffer, std::vector<std::shared_ptr<Buffer>>& vertexBuffers, std::vector<VkDeviceSize>& offsets)
{
    GrResult result = GR_SUCCESS;

    std::vector<VkBuffer> vkBuffers{};
    for (auto vertexBuffer : vertexBuffers)
    {
        vkBuffers.push_back(vertexBuffer->GetVkBuffer());
    }

    vkCmdBindVertexBuffers(pCommandBuffer->GetVkCommandBuffer(), 0, vkBuffers.size(), vkBuffers.data(), offsets.data());

    return result;
}

GrResult Application::BindVertexBuffer(CommandBuffer* pCommandBuffer, Buffer& vertexBuffer, VkDeviceSize offset)
{
    GrResult result = GR_SUCCESS;
    VkBuffer vkBuffer = vertexBuffer.GetVkBuffer();

    vkCmdBindVertexBuffers(pCommandBuffer->GetVkCommandBuffer(), 0, 1, &vkBuffer, &offset);

    return result;
}

GrResult Application::BindIndexBuffer(CommandBuffer* pCommandBuffer, Buffer& indexBuffer, VkDeviceSize offset)
{
    GrResult result = GR_SUCCESS;

    vkCmdBindIndexBuffer(pCommandBuffer->GetVkCommandBuffer(), indexBuffer.GetVkBuffer(), offset, VK_INDEX_TYPE_UINT32);

    return result;
}

VKAPI_ATTR VkBool32 VKAPI_CALL Application::DebugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    std::cout << "validation layer: " << pCallbackData->pMessage << std::endl;
    return VK_FALSE;
}
