#include "descriptorpool.h"

DescriptorPool::DescriptorPool()
{
}

GrResult DescriptorPool::Init(std::shared_ptr<Instance> pInstance,
    const std::vector<VkDescriptorPoolSize>& poolSizes,
    uint32_t maxSets)
{
    GrResult result = GR_SUCCESS;
    m_pInstance = pInstance;

    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo{};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.flags = 0;
    descriptorPoolCreateInfo.maxSets = maxSets;
    descriptorPoolCreateInfo.poolSizeCount = (uint32_t)poolSizes.size();
    descriptorPoolCreateInfo.pPoolSizes = poolSizes.data();

    if (vkCreateDescriptorPool(m_pInstance->GetVkDevice(), &descriptorPoolCreateInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create descriptor pool!");
    }

    return result;
}

GrResult DescriptorPool::Cleanup()
{
    GrResult result = GR_SUCCESS;
    vkResetDescriptorPool(m_pInstance->GetVkDevice(), m_descriptorPool, 0);
    vkDestroyDescriptorPool(m_pInstance->GetVkDevice(), m_descriptorPool, nullptr);
    return result;
}

VkDescriptorPool DescriptorPool::GetVkDescriptorPool()
{
    return m_descriptorPool;
}
