#include "computepipeline.h"

ComputePipeline::ComputePipeline() : 
    Pipeline()
{
}

GrResult ComputePipeline::Init(
    std::shared_ptr<Instance> pInstance, 
    Program* pProgram,
    std::vector<std::shared_ptr<DescriptorSet> > descriptorSets)
{
    GrResult result = Pipeline::Init(pInstance);

    for (auto shader : pProgram->GetShaders()) 
    {
        VkPipelineShaderStageCreateInfo shaderStageCreateInfo{};
        shaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        shaderStageCreateInfo.stage = shader.second.GetVkStage();
        shaderStageCreateInfo.module = shader.second.GetShaderModule();
        shaderStageCreateInfo.pName = "main";
        m_pipelineParameters.shaderStageCreateInfos.push_back(shaderStageCreateInfo);
    }

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
    for (auto descriptorSet : descriptorSets)
    {
        descriptorSetLayouts.push_back(descriptorSet->GetVkDescriptorSetLayout());
    }

    m_pipelineParameters.pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    m_pipelineParameters.pipelineLayoutInfo.setLayoutCount = descriptorSetLayouts.size(); // Optional
    m_pipelineParameters.pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data(); // Optional
    m_pipelineParameters.pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    m_pipelineParameters.pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

    if (vkCreatePipelineLayout(m_pInstance->GetVkDevice(), &(m_pipelineParameters.pipelineLayoutInfo), nullptr, &m_pipelineLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create pipeline layout!");
    }

    return result;
}

GrResult ComputePipeline::Create()
{
    GrResult result = GR_SUCCESS;

    VkComputePipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    pipelineInfo.pNext = nullptr;
    pipelineInfo.stage = m_pipelineParameters.shaderStageCreateInfos[0];
    pipelineInfo.layout = m_pipelineLayout;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    pipelineInfo.basePipelineIndex = -1; // Optional

    if (vkCreateComputePipelines(m_pInstance->GetVkDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &m_pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create graphics pipeline!");
    }

    return result;
}

GrResult ComputePipeline::Bind(CommandBuffer* pCommandBuffer)
{
    GrResult result = GR_SUCCESS;

    vkCmdBindPipeline(pCommandBuffer->GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_COMPUTE, m_pipeline);

    return result;
}