#include <shader.h>

Shader::Shader(GrShaderStage stage, GrShaderType type, std::vector<uint8_t>& code) :
    m_stage(stage),
    m_type(type),
    m_code(code)
{
    if ((type == GrShaderType::GR_GLSL) && (m_code[m_code.size() - 1] != '\0'))
    {
        m_code.push_back('\0');
    }

    switch (stage) //GrShaderStage
    {
    case GR_VERTEX:
    {
        m_glslangStage = EShLanguage::EShLangVertex;
        m_vkStage = VK_SHADER_STAGE_VERTEX_BIT;
        break;
    }
    case GR_TESSELLATION_CONTROL:
    {
        m_glslangStage = EShLanguage::EShLangTessControl;
        m_vkStage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
        break;
    }
    case GR_TESSELLATION_EVALUATION:
    {
        m_glslangStage = EShLanguage::EShLangTessEvaluation;
        m_vkStage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
        break;
    }
    case GR_GEOMETRY:
    {
        m_glslangStage = EShLanguage::EShLangGeometry;
        m_vkStage = VK_SHADER_STAGE_GEOMETRY_BIT;
        break;
    }
    case GR_FRAGMENT:
    {
        m_glslangStage = EShLanguage::EShLangFragment;
        m_vkStage = VK_SHADER_STAGE_FRAGMENT_BIT;
        break;
    }
    case GR_COMPUTE:
    {
        m_glslangStage = EShLanguage::EShLangCompute;
        m_vkStage = VK_SHADER_STAGE_COMPUTE_BIT;
        break;
    }
    case GR_RAYGEN:
    {
        m_glslangStage = EShLanguage::EShLangRayGen;
        m_vkStage = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
        break;
    }
    case GR_ANY_HIT:
    {
        m_glslangStage = EShLanguage::EShLangAnyHit;
        m_vkStage = VK_SHADER_STAGE_ANY_HIT_BIT_KHR;
        break;
    }
    case GR_CLOSEST_HIT:
    {
        m_glslangStage = EShLanguage::EShLangClosestHit;
        m_vkStage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
        break;
    }
    case GR_MISS:
    {
        m_glslangStage = EShLanguage::EShLangMiss;
        m_vkStage = VK_SHADER_STAGE_MISS_BIT_KHR;
        break;
    }
    case GR_INTERSECTION:
    {
        m_glslangStage = EShLanguage::EShLangIntersect;
        m_vkStage = VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
        break;
    }
    case GR_CALLABLE:
    {
        m_glslangStage = EShLanguage::EShLangCallable;
        m_vkStage = VK_SHADER_STAGE_CALLABLE_BIT_KHR;
        break;
    }
    default:
        assert(0 && "Unknown shader stage");
        break;
    }
}

std::vector<uint8_t>& Shader::GetCode()
{
    return m_code;
}

GrShaderStage Shader::GetStage()
{
    return m_stage;
}

GrShaderType Shader::GetType()
{
    return m_type;
}

EShLanguage Shader::GetGlslangStage()
{
    return m_glslangStage;
}

VkShaderStageFlagBits Shader::GetVkStage()
{
    return m_vkStage;
}

void Shader::SetShaderModule(VkShaderModule shaderModule)
{
    m_shaderModule = shaderModule;
}

VkShaderModule Shader::GetShaderModule()
{
    return m_shaderModule;
}
