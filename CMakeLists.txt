cmake_minimum_required(VERSION 3.29.0)

set(CMAKE_CXX_STANDARD 23)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

project( Grapid CXX )

IF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  SET(CMAKE_INSTALL_PREFIX ${Grapid_SOURCE_DIR}/install CACHE PATH <comment> FORCE)
ENDIF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)

include( FindVulkan )

# Include directories
include_directories(${Grapid_SOURCE_DIR}/include)
include_directories(${Grapid_SOURCE_DIR}/external/)
include_directories(${Grapid_SOURCE_DIR}/external/glm)
include_directories(${Grapid_SOURCE_DIR}/external/glfw/include)
include_directories(${Grapid_SOURCE_DIR}/external/assimp/include)
include_directories(${PROJECT_BINARY_DIR}/external/assimp/include)
include_directories(${Grapid_SOURCE_DIR}/external/VulkanMemoryAllocator/include)

set(LIBRARY_SOURCES 
src/accelerationstructure.cpp
src/application.cpp
src/buffer.cpp
src/camera.cpp
src/commandbuffer.cpp
src/compiler.cpp
src/computepipeline.cpp
src/descriptorpool.cpp
src/descriptorset.cpp
src/framebuffer.cpp
src/graphicspipeline.cpp
src/image.cpp
src/instance.cpp
src/object.cpp
src/pipeline.cpp
src/program.cpp
src/queue.cpp
src/raytracingpipeline.cpp
src/renderpass.cpp
src/sampler.cpp
src/shader.cpp
src/support.cpp
src/swapchain.cpp)
set(LIBRARY_HEADERS
include/accelerationstructure.h
include/application.h
include/buffer.h
include/camera.h
include/commandbuffer.h
include/compiler.h
include/computepipeline.h
include/descriptorpool.h
include/descriptorset.h
include/export.h
include/framebuffer.h
include/graphicspipeline.h
include/grapid.h
include/image.h
include/instance.h
include/object.h
include/pipeline.h
include/program.h
include/queue.h
include/raytracingpipeline.h
include/renderpass.h
include/sampler.h
include/shader.h
include/support.h
include/swapchain.h
include/types.h
include/utility.h)
add_library(Grapid SHARED ${LIBRARY_HEADERS} ${LIBRARY_SOURCES})

set_target_properties(Grapid PROPERTIES PUBLIC_HEADER "${LIBRARY_HEADERS}")

install(TARGETS Grapid 
        EXPORT DESTINATION lib/Debug
        RUNTIME DESTINATION bin/Debug
        PUBLIC_HEADER DESTINATION include/Grapid)

set(CMAKE_DEBUG_POSTFIX "" CACHE STRING <comment> FORCE)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${Grapid_SOURCE_DIR}/install/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${Grapid_SOURCE_DIR}/install/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${Grapid_SOURCE_DIR}/install/bin)
set(ASSIMP_RUNTIME_OUTPUT_DIRECTORY ${Grapid_SOURCE_DIR}/install/bin)
set(ASSIMP_LIBRARY_OUTPUT_DIRECTORY ${Grapid_SOURCE_DIR}/install/bin)
set(ASSIMP_ARCHIVE_OUTPUT_DIRECTORY ${Grapid_SOURCE_DIR}/install/lib)

set(BUILD_STATIC_LIBS ON CACHE BOOL <comment> FORCE)
set(BUILD_SHARED_LIBS OFF CACHE BOOL <comment> FORCE)

set(GLFW_BUILD_DOCS OFF CACHE BOOL <comment> FORCE)

set(ENABLE_OPT false)

add_subdirectory(external/glm)
add_subdirectory(external/glslang)
add_subdirectory(external/glfw)
add_subdirectory(external/assimp)
add_subdirectory(external/volk)
add_subdirectory(external/VulkanMemoryAllocator)

target_compile_definitions(Grapid PUBLIC VK_NO_PROTOTYPES)

if(WIN32)
    target_compile_definitions(Grapid PUBLIC VK_USE_PLATFORM_WIN32_KHR)
endif()

IF (MSVC)
add_definitions(-D_CRT_SECURE_NO_WARNINGS)
target_link_libraries(Grapid PUBLIC glm)
target_link_libraries(Grapid PUBLIC glfw)
target_link_libraries(Grapid PUBLIC glslang)
target_link_libraries(Grapid PUBLIC SPIRV)
target_link_libraries(Grapid PUBLIC OSDependent)
target_link_libraries(Grapid PUBLIC SPVRemapper)
target_link_libraries(Grapid PUBLIC assimp)
target_link_libraries(Grapid PUBLIC zlibstatic)
target_link_libraries(Grapid PUBLIC volk)
target_link_libraries(Grapid PUBLIC VulkanMemoryAllocator)
ELSEIF (APPLE) 
include_directories(/usr/local/include)
target_link_libraries(Grapid ${Grapid_SOURCE_DIR}/lib/libglfw3.a)
target_link_libraries(Grapid ${Vulkan_LIBRARY} )
target_link_libraries(Grapid "-framework Cocoa")
target_link_libraries(Grapid "-framework OpenGL")
target_link_libraries(Grapid "-framework IOKit")
ELSE ()
target_link_libraries(Grapid ${PROJECT_BINARY_DIR}/GLFW-install/lib/libglfw3.a)
target_link_libraries(Grapid -lm -ldl -lXinerama -lXcursor -lX11 -lXxf86vm -lXrandr -lpthread )
ENDIF ()

set(ASSIMP_BIN_INSTALL_DIR ${Grapid_SOURCE_DIR}/install/bin)
set(ASSIMP_INCLUDE_INSTALL_DIR ${Grapid_SOURCE_DIR}/install/include)
set(ASSIMP_LIB_INSTALL_DIR ${Grapid_SOURCE_DIR}/install/lib)

target_include_directories(glfw PRIVATE $ENV{VULKAN_SDK}/Include)

set_target_properties(glm PROPERTIES FOLDER "External/glm")

#set_target_properties(glm PROPERTIES FOLDER "External/glm")
set_target_properties(glslang PROPERTIES FOLDER "External/glslang")
#set_target_properties(glslangValidator PROPERTIES FOLDER "External/glslang")
set_target_properties(spirv-remap PROPERTIES FOLDER "External/glslang")
set_target_properties(glslang-default-resource-limits PROPERTIES FOLDER "External/glslang")
set_target_properties(OSDependent PROPERTIES FOLDER "External/glslang")
set_target_properties(SPIRV PROPERTIES FOLDER "External/glslang")
set_target_properties(SPVRemapper PROPERTIES FOLDER "External/glslang")
#set_target_properties(Continuous PROPERTIES FOLDER "External/glslang")
#set_target_properties(Experimental PROPERTIES FOLDER "External/glslang")
#set_target_properties(Nightly PROPERTIES FOLDER "External/glslang")
#set_target_properties(NightlyMemoryCheck PROPERTIES FOLDER "External/glslang")

set_target_properties(glfw PROPERTIES FOLDER "External/glfw")
set_target_properties(uninstall PROPERTIES FOLDER "External/glfw")

set_target_properties(assimp PROPERTIES FOLDER "External/assimp")
set_target_properties(UpdateAssimpLibsDebugSymbolsAndDLLs PROPERTIES FOLDER "External/assimp")
set_target_properties(unit PROPERTIES FOLDER "External/assimp")
set_target_properties(zlibstatic PROPERTIES FOLDER "External/assimp")

set_target_properties(volk PROPERTIES FOLDER "External/volk")

set_target_properties(VulkanMemoryAllocator PROPERTIES FOLDER "External/VulkanMemoryAllocator")

add_subdirectory(examples)

add_dependencies(Grapid glm glslang glfw volk VulkanMemoryAllocator)
add_dependencies(SPIRV glslang)
