#ifndef BUFFER_H
#define BUFFER_H

#include <stdexcept>
#include <memory>

#include <vk_mem_alloc.h>

#include <export.h>
#include <types.h>
#include "instance.h"

class Buffer
{
public:
    EXPORT Buffer();

    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance, 
        VkDeviceSize size, 
        VkBufferUsageFlags bufferUsage, 
        VkSharingMode sharingMode, 
        VkDeviceSize minAllocationAlignment = 0,
        VmaMemoryUsage memoryUsage = VMA_MEMORY_USAGE_AUTO,
        VmaAllocationCreateFlags flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT);

    EXPORT GrResult Update(const char* const newData, size_t offset, size_t size);
    EXPORT GrResult Cleanup();

    EXPORT uint8_t* Map();
    EXPORT void Unmap();
    EXPORT bool Mapped() { return m_mappedData != nullptr; }
    EXPORT void Flush();

    VkDeviceSize GetVkSize() { return m_size; }
    VkBuffer GetVkBuffer() { return m_buffer; }
    VkBuffer* GetVkBufferPointer() { return &m_buffer; }

private:
    std::shared_ptr<Instance> m_pInstance;
    VkBuffer m_buffer;
    VmaAllocation m_allocation;
    VkDeviceSize m_size;
    uint8_t* m_mappedData = nullptr;
    bool m_persistent = false;

};

#endif //BUFFER_H
