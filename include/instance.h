#ifndef INSTANCE_H
#define INSTANCE_H

#include <stdexcept>
#include <assert.h>
#include <vector>

#include <volk.h>
#include <vk_mem_alloc.h>

#include "export.h"
#include "types.h"

class Instance
{

public:
    EXPORT Instance();
    //EXPORT Instance(Instance const&) = delete;
    //EXPORT Instance& operator=(Instance const&) = delete;

    //EXPORT static Instance& Get() {
    //    static Instance instance;
    //    return instance;
    //}

    EXPORT GrResult SetVkDevice(VkDevice device);
    EXPORT GrResult SetVkInstance(VkInstance instance);
    EXPORT GrResult SetVkPhysicalDevice(VkPhysicalDevice physicalDevice);
    EXPORT GrResult SetVmaAllocator(VmaAllocator allocator);

    EXPORT VkDevice GetVkDevice();
    EXPORT VkInstance GetVkInstance();
    EXPORT VkPhysicalDevice GetVkPhysicalDevice(); 
    EXPORT VmaAllocator GetVmaAllocator();

    EXPORT uint32_t FindMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
    EXPORT VkFormat FindSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
    EXPORT VkFormat FindDepthFormat();

private:
    VkDevice m_device{ VK_NULL_HANDLE };
    VkInstance m_instance{ VK_NULL_HANDLE };
    VkPhysicalDevice m_physicalDevice{ VK_NULL_HANDLE };
    VmaAllocator m_allocator{ VK_NULL_HANDLE };
};

#endif //INSTANCE_H
