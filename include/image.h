#ifndef IMAGE_H
#define IMAGE_H

#include <stdexcept>

#include <export.h>
#include <types.h>
#include "instance.h"
#include "commandbuffer.h"
#include "queue.h"

class Image
{
public:
    EXPORT Image();

    EXPORT GrResult Import(
        std::shared_ptr<Instance> pInstance,
        VkImage image,
        VkFormat format,
        VkImageAspectFlags aspectFlags);

    EXPORT GrResult Init(
        std::shared_ptr<Instance> pInstance,
        uint32_t width, 
        uint32_t height,
        VkFormat format,
        VkImageUsageFlags usage,
        VkImageAspectFlags aspectFlags,
        VkImageTiling tiling = VK_IMAGE_TILING_OPTIMAL,
        VkMemoryPropertyFlags properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    EXPORT GrResult Cleanup();

    EXPORT void LayoutTransition(CommandBuffer commandBuffer,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        VkAccessFlags srcAccessMask,
        VkAccessFlags dstAccessMask,
        VkImageLayout oldLayout,
        VkImageLayout newLayout,
        VkImageSubresourceRange const& subresource_range);

    EXPORT void LayoutTransition(CommandBuffer commandBuffer,
        VkImageLayout old_layout,
        VkImageLayout new_layout,
        VkImageSubresourceRange const& subresource_range);

    EXPORT void LayoutTransition(CommandBuffer commandBuffer,
        VkImageLayout old_layout,
        VkImageLayout new_layout);

    EXPORT VkImage GetVkImage();
    EXPORT VkSampler GetSampler();
    EXPORT VkImageView GetImageView();
    EXPORT VkImageLayout GetImageLayout();
    EXPORT VkDeviceSize GetSize();

    EXPORT GrResult CreateStagingBuffer(VkDeviceSize size);
    EXPORT GrResult Upload(Queue queue, char* textureData);
    EXPORT GrResult Download(Queue queue, char* textureData);

private:
    std::shared_ptr<Instance> m_pInstance;
    VkImageLayout m_imageLayout{};
    VkDeviceMemory m_memory{};
    VkImage m_image{};
    VmaAllocation m_allocation{};
    VkImageView m_imageView{};
    VkSampler m_sampler{};

    VkBuffer m_stagingBuffer{};
    VmaAllocation m_stagingAllocation{};

    VkOffset3D m_imageOffset{};
    VkExtent3D m_imageExtent{};
    VkDeviceSize m_size;
};

#endif //IMAGE_H
