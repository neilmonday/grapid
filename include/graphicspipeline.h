#ifndef GRAPHICSPIPELINE_H
#define GRAPHICSPIPELINE_H

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <volk.h>

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <glslang/SPIRV/disassemble.h>

#include <export.h>
#include <types.h>
#include <shader.h>
#include <program.h>
#include <renderpass.h>
#include <pipeline.h>
#include <descriptorset.h>

class GraphicsPipeline : public Pipeline
{
public:
    struct GraphicsPipelineParameters
    {
        std::vector<VkVertexInputBindingDescription> bindingDescription{};
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
        std::vector<VkPipelineShaderStageCreateInfo> shaderStageCreateInfos{};
        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
        VkPipelineTessellationStateCreateInfo tessellation{};
        VkViewport viewport{};
        VkRect2D scissor{};
        VkPipelineViewportStateCreateInfo viewportState{};
        VkPipelineRasterizationStateCreateInfo rasterizer{};
        VkPipelineMultisampleStateCreateInfo multisampling{};
        VkPipelineDepthStencilStateCreateInfo depthStencil{};
        VkPipelineColorBlendAttachmentState colorBlendAttachment{};
        VkPipelineColorBlendStateCreateInfo colorBlending{};
        VkPipelineDynamicStateCreateInfo dynamicState{};
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        Renderpass renderpass{};
    };

    EXPORT GraphicsPipeline();

    EXPORT GrResult Init(
        std::shared_ptr<Instance> pInstance,
        Renderpass renderpass,
        VkExtent2D extent,
        Program* pProgram,
        std::shared_ptr<std::vector<VkVertexInputAttributeDescription> > attributeDescriptions,
        std::shared_ptr<std::vector<VkVertexInputBindingDescription> > bindingDescription,
        std::vector<std::shared_ptr<DescriptorSet> > descriptorSets,
        VkPrimitiveTopology topology);

    EXPORT GrResult Create() override;
    EXPORT GrResult Bind(CommandBuffer* pCommandBuffer) override;

    GraphicsPipelineParameters& GetPipelineParameters() { return m_pipelineParameters; }

private:
    GraphicsPipelineParameters m_pipelineParameters{};
};

#endif //GRAPHICSPIPELINE_H
