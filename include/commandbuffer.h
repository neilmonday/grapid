#ifndef COMMANDBUFFER_H
#define COMMANDBUFFER_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>

#include <glslang/Public/ShaderLang.h>

#include <export.h>
#include <types.h>
#include <queue.h>
#include "instance.h"

class CommandBuffer
{
public:
    EXPORT CommandBuffer();
    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance, Queue queue);
    EXPORT GrResult Cleanup(Queue queue);

    EXPORT GrResult Begin();
    EXPORT GrResult End();

    EXPORT VkCommandBuffer GetVkCommandBuffer();

private:
    std::shared_ptr<Instance> m_pInstance;
    VkCommandBuffer m_commandBuffer;

};

#endif //RENDERPCOMMANDBUFFER_HASS_H
