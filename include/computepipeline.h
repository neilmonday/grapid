#ifndef COMPUTEPIPELINE_H
#define COMPUTEPIPELINE_H

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <volk.h>

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <glslang/SPIRV/disassemble.h>

#include <export.h>
#include <types.h>
#include <shader.h>
#include <program.h>
#include <pipeline.h>
#include <descriptorset.h>

class ComputePipeline : public Pipeline
{
public:
    struct ComputePipelineParameters
    {
        std::vector<VkPipelineShaderStageCreateInfo> shaderStageCreateInfos{};
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    };

    EXPORT ComputePipeline();

    EXPORT GrResult Init(
        std::shared_ptr<Instance> pInstance,
        Program* pProgram,
        std::vector<std::shared_ptr<DescriptorSet> > descriptorSets);

    EXPORT GrResult Create() override;
    EXPORT GrResult Bind(CommandBuffer* pCommandBuffer) override;

    ComputePipelineParameters& GetPipelineParameters() { return m_pipelineParameters; }

private:
    ComputePipelineParameters m_pipelineParameters{};
};

#endif //COMPUTEPIPELINE_H
