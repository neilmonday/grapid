#ifndef OBJECT_H
#define OBJECT_H

#include <string>
#include <iostream>
#include <vector>

#include <glm/glm.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <export.h>
#include <types.h>

class Object
{
public:
    EXPORT Object();

    EXPORT GrResult Import(const std::string& file);

    EXPORT std::vector<glm::vec3> GetVertices();

private:
    std::vector<glm::vec3> vertices;
};

#endif //OBJECT_H