#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <glslang/Public/ShaderLang.h>

#include <export.h>
#include <types.h>

class Framebuffer
{
public:
    EXPORT Framebuffer(VkDevice* pDevice);
    EXPORT GrResult Init(VkExtent2D extent, VkRenderPass renderpass, VkImageView* attachment);
    EXPORT GrResult Cleanup();

    EXPORT VkFramebuffer GetVkFramebuffer();

private:
    VkDevice* m_pDevice;
    VkFramebuffer m_framebuffer;

};

#endif //VERTEXBUFFER_H