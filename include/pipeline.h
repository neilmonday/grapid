#ifndef PIPELINE_H
#define PIPELINE_H

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <volk.h>

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <glslang/SPIRV/disassemble.h>

#include <export.h>
#include <types.h>
#include <shader.h>
#include <program.h>
#include <renderpass.h>
#include "descriptorset.h"

class Pipeline
{
public:

    EXPORT Pipeline();

    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance);
    EXPORT GrResult Cleanup();

    EXPORT virtual GrResult Create() = 0;
    EXPORT virtual GrResult Bind(CommandBuffer* pCommandBuffer) = 0;

    EXPORT VkPipeline GetVkPipeline();
    EXPORT VkPipelineLayout GetVkPipelineLayout();

protected:
    std::shared_ptr<Instance> m_pInstance{};
    VkPipelineLayout m_pipelineLayout{};
    VkPipeline m_pipeline{};
};

#endif //PIPELINE_H
