#ifndef ACCELERATIONSTRUCTURE_H
#define ACCELERATIONSTRUCTURE_H

#include <stdexcept>
#include <memory>

#include <vk_mem_alloc.h>

#include <export.h>
#include <types.h>
#include <buffer.h>

class AccelerationStructure
{
public:
    EXPORT AccelerationStructure();

    EXPORT GrResult Init();
    EXPORT GrResult Update();
    EXPORT GrResult Cleanup();

    EXPORT void SetBuffer(std::shared_ptr<Buffer> buffer) { m_buffer = buffer; }
    EXPORT void SetVkAccelerationStructure(VkAccelerationStructureKHR accelerationStructure) { m_accelerationStructure = accelerationStructure; }
    EXPORT void SetVkDeviceAddress(VkDeviceAddress deviceAddress) { m_deviceAddress = deviceAddress; }

    EXPORT std::shared_ptr<Buffer> GetBuffer() { return m_buffer; }
    EXPORT VkAccelerationStructureKHR GetVkAccelerationStructure() { return m_accelerationStructure; }
    EXPORT VkDeviceAddress GetVkDeviceAddress() { return m_deviceAddress; }

private:
    VkAccelerationStructureKHR          m_accelerationStructure;
    VkDeviceAddress                     m_deviceAddress;
    std::shared_ptr<Buffer>             m_buffer;
};

#endif //ACCELERATIONSTRUCTURE_H
