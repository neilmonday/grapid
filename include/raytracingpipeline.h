#ifndef RAYTRACINGPIPELINE_H
#define RAYTRACINGPIPELINE_H

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <volk.h>

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <glslang/SPIRV/disassemble.h>

#include <export.h>
#include <types.h>
#include <buffer.h>
#include <shader.h>
#include <program.h>
#include <renderpass.h>
#include <pipeline.h>
#include <descriptorset.h>
#include <utility.h>

class RayTracingPipeline : public Pipeline
{
public:
    struct RayTracingPipelineParameters
    {
        std::vector<VkPipelineShaderStageCreateInfo> shaderStageCreateInfos{};
        std::vector<VkRayTracingShaderGroupCreateInfoKHR> m_shaderGroups{};
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    };

    EXPORT RayTracingPipeline();

    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance,
        Program* pProgram,
        std::vector<std::shared_ptr<DescriptorSet> > descriptorSets);

    EXPORT GrResult Create() override;
    EXPORT GrResult Bind(CommandBuffer* pCommandBuffer) override;

    EXPORT GrResult GetShaderBindingTableEntries(
        VkStridedDeviceAddressRegionKHR& raygen_shader_sbt_entry,
        VkStridedDeviceAddressRegionKHR& miss_shader_sbt_entry,
        VkStridedDeviceAddressRegionKHR& hit_shader_sbt_entry,
        VkStridedDeviceAddressRegionKHR& callable_shader_sbt_entry);

    EXPORT RayTracingPipelineParameters& GetPipelineParameters() { return m_pipelineParameters; }

private:
    RayTracingPipelineParameters m_pipelineParameters{};
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR  m_rayTracingPipelineProperties{};

    std::unique_ptr<Buffer> m_raygenShaderBindingTable;
    std::unique_ptr<Buffer> m_missShaderBindingTable;
    std::unique_ptr<Buffer> m_hitShaderBindingTable;

};

#endif //RAYTRACINGPIPELINE_H
