#ifndef RENDERPASS_H
#define RENDERPASS_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>

#include <glslang/Public/ShaderLang.h>

#include <export.h>
#include <types.h>
#include "framebuffer.h"
#include "commandbuffer.h"
#include "instance.h"

class Renderpass
{
public:
    EXPORT Renderpass();
    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance, VkFormat format);
    EXPORT GrResult Cleanup();

    EXPORT GrResult Begin(VkExtent2D extent, Framebuffer* pFramebuffer, CommandBuffer* pCommandBuffer);
    EXPORT GrResult End(CommandBuffer* pCommandBuffer);

    EXPORT VkRenderPass GetVkRenderPass();

private:
    std::shared_ptr<Instance> m_pInstance;
    VkRenderPass m_renderPass;

};

#endif //RENDERPASS_H
