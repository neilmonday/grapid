#ifndef TYPES_H
#define TYPES_H

#include <volk.h>

typedef enum GrResult {
    GR_SUCCESS = 0,
    GR_ERROR = -1,
    GR_RESULT_MAX_ENUM = 0x7FFFFFFF
} GrResult;

typedef enum GrShaderType {
    GR_GLSL,
    GR_SPIRV,
    GR_SHADER_TYPE_MAX
}GrShaderType;

typedef enum GrShaderStage {
    GR_VERTEX,
    GR_TESSELLATION_CONTROL,
    GR_TESSELLATION_EVALUATION,
    GR_GEOMETRY,
    GR_FRAGMENT,
    GR_COMPUTE,
    GR_RAYGEN,
    GR_ANY_HIT,
    GR_CLOSEST_HIT,
    GR_MISS,
    GR_INTERSECTION,
    GR_CALLABLE,
    GR_SHADER_STAGE_MAX
}GrShaderStage;

static GrResult VkResultToGrResult(VkResult vkResult)
{
    GrResult result = GR_SUCCESS;
    switch (vkResult)
    {
    case VK_SUCCESS:
        result = GR_SUCCESS;
        break;
    default:
        result = GR_ERROR;
        break;
    }
    return result;
}

#endif //TYPES_H
