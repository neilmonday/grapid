#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>

#include "volk.h"

#include <glslang/Public/ShaderLang.h>

#include <export.h>
#include <types.h>

class Shader
{
public:
    EXPORT Shader(GrShaderStage stage, GrShaderType type, std::vector<uint8_t>& code);

    EXPORT std::vector<uint8_t>& GetCode();
    EXPORT size_t GetCodeSize();
    EXPORT GrShaderStage GetStage();
    EXPORT GrShaderType GetType();
    EXPORT EShLanguage GetGlslangStage();
    EXPORT VkShaderStageFlagBits GetVkStage();
    EXPORT VkShaderModule GetShaderModule();

    EXPORT void SetShaderModule(VkShaderModule shaderModule);

private:
    std::vector<uint8_t> m_code;
    GrShaderStage m_stage;
    GrShaderType m_type;
    EShLanguage m_glslangStage;
    VkShaderStageFlagBits m_vkStage;

    VkShaderModule m_shaderModule;

    std::vector<char> m_spirv;
};

#endif //SHADER_H