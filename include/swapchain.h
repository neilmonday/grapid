#ifndef SWAPCHAIN_H
#define SWAPCHAIN_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>

#include <volk.h>

#include <glslang/Public/ShaderLang.h>

#include <export.h>
#include <types.h>
#include <instance.h>
#include <image.h>

class Swapchain
{
public:
    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance, VkExtent2D windowSize, VkSurfaceKHR surface);
    EXPORT GrResult Cleanup();

    //EXPORT VkSwapchainKHR GetVkSwapchain();
    EXPORT size_t GetImageViewCount();
    EXPORT Image GetImage(uint32_t i);
    EXPORT VkImageView GetVkImageView(uint32_t i);
    EXPORT VkFormat GetVkFormat();
    EXPORT VkExtent2D GetVkExtent2D();
    EXPORT VkSwapchainKHR GetVkSwapchain();

private:
    std::shared_ptr<Instance> m_pInstance;
    VkSwapchainKHR m_swapchainKHR;
    VkSurfaceKHR m_surface;
    std::vector<Image> m_images;
    std::vector<VkImageView> m_imageViews;
    VkFormat m_imageFormat;
    VkExtent2D m_extent;
};

#endif //SWAPCHAIN_H