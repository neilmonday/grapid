#ifndef DESCRIPTORSET_H
#define DESCRIPTORSET_H

#include <stdexcept>

#include <export.h>
#include <types.h>
#include "image.h"
#include "instance.h"
#include "descriptorpool.h"

class DescriptorSet
{
public:
    EXPORT DescriptorSet();

    EXPORT virtual GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool);
    EXPORT virtual GrResult Cleanup();

    EXPORT VkDescriptorSetLayout GetVkDescriptorSetLayout();
    EXPORT VkDescriptorSet GetVkDescriptorSet();

protected:
    std::shared_ptr<Instance> m_pInstance;
    DescriptorPool m_descriptorPool = {};
    VkDescriptorSetLayout m_descriptorSetLayout = {};
    VkDescriptorSet m_descriptorSet = {};
};

#endif //DESCRIPTORSET_H
