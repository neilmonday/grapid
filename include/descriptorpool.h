#ifndef DESCRIPTORPOOL_H
#define DESCRIPTORPOOL_H

#include <stdexcept>

#include <vector>
#include <export.h>
#include <types.h>
#include "image.h"
#include "instance.h"

class DescriptorPool
{
public:
    EXPORT DescriptorPool();

    EXPORT virtual GrResult Init(std::shared_ptr<Instance> pInstance,
        const std::vector<VkDescriptorPoolSize>& poolSizes,
        uint32_t maxSets);
    EXPORT virtual GrResult Cleanup();

    EXPORT VkDescriptorPool GetVkDescriptorPool();

protected:
    std::shared_ptr<Instance> m_pInstance;
    VkDescriptorPool m_descriptorPool = {};
};

#endif //DESCRIPTORPOOL_H
