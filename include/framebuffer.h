#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <iostream>
#include <stdexcept>
#include <vector>
#include <assert.h>

#include <volk.h>

#include <glslang/Public/ShaderLang.h>

#include <export.h>
#include <types.h>
#include "swapchain.h"

class Framebuffer
{
public:
    EXPORT Framebuffer();
    EXPORT GrResult Init(std::shared_ptr<Instance> m_pInstance, VkExtent2D extent, VkRenderPass renderpass, std::vector<VkImageView>& attachments);
    EXPORT GrResult Cleanup();

    EXPORT VkFramebuffer GetVkFramebuffer();

private:
    std::shared_ptr<Instance> m_pInstance;
    VkFramebuffer m_framebuffer;

};

#endif //FRAMEBUFFER_H