#ifndef EXPORT_H
#define EXPORT_H

#define MAKEDLL

#ifdef MAKEDLL
#  define EXPORT __declspec(dllexport)
#else
#  define EXPORT __declspec(dllimport)
#endif

#endif //EXPORT_H