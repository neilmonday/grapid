#ifndef PROGRAM_H
#define PROGRAM_H

#include <stdexcept>
#include <string>
#include <vector>
#include <array>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <glslang/SPIRV/disassemble.h>

#include <export.h>
#include <types.h>
#include <shader.h>
#include "instance.h"

class Program
{
public:
    EXPORT Program();

    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance);

    EXPORT GrResult AddShader(Shader shader);

    EXPORT GrResult Compile();

    EXPORT GrResult CreateShaderModules();

    EXPORT std::map<GrShaderStage, Shader> GetShaders();

    EXPORT GrResult Cleanup();

private:
    std::shared_ptr<Instance> m_pInstance;

    std::map<GrShaderStage, Shader> m_shaders;

    glslang::TProgram m_glslangProgram;

    std::vector<std::string> Processes;                     // what should be recorded by OpModuleProcessed, or equivalent

    // Per descriptor-set binding base data
    typedef std::map<unsigned int, unsigned int> TPerSetBaseBinding;

    std::vector<std::pair<std::string, int>> uniformLocationOverrides;
    int uniformBase = 0;

    std::array<std::array<unsigned int, GR_SHADER_STAGE_MAX>, glslang::EResCount> baseBinding;
    std::array<std::array<TPerSetBaseBinding, GR_SHADER_STAGE_MAX>, glslang::EResCount> baseBindingForSet;
    std::array<std::vector<std::string>, GR_SHADER_STAGE_MAX> baseResourceSetBinding;
};

#endif //PROGRAM_H