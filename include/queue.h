#ifndef QUEUE_H
#define QUEUE_H

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <export.h>
#include <types.h>
#include <shader.h>
#include "instance.h"

class Queue
{
public:
    EXPORT Queue();
    EXPORT GrResult Init(std::shared_ptr<Instance> pInstance, uint32_t queueFamilyIndex);
    EXPORT GrResult Cleanup();

    EXPORT VkQueue GetVkQueue();
    EXPORT VkCommandPool GetVkCommandPool();

private:
    std::shared_ptr<Instance> m_pInstance;
    VkQueue m_queue;
    VkCommandPool m_commandPool;

};

#endif //QUEUE_H