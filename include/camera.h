#ifndef CAMERA_H
#define CAMERA_H

#include <stdexcept>
#include <memory>

#include <glfw/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <export.h>
#include <types.h>
#include "instance.h"

struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 viewInverse;
    glm::mat4 projection;
    glm::mat4 projectionInverse;
    glm::vec4 cameraPosition;
};

class Camera
{
public:
    EXPORT Camera();

    EXPORT GrResult Init(GLFWwindow* window);
    EXPORT GrResult Update();
    EXPORT GrResult Cleanup();


    EXPORT void SetPosition(glm::vec3 position) { m_position = position; }
    EXPORT void SetUpVector(glm::vec3 up) { m_up = up; }
    EXPORT void SetForwardVector(glm::vec3 forward) { m_forward = forward; }
    EXPORT void SetAspectRatio(float aspectRatio) { m_aspectRatio = aspectRatio; }

    EXPORT glm::vec3 GetPosition() { return m_position; }
    EXPORT glm::vec3 GetUpVector() { return m_up; }
    EXPORT glm::vec3 GetForwardVector() { return m_forward; }
    EXPORT float GetAspectRatio() { return m_aspectRatio; }
    EXPORT std::shared_ptr<UniformBufferObject> GetUniformBufferObject() { return m_pUniformBufferObject; }

private:
    GLFWwindow* m_window;

    glm::dvec2 m_initialCursor;
    glm::vec3 m_position{};
    glm::vec3 m_up{}; //Vulkan's coordinate system has origin at top left of window. This means up is (0, -1, 0)
    glm::vec3 m_forward{};
    float m_aspectRatio;

    std::shared_ptr<UniformBufferObject> m_pUniformBufferObject;

    glm::vec2 CalculateRotation();
    GrResult CalculateTranslation(const glm::vec2 rotation, glm::vec3* position);

};

#endif //CAMERA_H
