#ifndef APPLICATION_H
#define APPLCIATION_H

#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <assert.h>

#include <volk.h>
#include <GLFW/glfw3.h>

#include "export.h"
#include "types.h"
#include "instance.h"
#include "swapchain.h"
#include "commandbuffer.h"
#include "framebuffer.h"
#include "buffer.h"
#include "support.h"

EXPORT class Application
{
public:
    EXPORT Application();
    EXPORT Application(std::string name, uint32_t apiVersion = VK_API_VERSION_1_3);
    EXPORT virtual GrResult Init();
    EXPORT virtual GrResult Run() = 0;
    EXPORT virtual GrResult Cleanup();

    //EXPORT static Instance& GetInstance() { return Instance::Get(); }

    EXPORT GrResult BindVertexBuffers(CommandBuffer* pCommandBuffer, std::vector<std::shared_ptr<Buffer>>& vertexBuffers, std::vector<VkDeviceSize>& offsets);
    EXPORT GrResult BindVertexBuffer(CommandBuffer* pCommandBuffer, Buffer& vertexBuffer, VkDeviceSize offset);
    EXPORT GrResult BindIndexBuffer(CommandBuffer* pCommandBuffer, Buffer& indexBuffer, VkDeviceSize offset);

    EXPORT Swapchain GetSwapchain();
    EXPORT std::vector<uint32_t> GetQueueFamilyIndices();

    void PopulateDebugMessengerCreateInfo();

    static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData);

protected:
    EXPORT virtual GrResult InitDependencies();
    EXPORT GrResult CreateOSWindow();
    EXPORT GrResult CreateInstance();
    EXPORT GrResult CreateDebugUtilsMessenger();
    EXPORT GrResult CreateSurface();
    EXPORT virtual GrResult LoadVolkInstance();
    EXPORT GrResult PickPhysicalDevice();
    EXPORT virtual GrResult CreateLogicalDevice();
    EXPORT virtual GrResult LoadVolkDevice();
    EXPORT GrResult CreateAllocator();
    EXPORT GrResult CreateSwapchain();
    EXPORT GrResult CreateSemaphores();

    EXPORT void DestroyWindow();
    EXPORT void DestroyInstance();
    EXPORT void DestroyDebugUtilsMessenger();
    EXPORT void DestroySurface();
    EXPORT void DestroyLogicalDevice();
    EXPORT void DestroyAllocator();
    EXPORT void DestroySwapchain();
    EXPORT void DestroySemaphores();

    GLFWwindow* m_window;

    const int MAX_FRAMES_IN_FLIGHT = 2;

    std::vector<VkSemaphore> m_imageAvailableSemaphores;
    std::vector<VkSemaphore> m_renderFinishedSemaphores;
    std::vector<VkFence> m_inFlightFences;
    std::vector<VkFence> m_inFlightImages;
    size_t m_currentFrame = 0;

    std::vector<const char*> m_requiredLayerNames;
    std::vector<const char*> m_enabledLayerNames;
    std::vector<const char*> m_requiredDeviceExtensionNames;
    std::vector<const char*> m_requiredInstanceExtensionNames;

    // The extension feature pointer
    void* m_lastRequestedExtensionFeature{ nullptr };

    // Holds the extension feature structures, we use a map to retain an order of requested structures
    std::map<VkStructureType, std::shared_ptr<void>> m_extensionFeatures;

    VkExtent2D m_windowSize;

    std::shared_ptr<Instance> m_pInstance{};

    //bool VK_KHR_get_memory_requirements2_enabled = false;
    bool VK_KHR_get_physical_device_properties2_enabled = false;
    bool VK_KHR_dedicated_allocation_enabled = false;
    bool VK_KHR_bind_memory2_enabled = false;
    bool VK_EXT_memory_budget_enabled = false;
    bool VK_AMD_device_coherent_memory_enabled = false;
    bool VK_KHR_buffer_device_address_enabled = false;
    bool VK_EXT_memory_priority_enabled = false;
    //bool VK_EXT_debug_utils_enabled = false;

private:
    //std
    std::string m_name;

    //Vulkan
    uint32_t m_apiVersion;
    VkSurfaceKHR m_surface;
    VkQueue m_graphicsAndPresentQueue;
    VkDebugUtilsMessengerEXT m_debugMessenger;

    VkDeviceMemory m_vertexBufferMemory;
    VkDeviceMemory m_indexBufferMemory;

    Swapchain m_swapchain;

    //GLFW
    int RateDeviceSuitability(VkPhysicalDevice physicalDevice);

    VkDebugUtilsMessengerCreateInfoEXT m_debugCreateInfo{};

};

#endif //APPLICATION_H