#ifndef UTILITY_H
#define UTILITY_H

#include <stdint.h>

inline uint32_t aligned_size(uint32_t value, uint32_t alignment)
{
    return (value + alignment - 1) & ~(alignment - 1);
}

#endif //UTILITY_H
