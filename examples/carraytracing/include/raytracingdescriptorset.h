#ifndef RAYTRACINGDESCRIPTORSET_H
#define RAYTRACINGDESCRIPTORSET_H

#include <descriptorset.h>
#include <accelerationstructure.h>

class RayTracingDescriptorSet : public DescriptorSet
{
public:
    RayTracingDescriptorSet();

    GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool) override;
    GrResult Cleanup() override;

    GrResult Update(AccelerationStructure accelerationStructure, Image storageImage, Buffer uniformBuffer);
};

#endif //RAYTRACINGDESCRIPTORSET_H
