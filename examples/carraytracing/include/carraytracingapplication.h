#ifndef CARRAYTRACINGAPPLICATION_H
#define CARRAYTRACINGAPPLICATION_H

#include <fstream>
#include <vector>

#include <application.h>
#include <shader.h>
#include <program.h>
#include <image.h>
//#include <graphicspipeline.h>
#include <raytracingpipeline.h>
#include <renderpass.h>
#include <camera.h>
#include <accelerationstructure.h>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include "vertexinput.h"
#include "raytracingdescriptorset.h"

void WindowSizeCallback(GLFWwindow* window, int width, int height);
std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size);
std::vector<char> GetBinaryFileContents(std::string const& filename);

class CarRayTracingApplication : public Application
{
public:
    CarRayTracingApplication(std::string name);

    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

    GrResult CreateStorageImage();
    GrResult CreateBottomLevelAccelerationStructure();
    GrResult CreateTopLevelAccelerationStructure();
    GrResult CreateUniformBuffer();

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult CreateLogicalDevice() override;
    GrResult LoadVolkDevice() override;

private:
    std::unique_ptr<Camera> m_pCamera{};

    Buffer m_uniformBuffer{};

    std::vector<CommandBuffer> m_commandBuffers;
    Queue m_queue;
    //GraphicsPipeline m_graphicsPipeline;
    RayTracingPipeline m_rayTracingPipeline;
    std::vector<Framebuffer> m_framebuffers;
    //Program m_graphicsProgram;
    Program m_rayTracingProgram;
    //std::vector<std::shared_ptr<Buffer> > m_vertexBuffers{};
    Buffer m_vertexPositionBuffer;
    //Buffer m_vertexNormalBuffer;
    //Buffer m_vertexTexCoordBuffer;
    Buffer m_indexBuffer;
    Buffer m_transformationMatrixBuffer;
    Buffer m_instancesBuffer;

    std::vector<VkRayTracingShaderGroupCreateInfoKHR> m_rayTracingShaderGroups{};

    AccelerationStructure m_bottomLevelAccelerationStructure;
    AccelerationStructure m_topLevelAccelerationStructure;

    uint32_t m_minAccelerationStructureScratchOffsetAlignment;


    VkPhysicalDeviceAccelerationStructureFeaturesKHR m_accelerationStructureFeatures{};

    //Image m_image;
    Image m_storageImage;
    Renderpass m_renderpass;
    //DescriptorPool m_graphicsDescriptorPool; 
    DescriptorPool m_rayTracingDescriptorPool;
    //std::vector<std::shared_ptr<DescriptorSet>> m_graphicsDescriptorSets;
    std::vector<std::shared_ptr<DescriptorSet>> m_rayTracingDescriptorSets;
    std::vector<Image> m_depthImages{};
    uint32_t m_indicesCount = {};
    bool m_terminate;
};

#endif //CARRAYTRACINGAPPLICATION_H
