#include "carraytracingapplication.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

bool g_terminate = false;

CarRayTracingApplication::CarRayTracingApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
    m_windowSize = { 2560, 1440 };

    // Ray tracing related extensions required by this sample
    m_requiredDeviceExtensionNames.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
    m_requiredDeviceExtensionNames.push_back(VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME);

    // Required by VK_KHR_acceleration_structure
    m_requiredDeviceExtensionNames.push_back(VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME);
    m_requiredDeviceExtensionNames.push_back(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);
    m_requiredDeviceExtensionNames.push_back(VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME);

    // Required for VK_KHR_ray_tracing_pipeline
    m_requiredDeviceExtensionNames.push_back(VK_KHR_SPIRV_1_4_EXTENSION_NAME);

    // Required by VK_KHR_spirv_1_4
    m_requiredDeviceExtensionNames.push_back(VK_KHR_SHADER_FLOAT_CONTROLS_EXTENSION_NAME);
}

GrResult CarRayTracingApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult CarRayTracingApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult CarRayTracingApplication::CreateLogicalDevice()
{
    GrResult result = GR_SUCCESS;
    float queuePriority = 1.0f;
    std::vector<uint32_t> queueFamilyIndices = GetQueueFamilyIndices();

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = queueFamilyIndices[0];
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    // Get the extension feature
    VkPhysicalDeviceFeatures2 getPhysicalDeviceFeatures2{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 };
    VkPhysicalDeviceBufferDeviceAddressFeatures getPhysicalDeviceBufferDeviceAddressFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES };
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR getPhysicalDeviceRayTracingPipelineFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR };
    VkPhysicalDeviceAccelerationStructureFeaturesKHR getPhysicalDeviceAccelerationStructureFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR };

    getPhysicalDeviceFeatures2.pNext = &getPhysicalDeviceBufferDeviceAddressFeatures;
    getPhysicalDeviceBufferDeviceAddressFeatures.pNext = &getPhysicalDeviceRayTracingPipelineFeatures;
    getPhysicalDeviceRayTracingPipelineFeatures.pNext = &getPhysicalDeviceAccelerationStructureFeatures;
    vkGetPhysicalDeviceFeatures2(m_pInstance->GetVkPhysicalDevice(), &getPhysicalDeviceFeatures2);
    
    if ((getPhysicalDeviceBufferDeviceAddressFeatures.bufferDeviceAddress != VK_TRUE) ||
        (getPhysicalDeviceRayTracingPipelineFeatures.rayTracingPipeline != VK_TRUE) ||
        (getPhysicalDeviceAccelerationStructureFeatures.accelerationStructure != VK_TRUE))
    {
        throw std::runtime_error("device does not support required features!");
    }

    VkPhysicalDeviceProperties2 getPhysicalDeviceProperties2{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2 };
    VkPhysicalDeviceAccelerationStructurePropertiesKHR getPhysicalDeviceAccelerationStructurePropertiesKHR{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR };
    getPhysicalDeviceProperties2.pNext = &getPhysicalDeviceAccelerationStructurePropertiesKHR;
    vkGetPhysicalDeviceProperties2KHR(m_pInstance->GetVkPhysicalDevice(), &getPhysicalDeviceProperties2);

    m_minAccelerationStructureScratchOffsetAlignment = getPhysicalDeviceAccelerationStructurePropertiesKHR.minAccelerationStructureScratchOffsetAlignment;

    // Enable the extension features
    VkPhysicalDeviceFeatures2 physicalDeviceFeatures2{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 };
    VkPhysicalDeviceBufferDeviceAddressFeatures physicalDeviceBufferDeviceAddressFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES };
    VkPhysicalDeviceRayTracingPipelineFeaturesKHR physicalDeviceRayTracingPipelineFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR };
    VkPhysicalDeviceAccelerationStructureFeaturesKHR physicalDeviceAccelerationStructureFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR };

    physicalDeviceBufferDeviceAddressFeatures.bufferDeviceAddress = VK_TRUE;
    physicalDeviceRayTracingPipelineFeatures.rayTracingPipeline = VK_TRUE;
    physicalDeviceAccelerationStructureFeatures.accelerationStructure = VK_TRUE;

    physicalDeviceFeatures2.pNext = &physicalDeviceBufferDeviceAddressFeatures;
    physicalDeviceBufferDeviceAddressFeatures.pNext = &physicalDeviceRayTracingPipelineFeatures;
    physicalDeviceRayTracingPipelineFeatures.pNext = &physicalDeviceAccelerationStructureFeatures;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = nullptr;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(m_requiredDeviceExtensionNames.size());
    createInfo.ppEnabledExtensionNames = m_requiredDeviceExtensionNames.data();
    createInfo.pNext = &physicalDeviceFeatures2;

    VkDevice device{};
    if (vkCreateDevice(m_pInstance->GetVkPhysicalDevice(), &createInfo, nullptr, &device) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create logical device!");
    }
    else
    {
        m_pInstance->SetVkDevice(device);

        VK_KHR_buffer_device_address_enabled = true;
    }

    return result;
}

GrResult CarRayTracingApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult CarRayTracingApplication::Init()
{
    GrResult result = Application::Init();

    m_pCamera = std::make_unique<Camera>();
    m_pCamera->Init(m_window);
    m_pCamera->SetAspectRatio((float)m_windowSize.width / (float)m_windowSize.height);

    Swapchain swapchain = GetSwapchain();
//
//    static constexpr uint8_t vertex_shader[] =
//#include "shaders/carraytracing/vertex.glsl"
//        ;
//     
    //std::vector<uint8_t> vertex_shader_code(
    //    vertex_shader, vertex_shader + sizeof(vertex_shader) - 1);

//
//    static constexpr uint8_t fragment_shader[] =
//#include "shaders/carraytracing/fragment.glsl"
//        ;

    //std::vector<uint8_t> fragment_shader_code(
    //    fragment_shader, fragment_shader + sizeof(fragment_shader) - 1);

    static constexpr uint8_t closesthit_shader[] =
#include "shaders/carraytracing/closesthit.glsl"
        ;

    std::vector<uint8_t> closesthit_shader_code(
        closesthit_shader, closesthit_shader + sizeof(closesthit_shader) - 1);

    static constexpr uint8_t miss_shader[] =
#include "shaders/carraytracing/miss.glsl"
        ;

    std::vector<uint8_t> miss_shader_code(
        miss_shader, miss_shader + sizeof(miss_shader) - 1);

    static constexpr uint8_t raygen_shader[] =
#include "shaders/carraytracing/raygen.glsl"
        ;

    std::vector<uint8_t> raygen_shader_code(
        raygen_shader, raygen_shader + sizeof(raygen_shader) - 1);

    //Shader vertex(GrShaderStage::GR_VERTEX, GrShaderType::GR_GLSL, vertex_shader);
    //Shader fragment(GrShaderStage::GR_FRAGMENT, GrShaderType::GR_GLSL, fragment_shader);

    //m_graphicsProgram.Init(m_pInstance);
    //m_graphicsProgram.AddShader(vertex);
    //m_graphicsProgram.AddShader(fragment);
    //m_graphicsProgram.Compile();
    //m_graphicsProgram.CreateShaderModules();

    Shader closestHit(GrShaderStage::GR_CLOSEST_HIT, GrShaderType::GR_GLSL, closesthit_shader_code);
    Shader miss(GrShaderStage::GR_MISS, GrShaderType::GR_GLSL, miss_shader_code);
    Shader raygen(GrShaderStage::GR_RAYGEN, GrShaderType::GR_GLSL, raygen_shader_code);

    m_rayTracingProgram.Init(m_pInstance);
    m_rayTracingProgram.AddShader(closestHit);
    m_rayTracingProgram.AddShader(miss);
    m_rayTracingProgram.AddShader(raygen);
    m_rayTracingProgram.Compile();
    m_rayTracingProgram.CreateShaderModules();

    m_queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

    //int width = 0, height = 0, data_size = 0;
    //std::vector<char> imageData = GetImageData("../../../../data/Lotus_dif_blackvrsn.png", 4, &width, &height, nullptr, &data_size);

    //m_image.Init(m_pInstance, width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_COLOR_BIT);
    //m_image.CreateStagingBuffer(data_size);
    //m_image.Upload(m_queue, imageData.data(), data_size, width, height);

    //std::vector<VkDescriptorPoolSize> graphicsPoolSizes =
    //{
    //    { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1 },
    //    { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 }
    //};
    //m_graphicsDescriptorPool.Init(m_pInstance, graphicsPoolSizes, 2); //2 here because 1 for Image and 1 for Transform descriptor sets

    //std::shared_ptr<ImageDescriptorSet> pImageDescriptorSet = std::make_shared<ImageDescriptorSet>();
    //pImageDescriptorSet->Init(m_pInstance, m_graphicsDescriptorPool);
    //pImageDescriptorSet->Update(m_image);

    //m_uniformBuffer.Init(m_pInstance, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

    //std::shared_ptr<TransformDescriptorSet> pTransformDescriptorSet = std::make_shared<TransformDescriptorSet>();
    //pTransformDescriptorSet->Init(m_pInstance, m_graphicsDescriptorPool);
    //pTransformDescriptorSet->Update(m_uniformBuffer);

    //m_graphicsDescriptorSets.push_back(pImageDescriptorSet);
    //m_graphicsDescriptorSets.push_back(pTransformDescriptorSet);

    m_renderpass.Init(m_pInstance, swapchain.GetVkFormat());

    //m_graphicsPipeline.Init(m_pInstance,
    //    m_renderpass,
    //    swapchain.GetVkExtent2D(),
    //    &m_graphicsProgram,
    //    VertexInput::getAttributeDescriptions(),
    //    VertexInput::getBindingDescription(),
    //    m_graphicsDescriptorSets,
    //    VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
    //m_graphicsPipeline.Create();

    // Get the acceleration structure features, which we'll need later on in the sample
    m_accelerationStructureFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    VkPhysicalDeviceFeatures2 device_features{};
    device_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    device_features.pNext = &m_accelerationStructureFeatures;
    vkGetPhysicalDeviceFeatures2(m_pInstance->GetVkPhysicalDevice(), &device_features);

    CreateStorageImage();
    CreateBottomLevelAccelerationStructure();
    CreateTopLevelAccelerationStructure();
    CreateUniformBuffer();

    std::vector<VkDescriptorPoolSize> rayTracingPoolSizes = {
        {VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 1},
        {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1},
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1}
    };
    m_rayTracingDescriptorPool.Init(m_pInstance, rayTracingPoolSizes, 1);

    std::shared_ptr<RayTracingDescriptorSet> pRayTracingDescriptorSet = std::make_shared<RayTracingDescriptorSet>();
    pRayTracingDescriptorSet->Init(m_pInstance, m_rayTracingDescriptorPool);
    pRayTracingDescriptorSet->Update(m_topLevelAccelerationStructure, m_storageImage, m_uniformBuffer);

    m_rayTracingDescriptorSets.push_back(pRayTracingDescriptorSet);

    m_rayTracingPipeline.Init(m_pInstance,
        &m_rayTracingProgram,
        m_rayTracingDescriptorSets);
    m_rayTracingPipeline.Create();

    for (uint32_t i = 0; i < swapchain.GetImageViewCount(); i++)
    {
        Image depthImage;
        VkFormat depthFormat = m_pInstance->FindDepthFormat();
        depthImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

        //probably need to be consistent with pointers
        std::vector<VkImageView> attachments = {
            swapchain.GetVkImageView(i),
            depthImage.GetImageView()
        };
        m_depthImages.push_back(depthImage);

        Framebuffer framebuffer;
        framebuffer.Init(m_pInstance, swapchain.GetVkExtent2D(), m_renderpass.GetVkRenderPass(), attachments);
        m_framebuffers.push_back(framebuffer);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, m_queue);
        m_commandBuffers.push_back(commandBuffer);

        commandBuffer.Begin();

        VkStridedDeviceAddressRegionKHR raygen_shader_sbt_entry{};

        VkStridedDeviceAddressRegionKHR miss_shader_sbt_entry{};

        VkStridedDeviceAddressRegionKHR hit_shader_sbt_entry{};

        VkStridedDeviceAddressRegionKHR callable_shader_sbt_entry{};
        result = m_rayTracingPipeline.GetShaderBindingTableEntries(
            raygen_shader_sbt_entry,
            miss_shader_sbt_entry,
            hit_shader_sbt_entry,
            callable_shader_sbt_entry);

        m_rayTracingPipeline.Bind(&commandBuffer);

        /*
            Dispatch the ray tracing commands
        */

        std::vector<VkDescriptorSet> vkDescriptorSets;
        for (auto descriptorSet : m_rayTracingDescriptorSets)
        {
            vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        }
        vkCmdBindDescriptorSets(commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, m_rayTracingPipeline.GetVkPipelineLayout(), 0, 1, vkDescriptorSets.data(), 0, nullptr);

        vkCmdTraceRaysKHR(
            commandBuffer.GetVkCommandBuffer(),
            &raygen_shader_sbt_entry,
            &miss_shader_sbt_entry,
            &hit_shader_sbt_entry,
            &callable_shader_sbt_entry,
            m_windowSize.width,
            m_windowSize.height,
            1);

        VkImageSubresourceRange subresource_range = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };

        // Prepare current swap chain image as transfer destination
        swapchain.GetImage(i).LayoutTransition(commandBuffer,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

        // Prepare ray tracing output image as transfer source
        m_storageImage.LayoutTransition(commandBuffer,
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            {},
            VK_ACCESS_TRANSFER_READ_BIT,
            VK_IMAGE_LAYOUT_GENERAL,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            subresource_range);

        //m_renderpass.Begin(swapchain.GetVkExtent2D(), &framebuffer, &commandBuffer);
        //m_graphicsPipeline.Bind(&commandBuffer);
        //BindVertexBuffer(&commandBuffer, m_vertexPositionBuffer, (VkDeviceSize)0 );
        //BindVertexBuffer(&commandBuffer, m_vertexNormalBuffer, (VkDeviceSize)0);
        //BindVertexBuffer(&commandBuffer, m_vertexTexCoordBuffer, (VkDeviceSize)0);
        //BindIndexBuffer(&commandBuffer, m_indexBuffer, 0);

        //std::vector<VkDescriptorSet> vkDescriptorSets;
        //for (auto descriptorSet : m_graphicsDescriptorSets)
        //{
        //    vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        //}
        //vkCmdBindDescriptorSets(commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_graphicsPipeline.GetVkPipelineLayout(), 0, 2, vkDescriptorSets.data(), 0, nullptr);
        //vkCmdDrawIndexed(commandBuffer.GetVkCommandBuffer(), m_indicesCount, 1, 0, 0, 0);
        //m_renderpass.End(&commandBuffer);

        VkImageCopy copy_region{};
        copy_region.srcSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
        copy_region.srcOffset = { 0, 0, 0 };
        copy_region.dstSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
        copy_region.dstOffset = { 0, 0, 0 };
        copy_region.extent = { m_windowSize.width, m_windowSize.height, 1 };
        vkCmdCopyImage(commandBuffer.GetVkCommandBuffer(), m_storageImage.GetVkImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            swapchain.GetImage(i).GetVkImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_region);

        // Transition swap chain image back for presentation
        swapchain.GetImage(i).LayoutTransition(commandBuffer,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

        // Transition ray tracing output image back to general layout
        m_storageImage.LayoutTransition(commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
            VK_ACCESS_TRANSFER_READ_BIT,
            {},
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            VK_IMAGE_LAYOUT_GENERAL,
            subresource_range);
        commandBuffer.End();
    }

    return result;
}

GrResult CarRayTracingApplication::Run()
{
    GrResult result = GR_SUCCESS;

    m_pCamera->SetPosition(glm::vec3(0.0f, 0.0f, -100.0f));
    m_pCamera->SetUpVector(glm::vec3(0.0f, -1.0f, 0.0f));

    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetWindowSizeCallback(m_window, &WindowSizeCallback);
    glfwSetKeyCallback(m_window, [](GLFWwindow * window, int key, int scancode, int action, int mods)
    {
        if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS))
        {
            g_terminate = true;
            //glfwDestroyWindow(window);
        }
    });

    glfwPollEvents();
    std::shared_ptr<UniformBufferObject> pUBO = m_pCamera->GetUniformBufferObject();

    while (!glfwWindowShouldClose(m_window))
    {

        CreateBottomLevelAccelerationStructure();
        CreateTopLevelAccelerationStructure();

        if (g_terminate)
            break;

        glfwPollEvents();

        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

        uint32_t imageIndex;
        vkAcquireNextImageKHR(m_pInstance->GetVkDevice(), GetSwapchain().GetVkSwapchain(), UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);
        
        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_inFlightImages[imageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightImages[imageIndex], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        m_inFlightImages[imageIndex] = m_inFlightFences[m_currentFrame];

        m_pCamera->SetForwardVector(glm::vec3(0.0f, 0.0f, 1.0f)); // shouldn't be a need to send this every frame
        m_pCamera->Update();
        m_uniformBuffer.Update((char*)pUBO.get(), 0, sizeof(UniformBufferObject));

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = m_commandBuffers[imageIndex].GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;

        VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame]);

        if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[m_currentFrame]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkSwapchainKHR swapChains[] = { GetSwapchain().GetVkSwapchain() };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr; // Optional

        vkQueuePresentKHR(m_queue.GetVkQueue(), &presentInfo);

        //This is lazy: vkQueueWaitIdle(queue.GetVkQueue());

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    return result;
}

GrResult CarRayTracingApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    for (auto framebuffer : m_framebuffers)
    {
        framebuffer.Cleanup();
    }
    m_vertexPositionBuffer.Cleanup();
    //m_vertexNormalBuffer.Cleanup();
    //m_vertexTexCoordBuffer.Cleanup();
    m_indexBuffer.Cleanup();
    for (auto depthImage : m_depthImages)
    {
        depthImage.Cleanup();
    }
    for (auto commandBuffer : m_commandBuffers)
    {
        commandBuffer.Cleanup(m_queue);
    }
    //m_graphicsPipeline.Cleanup();
    m_rayTracingPipeline.Cleanup();
    m_renderpass.Cleanup();
    //for (auto descriptorSet : m_graphicsDescriptorSets)
    //{
    //    descriptorSet->Cleanup();
    //}
    for (auto descriptorSet : m_rayTracingDescriptorSets)
    {
        descriptorSet->Cleanup();
    }
    //m_graphicsDescriptorPool.Cleanup();
    m_rayTracingDescriptorPool.Cleanup();
    //m_image.Cleanup();
    m_queue.Cleanup();
    //m_graphicsProgram.Cleanup();
    m_rayTracingProgram.Cleanup();
    result = Application::Cleanup();
    return result;
}

GrResult CarRayTracingApplication::CreateStorageImage()
{
    m_storageImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

    CommandBuffer commandBuffer;
    commandBuffer.Init(m_pInstance, m_queue);
    commandBuffer.Begin();

    m_storageImage.LayoutTransition(commandBuffer,
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        {},
        {},
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_GENERAL,
        { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 });

    commandBuffer.End();

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
    submitInfo.pCommandBuffers = &vkCommandBuffer;

    // Create fence to ensure that the command buffer has finished executing
    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = 0;

    VkFence fence;
    vkCreateFence(m_pInstance->GetVkDevice(), &fenceInfo, nullptr, &fence);

    // Submit to the queue
    if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, fence) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to submit to the queue!");
    }

    // Wait for the fence to signal that command buffer has finished executing
    vkWaitForFences(m_pInstance->GetVkDevice(), 1, &fence, VK_TRUE, 100000000000); //DEFAULT_FENCE_TIMEOUT

    vkDestroyFence(m_pInstance->GetVkDevice(), fence, nullptr);
    
    commandBuffer.Cleanup(m_queue);
} 

GrResult CarRayTracingApplication::CreateBottomLevelAccelerationStructure()
{
    GrResult result = GR_SUCCESS;

    std::string mesh_file = "../../../../data/car2.obj";
    // Create an instance of the Importer class
    Assimp::Importer importer;

    // And have it read the given file with some example postprocessing
    // Usually - if speed is not the most important aspect for you - you'll
    // probably to request more postprocessing than we do in this example.
    const aiScene* scene = importer.ReadFile(mesh_file,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_SortByPType |
        aiProcess_ConvertToLeftHanded);

    // If the import failed, report it
    if (nullptr == scene) {
        result = GrResult::GR_ERROR;
        return result;
    }

    //vertex
    std::vector<VertexPosition> vertices(scene->mMeshes[0]->mNumVertices);
    memcpy(vertices.data(), scene->mMeshes[0]->mVertices, vertices.size() * sizeof(VertexPosition));

    //std::vector<VertexNormal> normals(scene->mMeshes[0]->mNumVertices);
    //memcpy(normals.data(), scene->mMeshes[0]->mNormals, normals.size() * sizeof(VertexNormal));

    //std::vector<VertexTexCoord> texCoords(scene->mMeshes[0]->mNumVertices);
    //memcpy(texCoords.data(), scene->mMeshes[0]->mTextureCoords[0], texCoords.size() * sizeof(VertexTexCoord));

    struct aiFace;
    std::vector<uint32_t> indices = {};
    for (int face = 0; face < scene->mMeshes[0]->mNumFaces; face++)
    {
        for (int index = 0; index < scene->mMeshes[0]->mFaces[face].mNumIndices; index++)
        {
            indices.push_back(scene->mMeshes[0]->mFaces[face].mIndices[index]);
        }
    }
    m_indicesCount = indices.size();

    // Note that the buffer usage flags for buffers consumed by the bottom level acceleration structure require special flags
    const VkBufferUsageFlags buffer_usage_flags = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

    m_vertexPositionBuffer.Init(m_pInstance, vertices.size() * sizeof(VertexPosition), buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, 0, VMA_MEMORY_USAGE_CPU_TO_GPU);
    m_vertexPositionBuffer.Update((char*)vertices.data(), 0, vertices.size() * sizeof(VertexPosition));

    //m_vertexNormalBuffer.Init(m_pInstance, normals.size() * sizeof(VertexNormal), buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, 0, VMA_MEMORY_USAGE_CPU_TO_GPU);
    //m_vertexNormalBuffer.Update((char*)normals.data(), 0, normals.size() * sizeof(VertexNormal));

    //m_vertexTexCoordBuffer.Init(m_pInstance, texCoords.size() * sizeof(VertexTexCoord), buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, 0, VMA_MEMORY_USAGE_CPU_TO_GPU);
    //m_vertexTexCoordBuffer.Update((char*)texCoords.data(), 0, texCoords.size() * sizeof(VertexTexCoord));

    m_indexBuffer.Init(m_pInstance, indices.size() * sizeof(uint32_t), buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, 0, VMA_MEMORY_USAGE_CPU_TO_GPU);
    m_indexBuffer.Update((char*)indices.data(), 0, indices.size() * sizeof(uint32_t));

    // Setup a single transformation matrix that can be used to transform the whole geometry for a single bottom level acceleration structure
    VkTransformMatrixKHR vkTransformMatrix = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f };
    // vkCmdBuildAccelerationStructuresKHR - pInfos - 03810) // it must be aligned to 16
    m_transformationMatrixBuffer.Init(m_pInstance, sizeof(VkTransformMatrixKHR), buffer_usage_flags, VK_SHARING_MODE_EXCLUSIVE, 16);
    m_transformationMatrixBuffer.Update((char*)vkTransformMatrix.matrix, 0, sizeof(VkTransformMatrixKHR));

    VkDeviceOrHostAddressConstKHR vertex_position_buffer_device_address{};
    //VkDeviceOrHostAddressConstKHR vertex_normal_buffer_device_address{};
    //VkDeviceOrHostAddressConstKHR vertex_texcoord_buffer_device_address{};
    VkDeviceOrHostAddressConstKHR index_buffer_device_address{};
    VkDeviceOrHostAddressConstKHR transform_matrix_buffer_device_address{};
    VkDeviceOrHostAddressConstKHR scratch_buffer_device_address{};

    VkBufferDeviceAddressInfoKHR vertex_position_buffer_device_address_info{};
    vertex_position_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    vertex_position_buffer_device_address_info.buffer = m_vertexPositionBuffer.GetVkBuffer();
    vertex_position_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &vertex_position_buffer_device_address_info);

    //VkBufferDeviceAddressInfoKHR vertex_normal_buffer_device_address_info{};
    //vertex_normal_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    //vertex_normal_buffer_device_address_info.buffer = m_vertexNormalBuffer.GetVkBuffer();
    //vertex_normal_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &vertex_normal_buffer_device_address_info);

    //VkBufferDeviceAddressInfoKHR vertex_texcoord_buffer_device_address_info{};
    //vertex_texcoord_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    //vertex_texcoord_buffer_device_address_info.buffer = m_vertexTexCoordBuffer.GetVkBuffer();
    //vertex_texcoord_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &vertex_texcoord_buffer_device_address_info);

    VkBufferDeviceAddressInfoKHR index_buffer_device_address_info{};
    index_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    index_buffer_device_address_info.buffer = m_indexBuffer.GetVkBuffer();
    index_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &index_buffer_device_address_info);

    VkBufferDeviceAddressInfoKHR transform_matrix_buffer_device_address_info{};
    transform_matrix_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    transform_matrix_buffer_device_address_info.buffer = m_transformationMatrixBuffer.GetVkBuffer();
    transform_matrix_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &transform_matrix_buffer_device_address_info);

    // The bottom level acceleration structure contains one set of triangles as the input geometry
    VkAccelerationStructureGeometryKHR acceleration_structure_geometry{};
    acceleration_structure_geometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    acceleration_structure_geometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    acceleration_structure_geometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    acceleration_structure_geometry.geometry.triangles.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    acceleration_structure_geometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
    acceleration_structure_geometry.geometry.triangles.vertexData = vertex_position_buffer_device_address;
    acceleration_structure_geometry.geometry.triangles.maxVertex = vertices.size() - 1;
    acceleration_structure_geometry.geometry.triangles.vertexStride = sizeof(VertexPosition);
    acceleration_structure_geometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
    acceleration_structure_geometry.geometry.triangles.indexData = index_buffer_device_address;
    acceleration_structure_geometry.geometry.triangles.transformData = transform_matrix_buffer_device_address;

    // Get the size requirements for buffers involved in the acceleration structure build process
    VkAccelerationStructureBuildGeometryInfoKHR acceleration_structure_build_geometry_info{};
    acceleration_structure_build_geometry_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    acceleration_structure_build_geometry_info.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    acceleration_structure_build_geometry_info.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    acceleration_structure_build_geometry_info.geometryCount = 1;
    acceleration_structure_build_geometry_info.pGeometries = &acceleration_structure_geometry;

    const uint32_t primitive_count = 1;

    VkAccelerationStructureBuildSizesInfoKHR acceleration_structure_build_sizes_info{};
    acceleration_structure_build_sizes_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        m_pInstance->GetVkDevice(),
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &acceleration_structure_build_geometry_info,
        &primitive_count,
        &acceleration_structure_build_sizes_info);

    std::shared_ptr<Buffer> bottomLevelAccelerationStructureBuffer = std::make_shared<Buffer>();
    bottomLevelAccelerationStructureBuffer->Init(m_pInstance,
        acceleration_structure_build_sizes_info.accelerationStructureSize,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        VMA_MEMORY_USAGE_GPU_ONLY);
    m_bottomLevelAccelerationStructure.SetBuffer(bottomLevelAccelerationStructureBuffer);

    // Create the acceleration structure
    VkAccelerationStructureCreateInfoKHR acceleration_structure_create_info{};
    acceleration_structure_create_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    acceleration_structure_create_info.buffer = m_bottomLevelAccelerationStructure.GetBuffer()->GetVkBuffer();
    acceleration_structure_create_info.size = acceleration_structure_build_sizes_info.accelerationStructureSize;
    acceleration_structure_create_info.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    VkAccelerationStructureKHR bottomLevelAccelerationStructure;
    vkCreateAccelerationStructureKHR(m_pInstance->GetVkDevice(), &acceleration_structure_create_info, nullptr, &bottomLevelAccelerationStructure);
    m_bottomLevelAccelerationStructure.SetVkAccelerationStructure(bottomLevelAccelerationStructure);
    // The actual build process starts here

    // Create a scratch buffer as a temporary storage for the acceleration structure build
    Buffer scratchBuffer = {};
    scratchBuffer.Init(m_pInstance, 
        acceleration_structure_build_sizes_info.buildScratchSize,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        m_minAccelerationStructureScratchOffsetAlignment);

    VkBufferDeviceAddressInfoKHR scratch_buffer_device_address_info{};
    scratch_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    scratch_buffer_device_address_info.buffer = scratchBuffer.GetVkBuffer();
    scratch_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &scratch_buffer_device_address_info);

    VkAccelerationStructureBuildGeometryInfoKHR acceleration_build_geometry_info{};
    acceleration_build_geometry_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    acceleration_build_geometry_info.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    acceleration_build_geometry_info.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    acceleration_build_geometry_info.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    acceleration_build_geometry_info.dstAccelerationStructure = m_bottomLevelAccelerationStructure.GetVkAccelerationStructure();
    acceleration_build_geometry_info.geometryCount = 1;
    acceleration_build_geometry_info.pGeometries = &acceleration_structure_geometry;
    acceleration_build_geometry_info.scratchData.deviceAddress = scratch_buffer_device_address.deviceAddress;

    VkAccelerationStructureBuildRangeInfoKHR acceleration_structure_build_range_info;
    acceleration_structure_build_range_info.primitiveCount = 1;
    acceleration_structure_build_range_info.primitiveOffset = 0;
    acceleration_structure_build_range_info.firstVertex = 0;
    acceleration_structure_build_range_info.transformOffset = 0;
    std::vector<VkAccelerationStructureBuildRangeInfoKHR*> acceleration_build_structure_range_infos = { &acceleration_structure_build_range_info };

    // Build the acceleration structure on the device via a one-time command buffer submission
    // Some implementations may support acceleration structure building on the host (VkPhysicalDeviceAccelerationStructureFeaturesKHR->accelerationStructureHostCommands), but we prefer device builds
    CommandBuffer commandBuffer;
    commandBuffer.Init(m_pInstance, m_queue);
    commandBuffer.Begin();
    vkCmdBuildAccelerationStructuresKHR(
        commandBuffer.GetVkCommandBuffer(),
        1,
        &acceleration_build_geometry_info,
        acceleration_build_structure_range_infos.data());
    commandBuffer.End();

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
    submitInfo.pCommandBuffers = &vkCommandBuffer;

    // Create fence to ensure that the command buffer has finished executing
    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = 0;

    VkFence fence;
    vkCreateFence(m_pInstance->GetVkDevice(), &fenceInfo, nullptr, &fence);

    // Submit to the queue
    if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, fence) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to submit to the queue!");
    }

    // Wait for the fence to signal that command buffer has finished executing
    vkWaitForFences(m_pInstance->GetVkDevice(), 1, &fence, VK_TRUE, 100000000000); //DEFAULT_FENCE_TIMEOUT

    vkDestroyFence(m_pInstance->GetVkDevice(), fence, nullptr);

    commandBuffer.Cleanup(m_queue);
    scratchBuffer.Cleanup();
    // Get the bottom acceleration structure's handle, which will be used during the top level acceleration build
    VkAccelerationStructureDeviceAddressInfoKHR acceleration_device_address_info{};
    acceleration_device_address_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    acceleration_device_address_info.accelerationStructure = m_bottomLevelAccelerationStructure.GetVkAccelerationStructure();
    VkDeviceAddress deviceAddress = vkGetAccelerationStructureDeviceAddressKHR(m_pInstance->GetVkDevice(), &acceleration_device_address_info);
    m_bottomLevelAccelerationStructure.SetVkDeviceAddress(deviceAddress);

    return result;
}

GrResult CarRayTracingApplication::CreateTopLevelAccelerationStructure()
{
    GrResult result = GR_SUCCESS;

    VkTransformMatrixKHR transform_matrix = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f };

    VkAccelerationStructureInstanceKHR acceleration_structure_instance{};
    acceleration_structure_instance.transform = transform_matrix;
    acceleration_structure_instance.instanceCustomIndex = 0;
    acceleration_structure_instance.mask = 0xFF;
    acceleration_structure_instance.instanceShaderBindingTableRecordOffset = 0;
    acceleration_structure_instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
    acceleration_structure_instance.accelerationStructureReference = m_bottomLevelAccelerationStructure.GetVkDeviceAddress();

    m_instancesBuffer.Init(m_pInstance,
        sizeof(VkAccelerationStructureInstanceKHR),
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        16, // VUID-vkCmdBuildAccelerationStructuresKHR-pInfos-03715
        VMA_MEMORY_USAGE_CPU_TO_GPU);

    m_instancesBuffer.Update((char*)&acceleration_structure_instance, 0, sizeof(VkAccelerationStructureInstanceKHR));

    VkDeviceOrHostAddressConstKHR instance_data_device_address{};
    VkBufferDeviceAddressInfoKHR buffer_device_address_info{};
    buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    buffer_device_address_info.buffer = m_instancesBuffer.GetVkBuffer();
    instance_data_device_address.deviceAddress = vkGetBufferDeviceAddressKHR(m_pInstance->GetVkDevice(), &buffer_device_address_info);

    // The top level acceleration structure contains (bottom level) instance as the input geometry
    VkAccelerationStructureGeometryKHR acceleration_structure_geometry{};
    acceleration_structure_geometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    acceleration_structure_geometry.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    acceleration_structure_geometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    acceleration_structure_geometry.geometry.instances.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    acceleration_structure_geometry.geometry.instances.arrayOfPointers = VK_FALSE;
    acceleration_structure_geometry.geometry.instances.data = instance_data_device_address;

    // Get the size requirements for buffers involved in the acceleration structure build process
    VkAccelerationStructureBuildGeometryInfoKHR acceleration_structure_build_geometry_info{};
    acceleration_structure_build_geometry_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    acceleration_structure_build_geometry_info.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    acceleration_structure_build_geometry_info.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    acceleration_structure_build_geometry_info.geometryCount = 1;
    acceleration_structure_build_geometry_info.pGeometries = &acceleration_structure_geometry;

    const uint32_t primitive_count = 1;

    VkAccelerationStructureBuildSizesInfoKHR acceleration_structure_build_sizes_info{};
    acceleration_structure_build_sizes_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        m_pInstance->GetVkDevice(), VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &acceleration_structure_build_geometry_info,
        &primitive_count,
        &acceleration_structure_build_sizes_info);

    // Create a buffer to hold the acceleration structure
    std::shared_ptr<Buffer> topLevelAccelerationStructureBuffer = std::make_shared<Buffer>();
    topLevelAccelerationStructureBuffer->Init(
        m_pInstance,
        acceleration_structure_build_sizes_info.accelerationStructureSize,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        VMA_MEMORY_USAGE_GPU_ONLY);
    m_topLevelAccelerationStructure.SetBuffer(topLevelAccelerationStructureBuffer);

    // Create the acceleration structure
    VkAccelerationStructureCreateInfoKHR acceleration_structure_create_info{};
    acceleration_structure_create_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    acceleration_structure_create_info.buffer = m_topLevelAccelerationStructure.GetBuffer()->GetVkBuffer();
    acceleration_structure_create_info.size = acceleration_structure_build_sizes_info.accelerationStructureSize;
    acceleration_structure_create_info.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    VkAccelerationStructureKHR topLevelAccelerationStructure;
    vkCreateAccelerationStructureKHR(m_pInstance->GetVkDevice(), &acceleration_structure_create_info, nullptr, &topLevelAccelerationStructure);
    m_topLevelAccelerationStructure.SetVkAccelerationStructure(topLevelAccelerationStructure);

    // The actual build process starts here
    // Create a scratch buffer as a temporary storage for the acceleration structure build
    Buffer scratchBuffer = {};
    scratchBuffer.Init(m_pInstance,
        acceleration_structure_build_sizes_info.buildScratchSize,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_SHARING_MODE_EXCLUSIVE,
        m_minAccelerationStructureScratchOffsetAlignment);

    VkBufferDeviceAddressInfoKHR scratch_buffer_device_address_info{};
    scratch_buffer_device_address_info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    scratch_buffer_device_address_info.buffer = scratchBuffer.GetVkBuffer();

    VkDeviceOrHostAddressConstKHR scratch_buffer_device_address{};
    scratch_buffer_device_address.deviceAddress = vkGetBufferDeviceAddress(m_pInstance->GetVkDevice(), &scratch_buffer_device_address_info);

    VkAccelerationStructureBuildGeometryInfoKHR acceleration_build_geometry_info{};
    acceleration_build_geometry_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    acceleration_build_geometry_info.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    acceleration_build_geometry_info.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    acceleration_build_geometry_info.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    acceleration_build_geometry_info.dstAccelerationStructure = m_topLevelAccelerationStructure.GetVkAccelerationStructure();
    acceleration_build_geometry_info.geometryCount = 1;
    acceleration_build_geometry_info.pGeometries = &acceleration_structure_geometry;
    acceleration_build_geometry_info.scratchData.deviceAddress = scratch_buffer_device_address.deviceAddress;

    VkAccelerationStructureBuildRangeInfoKHR acceleration_structure_build_range_info;
    acceleration_structure_build_range_info.primitiveCount = 1;
    acceleration_structure_build_range_info.primitiveOffset = 0;
    acceleration_structure_build_range_info.firstVertex = 0;
    acceleration_structure_build_range_info.transformOffset = 0;
    std::vector<VkAccelerationStructureBuildRangeInfoKHR*> acceleration_build_structure_range_infos = { &acceleration_structure_build_range_info };

    // Build the acceleration structure on the device via a one-time command buffer submission
    // Some implementations may support acceleration structure building on the host (VkPhysicalDeviceAccelerationStructureFeaturesKHR->accelerationStructureHostCommands), but we prefer device builds

    CommandBuffer commandBuffer;
    commandBuffer.Init(m_pInstance, m_queue);
    commandBuffer.Begin();
    vkCmdBuildAccelerationStructuresKHR(
        commandBuffer.GetVkCommandBuffer(),
        1,
        &acceleration_build_geometry_info,
        acceleration_build_structure_range_infos.data());
    commandBuffer.End();

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
    submitInfo.pCommandBuffers = &vkCommandBuffer;

    // Create fence to ensure that the command buffer has finished executing
    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = 0;

    VkFence fence;
    vkCreateFence(m_pInstance->GetVkDevice(), &fenceInfo, nullptr, &fence);

    // Submit to the queue
    if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, fence) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to submit to the queue!");
    }

    // Wait for the fence to signal that command buffer has finished executing
    vkWaitForFences(m_pInstance->GetVkDevice(), 1, &fence, VK_TRUE, 100000000000); //DEFAULT_FENCE_TIMEOUT

    vkDestroyFence(m_pInstance->GetVkDevice(), fence, nullptr);

    // Get the top acceleration structure's handle, which will be used to setup it's descriptor
    VkAccelerationStructureDeviceAddressInfoKHR acceleration_device_address_info{};
    acceleration_device_address_info.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    acceleration_device_address_info.accelerationStructure = m_topLevelAccelerationStructure.GetVkAccelerationStructure();
    VkDeviceAddress deviceAddress = vkGetAccelerationStructureDeviceAddressKHR(m_pInstance->GetVkDevice(), &acceleration_device_address_info);
    m_topLevelAccelerationStructure.SetVkDeviceAddress(deviceAddress);

    return result;
}

GrResult CarRayTracingApplication::CreateUniformBuffer()
{
    GrResult result = GR_SUCCESS;
    m_uniformBuffer.Init(m_pInstance, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE, 0, VMA_MEMORY_USAGE_CPU_TO_GPU);
    return result;
}

std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size) {
    std::vector<char> file_data = GetBinaryFileContents(filename);
    if (file_data.size() == 0) {
        return std::vector<char>();
    }

    int tmp_width = 0, tmp_height = 0, tmp_components = 0;
    unsigned char* image_data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(&file_data[0]), static_cast<int>(file_data.size()), &tmp_width, &tmp_height, &tmp_components, requested_components);
    if ((image_data == nullptr) ||
        (tmp_width <= 0) ||
        (tmp_height <= 0) ||
        (tmp_components <= 0)) {
        std::cout << "Could not read image data!" << std::endl;
        return std::vector<char>();
    }

    int size = (tmp_width) * (tmp_height) * (requested_components <= 0 ? tmp_components : requested_components);
    if (data_size) {
        *data_size = size;
    }
    if (width) {
        *width = tmp_width;
    }
    if (height) {
        *height = tmp_height;
    }
    if (components) {
        *components = tmp_components;
    }

    std::vector<char> output(size);
    memcpy(&output[0], image_data, size);

    stbi_image_free(image_data);
    return output;
}

std::vector<char> GetBinaryFileContents(std::string const& filename) {

    std::ifstream file(filename, std::ios::binary);
    if (file.fail()) {
        std::cout << "Could not open \"" << filename << "\" file!" << std::endl;
        return std::vector<char>();
    }

    std::streampos begin, end;
    begin = file.tellg();
    file.seekg(0, std::ios::end);
    end = file.tellg();

    std::vector<char> result(static_cast<size_t>(end - begin));
    file.seekg(0, std::ios::beg);
    file.read(&result[0], end - begin);
    file.close();

    return result;
}

void WindowSizeCallback(GLFWwindow* window, int width, int height)
{
    //glViewport(0, 0, width, height);
}
