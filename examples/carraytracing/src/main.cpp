#include <string>

#include <carraytracingapplication.h>

void main()
{
    std::string name = "Car";
    CarRayTracingApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}