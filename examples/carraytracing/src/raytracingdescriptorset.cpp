#include "RayTracingDescriptorSet.h"

RayTracingDescriptorSet::RayTracingDescriptorSet()
{
}

GrResult RayTracingDescriptorSet::Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool)
{
    GrResult result = DescriptorSet::Init(pInstance, descriptorPool);

    // Slot for binding top level acceleration structures to the ray generation shader
    VkDescriptorSetLayoutBinding acceleration_structure_layout_binding{};
    acceleration_structure_layout_binding.binding = 0;
    acceleration_structure_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    acceleration_structure_layout_binding.descriptorCount = 1;
    acceleration_structure_layout_binding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    VkDescriptorSetLayoutBinding result_image_layout_binding{};
    result_image_layout_binding.binding = 1;
    result_image_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    result_image_layout_binding.descriptorCount = 1;
    result_image_layout_binding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    VkDescriptorSetLayoutBinding uniform_buffer_binding{};
    uniform_buffer_binding.binding = 2;
    uniform_buffer_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniform_buffer_binding.descriptorCount = 1;
    uniform_buffer_binding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;

    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings = {
        acceleration_structure_layout_binding,
        result_image_layout_binding,
        uniform_buffer_binding };

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.bindingCount = static_cast<uint32_t>(descriptorSetLayoutBindings.size());
    descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

    if (vkCreateDescriptorSetLayout(m_pInstance->GetVkDevice(), &descriptorSetLayoutCreateInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create descriptor set layout!");
    }

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = m_descriptorPool.GetVkDescriptorPool();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &m_descriptorSetLayout;

    if (vkAllocateDescriptorSets(m_pInstance->GetVkDevice(), &descriptorSetAllocateInfo, &m_descriptorSet) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to allocate descriptor set!");
    }

    return result;
}

GrResult RayTracingDescriptorSet::Cleanup()
{
    GrResult result = GR_SUCCESS;
    //vkResetDescriptorPool will free all of the DescriptorSets in the pool. 
    vkDestroyDescriptorSetLayout(m_pInstance->GetVkDevice(), m_descriptorSetLayout, nullptr);
    return result;
}

GrResult RayTracingDescriptorSet::Update(AccelerationStructure accelerationStructure, Image storageImage, Buffer uniformBuffer)
{
    GrResult result = GR_SUCCESS;

    //VkDescriptorBufferInfo bufferInfo{};
    //bufferInfo.buffer = buffer.GetVkBuffer();
    //bufferInfo.offset = 0;
    //bufferInfo.range = buffer.GetVkSize();

    VkAccelerationStructureKHR vkAccelerationStructure = accelerationStructure.GetVkAccelerationStructure();

    // Setup the descriptor for binding our top level acceleration structure to the ray tracing shaders
    VkWriteDescriptorSetAccelerationStructureKHR descriptor_acceleration_structure_info{};
    descriptor_acceleration_structure_info.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
    descriptor_acceleration_structure_info.accelerationStructureCount = 1;
    descriptor_acceleration_structure_info.pAccelerationStructures = &vkAccelerationStructure;

    VkWriteDescriptorSet acceleration_structure_write{};
    acceleration_structure_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    acceleration_structure_write.dstSet = m_descriptorSet;
    acceleration_structure_write.dstBinding = 0;
    acceleration_structure_write.descriptorCount = 1;
    acceleration_structure_write.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    // The acceleration structure descriptor has to be chained via pNext
    acceleration_structure_write.pNext = &descriptor_acceleration_structure_info;

    VkDescriptorImageInfo image_descriptor{};
    image_descriptor.imageView = storageImage.GetImageView();
    image_descriptor.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    VkWriteDescriptorSet result_image_write{};
    result_image_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    result_image_write.dstSet = m_descriptorSet;
    result_image_write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    result_image_write.dstBinding = 1;
    result_image_write.pImageInfo = &image_descriptor;
    result_image_write.descriptorCount = 1;

    VkDescriptorBufferInfo buffer_descriptor{};
    buffer_descriptor.buffer = uniformBuffer.GetVkBuffer();
    buffer_descriptor.range = uniformBuffer.GetVkSize();
    buffer_descriptor.offset = 0;

    VkWriteDescriptorSet uniform_buffer_write{};
    uniform_buffer_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    uniform_buffer_write.dstSet = m_descriptorSet;
    uniform_buffer_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniform_buffer_write.dstBinding = 2;
    uniform_buffer_write.pBufferInfo = &buffer_descriptor;
    uniform_buffer_write.descriptorCount = 1;

    std::vector<VkWriteDescriptorSet> write_descriptor_sets = {
        acceleration_structure_write,
        result_image_write,
        uniform_buffer_write };

    vkUpdateDescriptorSets(m_pInstance->GetVkDevice(), static_cast<uint32_t>(write_descriptor_sets.size()), write_descriptor_sets.data(), 0, nullptr);
    return result;
}
