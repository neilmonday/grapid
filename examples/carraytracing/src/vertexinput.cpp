#include "vertexinput.h"

std::shared_ptr <std::vector <VkVertexInputBindingDescription> > VertexInput::getBindingDescription()
{
    std::shared_ptr <std::vector < VkVertexInputBindingDescription> > bindingDescriptions = std::make_shared<std::vector < VkVertexInputBindingDescription> >();
    VkVertexInputBindingDescription positionBindingDescription{};
    positionBindingDescription.binding = 0;
    positionBindingDescription.stride = sizeof(VertexPosition);
    positionBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    bindingDescriptions->push_back(positionBindingDescription);

    VkVertexInputBindingDescription normalBindingDescription{};
    normalBindingDescription.binding = 1;
    normalBindingDescription.stride = sizeof(VertexNormal);
    normalBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    bindingDescriptions->push_back(normalBindingDescription);

    VkVertexInputBindingDescription texCoordBindingDescription{};
    texCoordBindingDescription.binding = 2;
    texCoordBindingDescription.stride = sizeof(VertexTexCoord);
    texCoordBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    bindingDescriptions->push_back(texCoordBindingDescription);
    return bindingDescriptions;
}

std::shared_ptr<std::vector<VkVertexInputAttributeDescription> > VertexInput::getAttributeDescriptions()
{
    std::shared_ptr<std::vector<VkVertexInputAttributeDescription> > attributeDescriptions = std::make_shared<std::vector<VkVertexInputAttributeDescription> >();

    VkVertexInputAttributeDescription positionDescription;
    positionDescription.binding = 0;
    positionDescription.location = 0;
    positionDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionDescription.offset = offsetof(VertexPosition, position);
    attributeDescriptions->push_back(positionDescription);

    VkVertexInputAttributeDescription normalDescription;
    normalDescription.binding = 1;
    normalDescription.location = 1;
    normalDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    normalDescription.offset = offsetof(VertexNormal, normal);
    attributeDescriptions->push_back(normalDescription);

    VkVertexInputAttributeDescription texCoordDescription;
    texCoordDescription.binding = 2;
    texCoordDescription.location = 2;
    texCoordDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    texCoordDescription.offset = offsetof(VertexTexCoord, texCoord);
    attributeDescriptions->push_back(texCoordDescription);

    /*VkVertexInputAttributeDescription colorDescription;
    colorDescription.binding = 0;
    colorDescription.location = 1;
    colorDescription.format = VK_FORMAT_R32G32_SFLOAT;
    colorDescription.offset = offsetof(Vertex, uv);
    attributeDescriptions.push_back(colorDescription);*/

    return attributeDescriptions;
}
