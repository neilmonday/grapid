#ifndef CUBEAPPLICATION_H
#define CUBEAPPLICATION_H

#include <fstream>
#include <vector>

#include <application.h>
#include <shader.h>
#include <program.h>
#include <image.h>
#include <graphicspipeline.h>
#include <renderpass.h>
#include <camera.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "vertexinput.h"
#include "imagedescriptorset.h"
#include "transformdescriptorset.h"

std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size);
std::vector<char> GetBinaryFileContents(std::string const& filename);

class CubeApplication : public Application
{
public:
    CubeApplication(std::string name);

    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult LoadVolkDevice() override;

private:
    std::unique_ptr<Camera> m_pCamera{};

    Buffer m_uniformBuffer{};

    UniformBufferObject m_uniformBufferObject;
    std::vector<CommandBuffer> m_commandBuffers;
    Queue m_queue;
    Program m_program;
    Image m_image;
    std::vector<std::shared_ptr<DescriptorSet>> m_descriptorSets;
    std::vector<std::shared_ptr<Buffer> > m_vertexBuffers{};
    Buffer m_indexBuffer;
    Renderpass m_renderpass;
    GraphicsPipeline m_pipeline;
    std::vector<Framebuffer> m_framebuffers;
    std::vector<Image> m_depthImages{};
    DescriptorPool m_descriptorPool;
};

#endif //CUBEAPPLICATION_H
