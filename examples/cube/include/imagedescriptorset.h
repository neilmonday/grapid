#ifndef IMAGEDESCRIPTORSET_H
#define IMAGEDESCRIPTORSET_H

#include <descriptorset.h>

class ImageDescriptorSet : public DescriptorSet
{
public:
    ImageDescriptorSet();

    GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool) override;
    GrResult Cleanup() override;

    GrResult Update(Image image);
};

#endif //IMAGEDESCRIPTORSET_H
