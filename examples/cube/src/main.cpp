#include <string>

#include <cubeapplication.h>

void main()
{
    std::string name = "Cube";
    CubeApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}