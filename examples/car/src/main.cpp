#include <string>

#include <carapplication.h>

void main()
{
    std::string name = "Car";
    CarApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}