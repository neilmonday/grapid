#include "carapplication.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

bool g_terminate = false;

CarApplication::CarApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
    m_windowSize = { 2560, 1440 };
}

GrResult CarApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult CarApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult CarApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult CarApplication::Init()
{
    GrResult result = Application::Init();

    m_pCamera = std::make_unique<Camera>();
    m_pCamera->Init(m_window);
    m_pCamera->SetAspectRatio((float)m_windowSize.width / (float)m_windowSize.height);

    Swapchain swapchain = GetSwapchain();

    static constexpr uint8_t vertex_shader[] =
#include "shaders/car/vertex.glsl"
        ;

    std::vector<uint8_t> vertex_shader_code(
        vertex_shader, vertex_shader + sizeof(vertex_shader) - 1);

    static constexpr uint8_t fragment_shader[] =
#include "shaders/car/fragment.glsl"
        ;

    std::vector<uint8_t> fragment_shader_code(
        fragment_shader, fragment_shader + sizeof(fragment_shader) - 1);

    Shader vertex(GrShaderStage::GR_VERTEX, GrShaderType::GR_GLSL, vertex_shader_code);
    Shader fragment(GrShaderStage::GR_FRAGMENT, GrShaderType::GR_GLSL, fragment_shader_code);
    
    m_program.Init(m_pInstance);
    m_program.AddShader(vertex);
    m_program.AddShader(fragment);
    m_program.Compile();
    m_program.CreateShaderModules();

    m_queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

    int width = 0, height = 0, data_size = 0;
    std::vector<char> imageData = GetImageData("../../../../data/Lotus_dif_blackvrsn.png", 4, &width, &height, nullptr, &data_size);

    m_image.Init(m_pInstance, width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_COLOR_BIT);
    m_image.CreateStagingBuffer(data_size);
    m_image.Upload(m_queue, imageData.data());

    std::vector<VkDescriptorPoolSize> poolSizes =
    {
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1 }
    };
    m_descriptorPool.Init(m_pInstance, poolSizes, 2);

    std::shared_ptr<ImageDescriptorSet> pImageDescriptorSet = std::make_shared<ImageDescriptorSet>();
    pImageDescriptorSet->Init(m_pInstance, m_descriptorPool);
    pImageDescriptorSet->Update(m_image);

    m_uniformBuffer.Init(m_pInstance, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

    std::shared_ptr<TransformDescriptorSet> pTransformDescriptorSet = std::make_shared<TransformDescriptorSet>();
    pTransformDescriptorSet->Init(m_pInstance, m_descriptorPool);
    pTransformDescriptorSet->Update(m_uniformBuffer);

    m_descriptorSets.push_back(pImageDescriptorSet);
    m_descriptorSets.push_back(pTransformDescriptorSet);

    m_renderpass.Init(m_pInstance, swapchain.GetVkFormat());

    m_pipeline.Init(m_pInstance,
        m_renderpass,
        swapchain.GetVkExtent2D(),
        &m_program,
        VertexInput::getAttributeDescriptions(),
        VertexInput::getBindingDescription(),
        m_descriptorSets,
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
    m_pipeline.Create();

    std::string mesh_file = "../../../../data/car2.obj";
    // Create an instance of the Importer class
    Assimp::Importer importer;

    // And have it read the given file with some example postprocessing
    // Usually - if speed is not the most important aspect for you - you'll
    // probably to request more postprocessing than we do in this example.
    const aiScene* scene = importer.ReadFile(mesh_file,
        aiProcess_CalcTangentSpace |
        aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices |
        aiProcess_SortByPType |
        aiProcess_ConvertToLeftHanded);

    // If the import failed, report it
    if (nullptr == scene) {
        result = GrResult::GR_ERROR;
        return result;
    }

    //vertex
    std::vector<VertexPosition> vertices(scene->mMeshes[0]->mNumVertices);
    memcpy(vertices.data(), scene->mMeshes[0]->mVertices, vertices.size() * sizeof(VertexPosition));

    std::vector<VertexNormal> normals(scene->mMeshes[0]->mNumVertices);
    memcpy(normals.data(), scene->mMeshes[0]->mNormals, normals.size() * sizeof(VertexNormal));

    std::vector<VertexTexCoord> texCoords(scene->mMeshes[0]->mNumVertices);
    memcpy(texCoords.data(), scene->mMeshes[0]->mTextureCoords[0], texCoords.size() * sizeof(VertexTexCoord));

    struct aiFace;
    std::vector<uint32_t> indices = {};
    for (int face = 0; face < scene->mMeshes[0]->mNumFaces; face++)
    {
        for (int index = 0; index < scene->mMeshes[0]->mFaces[face].mNumIndices; index++)
        {
            indices.push_back(scene->mMeshes[0]->mFaces[face].mIndices[index]);
        }
    }
    std::shared_ptr<Buffer> pVertexPositionBuffer = std::make_shared<Buffer>();
    pVertexPositionBuffer->Init(m_pInstance, vertices.size() * sizeof(VertexPosition), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    pVertexPositionBuffer->Update((char*)vertices.data(), 0, vertices.size() * sizeof(VertexPosition));
    m_vertexBuffers.push_back(pVertexPositionBuffer);

    std::shared_ptr<Buffer> pVertexNormalBuffer = std::make_shared<Buffer>();
    pVertexNormalBuffer->Init(m_pInstance, normals.size() * sizeof(VertexNormal), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    pVertexNormalBuffer->Update((char*)normals.data(), 0, normals.size() * sizeof(VertexNormal));
    m_vertexBuffers.push_back(pVertexNormalBuffer);

    std::shared_ptr<Buffer> pVertexTexCoordBuffer = std::make_shared<Buffer>();
    pVertexTexCoordBuffer->Init(m_pInstance, texCoords.size() * sizeof(VertexTexCoord), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    pVertexTexCoordBuffer->Update((char*)texCoords.data(), 0, texCoords.size() * sizeof(VertexTexCoord));
    m_vertexBuffers.push_back(pVertexTexCoordBuffer);

    m_indexBuffer.Init(m_pInstance, indices.size() * sizeof(uint32_t), VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    m_indexBuffer.Update((char*)indices.data(), 0, indices.size() * sizeof(uint32_t));

    for (uint32_t i = 0; i < swapchain.GetImageViewCount(); i++)
    {
        Image depthImage;
        VkFormat depthFormat = m_pInstance->FindDepthFormat();
        depthImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

        //probably need to be consistent with pointers
        std::vector<VkImageView> attachments = {
            swapchain.GetVkImageView(i),
            depthImage.GetImageView()
        };
        m_depthImages.push_back(depthImage);

        Framebuffer framebuffer;
        framebuffer.Init(m_pInstance, swapchain.GetVkExtent2D(), m_renderpass.GetVkRenderPass(), attachments);
        m_framebuffers.push_back(framebuffer);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, m_queue);
        m_commandBuffers.push_back(commandBuffer);

        commandBuffer.Begin();
        m_renderpass.Begin(swapchain.GetVkExtent2D(), &framebuffer, &commandBuffer);
        m_pipeline.Bind(&commandBuffer);
        std::vector<VkDeviceSize> offsets(m_vertexBuffers.size(), 0);
        BindVertexBuffers(&commandBuffer, m_vertexBuffers, offsets);
        BindIndexBuffer(&commandBuffer, m_indexBuffer, 0);

        std::vector<VkDescriptorSet> vkDescriptorSets;
        for (auto descriptorSet : m_descriptorSets)
        {
            vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        }
        vkCmdBindDescriptorSets(commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline.GetVkPipelineLayout(), 0, 2, vkDescriptorSets.data(), 0, nullptr);
        vkCmdDrawIndexed(commandBuffer.GetVkCommandBuffer(), static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
        m_renderpass.End(&commandBuffer);
        commandBuffer.End();
    }

    return result;
}

GrResult CarApplication::Run()
{
    GrResult result = GR_SUCCESS;

    m_pCamera->SetPosition(glm::vec3(0.0f, 0.0f, -100.0f));
    m_pCamera->SetUpVector(glm::vec3(0.0f, -1.0f, 0.0f));

    //glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    //glfwSetInputMode(m_window, GLFW_STICKY_KEYS, GLFW_TRUE);

    glfwSetKeyCallback(m_window, [](GLFWwindow * window, int key, int scancode, int action, int mods)
    {
        if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS))
        {
            g_terminate = true;
            //glfwDestroyWindow(window);
        }
    });

    glfwPollEvents();
    std::shared_ptr<UniformBufferObject> pUBO = m_pCamera->GetUniformBufferObject();

    while (!glfwWindowShouldClose(m_window))
    {
        if (g_terminate)
            break;

        glfwPollEvents();

        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

        uint32_t imageIndex;
        vkAcquireNextImageKHR(m_pInstance->GetVkDevice(), GetSwapchain().GetVkSwapchain(), UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);
        
        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_inFlightImages[imageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightImages[imageIndex], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        m_inFlightImages[imageIndex] = m_inFlightFences[m_currentFrame];

        m_pCamera->SetForwardVector(glm::vec3(0.0f, 0.0f, 1.0f)); // shouldn't be a need to send this every frame
        m_pCamera->Update();
        m_uniformBuffer.Update((char*)pUBO.get(), 0, sizeof(UniformBufferObject));

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = m_commandBuffers[imageIndex].GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;

        VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame]);

        if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[m_currentFrame]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkSwapchainKHR swapChains[] = { GetSwapchain().GetVkSwapchain() };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr; // Optional

        vkQueuePresentKHR(m_queue.GetVkQueue(), &presentInfo);

        //This is lazy: vkQueueWaitIdle(queue.GetVkQueue());

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    return result;
}

GrResult CarApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    for (auto framebuffer : m_framebuffers)
    {
        framebuffer.Cleanup();
    }
    m_uniformBuffer.Cleanup();
    for (auto vertexBuffer : m_vertexBuffers)
    {
        vertexBuffer->Cleanup();
    }
    m_indexBuffer.Cleanup();
    for (auto depthImage : m_depthImages)
    {
        depthImage.Cleanup();
    }
    for (auto commandBuffer : m_commandBuffers)
    {
        commandBuffer.Cleanup(m_queue);
    }
    m_pipeline.Cleanup();
    m_renderpass.Cleanup();
    for (auto descriptorSet : m_descriptorSets)
    {
        descriptorSet->Cleanup();
    }
    m_descriptorPool.Cleanup();
    m_image.Cleanup();
    m_queue.Cleanup();
    m_program.Cleanup();
    result = Application::Cleanup();
    return result;
}

std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size) {
    std::vector<char> file_data = GetBinaryFileContents(filename);
    if (file_data.size() == 0) {
        return std::vector<char>();
    }

    int tmp_width = 0, tmp_height = 0, tmp_components = 0;
    unsigned char* image_data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(&file_data[0]), static_cast<int>(file_data.size()), &tmp_width, &tmp_height, &tmp_components, requested_components);
    if ((image_data == nullptr) ||
        (tmp_width <= 0) ||
        (tmp_height <= 0) ||
        (tmp_components <= 0)) {
        std::cout << "Could not read image data!" << std::endl;
        return std::vector<char>();
    }

    int size = (tmp_width) * (tmp_height) * (requested_components <= 0 ? tmp_components : requested_components);
    if (data_size) {
        *data_size = size;
    }
    if (width) {
        *width = tmp_width;
    }
    if (height) {
        *height = tmp_height;
    }
    if (components) {
        *components = tmp_components;
    }

    std::vector<char> output(size);
    memcpy(&output[0], image_data, size);

    stbi_image_free(image_data);
    return output;
}

std::vector<char> GetBinaryFileContents(std::string const& filename) {

    std::ifstream file(filename, std::ios::binary);
    if (file.fail()) {
        std::cout << "Could not open \"" << filename << "\" file!" << std::endl;
        return std::vector<char>();
    }

    std::streampos begin, end;
    begin = file.tellg();
    file.seekg(0, std::ios::end);
    end = file.tellg();

    std::vector<char> result(static_cast<size_t>(end - begin));
    file.seekg(0, std::ios::beg);
    file.read(&result[0], end - begin);
    file.close();

    return result;
}
