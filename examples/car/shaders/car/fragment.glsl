R"=====(#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set=0, binding=0) uniform sampler2D uTexture;

layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTexcoord;
layout(location = 3) in vec4 fragPosition;
layout(location = 4) in vec4 viewPosition;

layout(location = 0) out vec4 outColor;

const float AMBIENT = 0.1;
const float SPECULAR = 5.0;
const float PI = 3.141592653589f;
const float TWO_PI = (2.0f * PI);
const float ONE_OVER_PI = (1.0f / PI);
const float ONE_OVER_TWO_PI = (1.0f / TWO_PI);

void main() {
    vec3 lightPosition = vec3(100.0f);
    float lightDistance = distance(lightPosition, fragPosition.xyz);
    vec3 lightDir = normalize(lightPosition - fragPosition.xyz);
    vec3 normal = normalize(inNormal);
    vec3 materialColor = texture( uTexture, vec2(inTexcoord.x, inTexcoord.y) ).xyz;
    vec3 lightColor = vec3(1.0, 1.0, 1.0);
    vec3 viewDir = normalize(viewPosition.xyz - fragPosition.xyz);
    vec3 reflectDir = reflect(-lightDir, normal);  
    
    //being extra verbose with variable names for clarity
    //ambient
    vec3 ambientMaterialColor = materialColor;
    vec3 ambientLightColor = lightColor;
    vec3 ambient = ambientMaterialColor * ambientLightColor * AMBIENT;

    //diffuse
    vec3 diffuseMaterialColor = materialColor;
    vec3 diffuseLightColor = lightColor;
    vec3 diffuse = diffuseMaterialColor * diffuseLightColor * max(dot(normal, lightDir), 0);

    //specular
    vec3 specularMaterialColor =  vec3(1.0f);//materialColor; 
    vec3 specularLightColor = lightColor;
    vec3 specular = specularMaterialColor * specularLightColor * pow(max(dot(reflectDir, viewDir), 0.0), 32) * SPECULAR;

    //attenuation
    float d = lightDistance;  //distance from point to light
    float Kc = 0.0002f; //constant
    float Kl = 0.0002f; //linear
    float Kq = 0.0002f; //quadratic attenuation
    float attenuation = 1.0f / (Kc + Kl*d + Kq*d*d);

    outColor = vec4(ambient + attenuation * (diffuse + specular), 1.0f);
    //outColor = vec4(materialColor * (ambient + diffuse + specular), 1.0f);
}
)====="
