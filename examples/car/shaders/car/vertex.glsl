R"=====(#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set=0, binding=0) uniform sampler2D uTexture;

layout(set=1, binding=0) uniform TransformationBlock {
    mat4 model;
    mat4 view;
    mat4 proj;
    vec3 cameraPosition;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTexCoord;

layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outTexCoord;
layout(location = 3) out vec4 fragPosition;
layout(location = 4) out vec4 viewPosition;

void main() {
    viewPosition = ubo.model * vec4(ubo.cameraPosition, 1.0f);
    fragPosition = ubo.model * vec4(inPosition, 1.0);
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);
    outNormal = inNormal;
    outTexCoord = inTexCoord;
}
)====="
