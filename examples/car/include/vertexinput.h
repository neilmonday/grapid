#ifndef VERTEXINPUT_H
#define VERTEXINPUT_H

#include <vector>
#include <memory>

#include <volk.h>

#include <glm/glm.hpp>

#include <export.h>
#include <types.h>

struct VertexPosition
{
    glm::vec3 position;
};

struct VertexNormal
{
    glm::vec3 normal;
};

struct VertexTexCoord
{
    glm::vec3 texCoord; // tex-coord has 3 components in .3ds file
};

struct VertexInput
{
    static std::shared_ptr<std::vector<VkVertexInputBindingDescription> > getBindingDescription();
    static std::shared_ptr<std::vector<VkVertexInputAttributeDescription> > getAttributeDescriptions();
};

#endif //VERTEXINPUT_H
