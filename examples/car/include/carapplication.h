#ifndef CARAPPLICATION_H
#define CARAPPLICATION_H

#include <fstream>
#include <vector>

#include <application.h>
#include <shader.h>
#include <program.h>
#include <image.h>
#include <graphicspipeline.h>
#include <renderpass.h>
#include <camera.h>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include "vertexinput.h"
#include "imagedescriptorset.h"
#include "transformdescriptorset.h"

void WindowSizeCallback(GLFWwindow* window, int width, int height);
std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size);
std::vector<char> GetBinaryFileContents(std::string const& filename);

class CarApplication : public Application
{
public:
    CarApplication(std::string name);

    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult LoadVolkDevice() override;

private:
    std::unique_ptr<Camera> m_pCamera{};

    Buffer m_uniformBuffer{};

    std::vector<CommandBuffer> m_commandBuffers;
    Queue m_queue;
    Program m_program;
    GraphicsPipeline m_pipeline;
    std::vector<Framebuffer> m_framebuffers;
    std::vector<std::shared_ptr<Buffer> > m_vertexBuffers{};
    Buffer m_indexBuffer;
    Image m_image;
    Renderpass m_renderpass;
    DescriptorPool m_descriptorPool;
    std::vector<std::shared_ptr<DescriptorSet>> m_descriptorSets;
    std::vector<Image> m_depthImages{};

    bool m_terminate;
};

#endif //CARAPPLICATION_H
