#include <string>

#include <angleapplication.h>

void main()
{
    std::string name = "Angle";
    AngleApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}