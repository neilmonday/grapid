#include "vertexinput.h"

std::shared_ptr<std::vector<VkVertexInputBindingDescription>> VertexInput::getBindingDescription()
{
    std::shared_ptr<std::vector<VkVertexInputBindingDescription>> bindingDescriptions = std::make_shared<std::vector<VkVertexInputBindingDescription>>();

    VkVertexInputBindingDescription positionBindingDescription{};
    positionBindingDescription.binding = 0;
    positionBindingDescription.stride = sizeof(Vertex);
    positionBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    bindingDescriptions->push_back(positionBindingDescription);

    return bindingDescriptions;
}

std::shared_ptr<std::vector<VkVertexInputAttributeDescription>>VertexInput::getAttributeDescriptions()
{
    std::shared_ptr<std::vector<VkVertexInputAttributeDescription>> attributeDescriptions = std::make_shared<std::vector<VkVertexInputAttributeDescription>>();

    VkVertexInputAttributeDescription positionDescription;
    positionDescription.binding = 0;
    positionDescription.location = 0;
    positionDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionDescription.offset = offsetof(Vertex, position);
    attributeDescriptions->push_back(positionDescription);

    return attributeDescriptions;
}
