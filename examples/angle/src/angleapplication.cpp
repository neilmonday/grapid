#include "angleapplication.h"

bool g_terminate = false;

AngleApplication::AngleApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
    m_windowSize = { 400, 300 };
    m_requiredDeviceExtensionNames.push_back(VK_EXT_VERTEX_INPUT_DYNAMIC_STATE_EXTENSION_NAME);
}

GrResult AngleApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult AngleApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult AngleApplication::CreateLogicalDevice()
{
    GrResult result = GR_SUCCESS;
    float queuePriority = 1.0f;
    std::vector<uint32_t> queueFamilyIndices = GetQueueFamilyIndices();

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = queueFamilyIndices[0];
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures deviceFeatures{}; //dont need anything specific yet
    deviceFeatures.tessellationShader = 1;

    VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT physicalDeviceVertexInputDynamicStateFeatures{};
    physicalDeviceVertexInputDynamicStateFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_INPUT_DYNAMIC_STATE_FEATURES_EXT;
    physicalDeviceVertexInputDynamicStateFeatures.vertexInputDynamicState = 1;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(m_requiredDeviceExtensionNames.size());
    createInfo.ppEnabledExtensionNames = m_requiredDeviceExtensionNames.data();
    createInfo.pNext = &physicalDeviceVertexInputDynamicStateFeatures;

    VkDevice device{};
    if (vkCreateDevice(m_pInstance->GetVkPhysicalDevice(), &createInfo, nullptr, &device) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create logical device!");
    }
    else
    {
        m_pInstance->SetVkDevice(device);
    }

    return result;
}

GrResult AngleApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult AngleApplication::Init()
{
    GrResult result = Application::Init();

    Swapchain swapchain = GetSwapchain();

    static constexpr uint8_t vertex_shader[] =
#include "shaders/angle/vertex.glsl"
        ;

        std::vector<uint8_t> vertex_shader_code(
            vertex_shader, vertex_shader + sizeof(vertex_shader) - 1);

    static constexpr uint8_t fragment_shader[] =
#include "shaders/angle/fragment.glsl"
        ;

    std::vector<uint8_t> fragment_shader_code(
        fragment_shader, fragment_shader + sizeof(fragment_shader) - 1);
        
    Shader vertex(GrShaderStage::GR_VERTEX, GrShaderType::GR_GLSL, vertex_shader_code);
    Shader fragment(GrShaderStage::GR_FRAGMENT, GrShaderType::GR_GLSL, fragment_shader_code);

    m_program.Init(m_pInstance);
    m_program.AddShader(vertex);
    m_program.AddShader(fragment);
    m_program.Compile();
    m_program.CreateShaderModules();

    m_renderpass.Init(m_pInstance, swapchain.GetVkFormat());

    std::vector<std::shared_ptr<DescriptorSet> > descriptorSets;
    //For the pipeline, first init, then we can adjust parameters.
    m_pipeline.Init(m_pInstance,
        m_renderpass,
        swapchain.GetVkExtent2D(),
        &m_program, 
        VertexInput::getAttributeDescriptions(), 
        VertexInput::getBindingDescription(), 
        descriptorSets,
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

    //We can tweak some of the parameters like so before creating the pipeline
    m_pipeline.GetPipelineParameters().rasterizer.cullMode = VK_CULL_MODE_NONE;//We want to see front and back faces
    m_pipeline.GetPipelineParameters().rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

    std::vector<VkDynamicState> dynamicStates =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        //VK_DYNAMIC_STATE_LINE_WIDTH//,
        VK_DYNAMIC_STATE_VERTEX_INPUT_EXT
    };
    m_pipeline.GetPipelineParameters().dynamicState.dynamicStateCount = dynamicStates.size();
    m_pipeline.GetPipelineParameters().dynamicState.pDynamicStates = dynamicStates.data();

    //Once we have it setup, we can then create the pipeline
    m_pipeline.Create();

    m_queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

    //const std::vector<Vertex> vertices = {
    const std::vector<char> vertices = {
        //56,  72,
        //80,  72,
        //56, 100,
        //56, 100,
        //80,  72,
        //80, 100

        -51,
        56,  72, 
        80,  72, 
        56, 100, 
        56, 100, 
        80,  72, 
        80, 100

        //{{-0.7f,  0.7f}},
        //{{-0.7f, -0.7f}},
        //{{ 0.7f,  0.7f}},
        //{{-0.7f, -0.7f}},
        //{{ 0.7f, -0.7f}},
        //{{ 0.7f,  0.7f}}
    };

    std::shared_ptr<Buffer> vertexBuffer = std::make_shared<Buffer>();
    vertexBuffer->Init(m_pInstance, vertices.size() * sizeof(char), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    vertexBuffer->Update((char*)vertices.data(), 0, vertices.size() * sizeof(char));
    //vertexBuffer->Init(vertices.size() * sizeof(Vertex), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    //vertexBuffer->Update((char*)vertices.data(), 0, vertices.size() * sizeof(Vertex));
    m_vertexBuffers.push_back(vertexBuffer);

    for (uint32_t i = 0; i < swapchain.GetImageViewCount(); i++)
    {
        Image depthImage;
        VkFormat depthFormat = m_pInstance->FindDepthFormat();
        depthImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

        //probably need to be consistent with pointers
        std::vector<VkImageView> attachments = {
            swapchain.GetVkImageView(i),
            depthImage.GetImageView()
        };
        m_depthImages.push_back(depthImage);

        Framebuffer framebuffer;
        framebuffer.Init(m_pInstance, swapchain.GetVkExtent2D(), m_renderpass.GetVkRenderPass(), attachments);
        m_framebuffers.push_back(framebuffer);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, m_queue);
        m_commandBuffers.push_back(commandBuffer);

        commandBuffer.Begin();
        m_renderpass.Begin(swapchain.GetVkExtent2D(), &framebuffer, &commandBuffer);
        m_pipeline.Bind(&commandBuffer);

        /////////////////////////
        std::vector<VkVertexInputBindingDescription2EXT> vertexBindingDescriptions;
        VkVertexInputBindingDescription2EXT vertexBindingDescription{};
        vertexBindingDescription.sType = VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT;
        vertexBindingDescription.stride = 2;//sizeof(Vertex);
        vertexBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        vertexBindingDescription.divisor = 1;
        vertexBindingDescriptions.push_back(vertexBindingDescription);

        std::vector < VkVertexInputAttributeDescription2EXT> vertexAttributeDescriptions;
        VkVertexInputAttributeDescription2EXT vertexAttributeDescription{};
        vertexAttributeDescription.sType = VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT;
        vertexAttributeDescription.format = VK_FORMAT_R8G8_SSCALED;
        //vertexAttributeDescription.format = VK_FORMAT_R32G32_SFLOAT;
        vertexAttributeDescription.offset = 0;
        vertexAttributeDescriptions.push_back(vertexAttributeDescription);
        auto fn = (PFN_vkCmdSetVertexInputEXT)vkGetDeviceProcAddr(m_pInstance->GetVkDevice(), "vkCmdSetVertexInputEXT");
        fn(commandBuffer.GetVkCommandBuffer(), vertexBindingDescriptions.size(), vertexBindingDescriptions.data(), vertexAttributeDescriptions.size(), vertexAttributeDescriptions.data());

        VkViewport viewport{ 0.0, 300.0, 400.0, -300.0, 0.0, 1.0 };

        vkCmdSetViewport(commandBuffer.GetVkCommandBuffer(), 0, 1, &viewport);

        /////////////////////////

        std::vector<VkDeviceSize> offsets;
        for (auto vertexBuffer : m_vertexBuffers)
        {
            offsets.push_back(1); //This 1 is special to test skipping the first byte of vertex data.
        }
        BindVertexBuffers(&commandBuffer, m_vertexBuffers, offsets);

        vkCmdDraw(commandBuffer.GetVkCommandBuffer(), vertices.size(), 1, 0, 0);
        m_renderpass.End(&commandBuffer);
        commandBuffer.End();
    }

    return result;
}

GrResult AngleApplication::Run()
{
    GrResult result = GR_SUCCESS;

    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetKeyCallback(m_window, [](GLFWwindow* window, int key, int scancode, int action, int mods)
        {
            if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS))
            {
                g_terminate = true;
                //glfwDestroyWindow(window);
            }
        });

    while (!glfwWindowShouldClose(m_window))
    {
        if (g_terminate)
            break;

        glfwPollEvents();

        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

        uint32_t imageIndex;
        vkAcquireNextImageKHR(m_pInstance->GetVkDevice(), GetSwapchain().GetVkSwapchain(), UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);

        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_inFlightImages[imageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightImages[imageIndex], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        m_inFlightImages[imageIndex] = m_inFlightFences[m_currentFrame];

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = m_commandBuffers[imageIndex].GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;

        VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame]);

        if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[m_currentFrame]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkSwapchainKHR swapChains[] = { GetSwapchain().GetVkSwapchain() };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr; // Optional

        vkQueuePresentKHR(m_queue.GetVkQueue(), &presentInfo);

        //This is lazy: vkQueueWaitIdle(queue.GetVkQueue());

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    return result;
}

GrResult AngleApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    for (auto framebuffer : m_framebuffers)
    {
        framebuffer.Cleanup();
    }
    for (auto depthImage : m_depthImages)
    {
        depthImage.Cleanup();
    }
    for (auto commandBuffer : m_commandBuffers)
    {
        commandBuffer.Cleanup(m_queue);
    }
    for (auto vertexBuffer : m_vertexBuffers)
    {
        vertexBuffer->Cleanup();
    }

    m_pipeline.Cleanup();
    m_renderpass.Cleanup();
    m_queue.Cleanup();
    m_program.Cleanup();
    result = Application::Cleanup();
    return result;
}