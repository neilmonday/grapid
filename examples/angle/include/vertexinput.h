#ifndef VERTEXINPUT_H
#define VERTEXINPUT_H

#include <vector>
#include <memory>

#include <volk.h>

#include <glm/glm.hpp>

#include <export.h>
#include <types.h>

struct Vertex
{
    glm::u8vec2 position;
    //glm::vec2 position;
};

struct VertexInput
{
    static std::shared_ptr<std::vector<VkVertexInputBindingDescription>> getBindingDescription();
    static std::shared_ptr<std::vector<VkVertexInputAttributeDescription>> getAttributeDescriptions();
};

#endif //VERTEXINPUT_H
