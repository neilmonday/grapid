R"=====(#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPosition;

void main() {
    vec2 position = inPosition/127.0f;
    gl_Position = vec4(position, 0.0, 1.0);
}
)====="
