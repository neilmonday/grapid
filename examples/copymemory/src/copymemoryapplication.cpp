#include "copymemoryapplication.h"

#define VMA_IMPLEMENTATION
#define VMA_VULKAN_VERSION 1003000 // Vulkan 1.3
#include "vk_mem_alloc.h"

bool g_terminate = false;

bool readBinaryFile(const std::string& filename, std::vector<unsigned int>& data);

CopyMemoryApplication::CopyMemoryApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
    m_windowSize = { 256, 256 };

    m_requiredDeviceExtensionNames.push_back(VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME);

    m_requiredDeviceExtensionNames.push_back("VK_KHR_copy_memory_indirect");
}

GrResult CopyMemoryApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult CopyMemoryApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult CopyMemoryApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult CopyMemoryApplication::Init()
{
    GrResult result = Application::Init();
    uint32_t copy_mode = 4;
    //// Copy memory with no shaders ////
    if (copy_mode == 0)
    {
        size_t size = 64;
        Queue queue;
        queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

        Buffer srcBuffer;
        Buffer dstBuffer;

        srcBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
        dstBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        void* srcBufferMemoryPointer = srcBuffer.Map();
        char* srcBufferMemoryCharPointer = (char*)srcBufferMemoryPointer;

        for (uint32_t i = 0; i < 64; i++)
        {
            srcBufferMemoryCharPointer[i] = (char)i;
        }

        srcBuffer.Flush();
        srcBuffer.Unmap();

        std::vector<VkBufferCopy> regions{};
        VkBufferCopy region{};
        region.srcOffset = 0;
        region.dstOffset = 0;
        region.size = 64;
        regions.push_back(region);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, queue);
        commandBuffer.Begin();
        vkCmdCopyBuffer(commandBuffer.GetVkCommandBuffer(), srcBuffer.GetVkBuffer(), dstBuffer.GetVkBuffer(), regions.size(), regions.data());
        commandBuffer.End();

        // Submit command buffer and copy data from src buffer to a vertex buffer
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0]);

        if (vkQueueSubmit(queue.GetVkQueue(), 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit texture copy command buffer!");
        }
        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0], VK_TRUE, UINT64_MAX);
        void* dstBufferMemoryPointer = dstBuffer.Map();
        char* dstBufferMemoryCharPointer = (char*)dstBufferMemoryPointer;

        dstBuffer.Unmap();

        vkDeviceWaitIdle(m_pInstance->GetVkDevice());
        commandBuffer.Cleanup(queue);
    }
    //// End copy memory with no shaders ////


    //// Copy memory with shaders ////
    else if (copy_mode == 1)
    {
        static constexpr uint8_t compute_shader[] =
#include "shaders/copymemory/compute.glsl"
            ;
        std::vector<uint8_t> compute_shader_code(compute_shader, compute_shader + sizeof(compute_shader));
        Shader compute(GrShaderStage::GR_COMPUTE, GrShaderType::GR_GLSL, compute_shader_code);

        //static constexpr uint8_t copymemory_shader_spv[] =
        //{
        //#include "shaders/copymemory/copymemory.spv.h"
        //};
        //std::vector<uint8_t> copymemory_shader_code(
        //    std::begin(copymemory_shader_spv), 
        //    std::end(copymemory_shader_spv));
        //Shader copymemory(GrShaderStage::GR_COMPUTE, GrShaderType::GR_SPIRV, copymemory_shader_code);

        Program program;
        program.Init(m_pInstance);
        program.AddShader(compute);
        program.Compile();
        program.CreateShaderModules();

        Queue queue;
        queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

        std::vector<VkDescriptorPoolSize> poolSizes =
        {
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 2 }
        };
        DescriptorPool descriptorPool;
        descriptorPool.Init(m_pInstance, poolSizes, 1);

        Buffer srcBuffer;
        Buffer dstBuffer;

        srcBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
        dstBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        void* srcBufferMemoryPointer = srcBuffer.Map();
        char* srcBufferMemoryCharPointer = (char*)srcBufferMemoryPointer;

        for (uint32_t i = 0; i < 64; i++)
        {
            srcBufferMemoryCharPointer[i] = (char)i;
        }

        srcBuffer.Flush();
        srcBuffer.Unmap();

        std::shared_ptr<StorageDescriptorSet> pStorageDescriptorSet = std::make_shared<StorageDescriptorSet>();
        pStorageDescriptorSet->Init(m_pInstance, descriptorPool);
        pStorageDescriptorSet->Update(srcBuffer, dstBuffer);

        std::vector<std::shared_ptr<DescriptorSet>> descriptorSets;
        descriptorSets.push_back(pStorageDescriptorSet);

        ComputePipeline pipeline;
        pipeline.Init(m_pInstance, &program, descriptorSets);
        pipeline.Create();

        std::vector<VkDescriptorSet> vkDescriptorSets;
        for (auto descriptorSet : descriptorSets)
        {
            vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        }

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, queue);
        commandBuffer.Begin();
        pipeline.Bind(&commandBuffer);
        vkCmdBindDescriptorSets( commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.GetVkPipelineLayout(), 0, vkDescriptorSets.size(), vkDescriptorSets.data(), 0, nullptr);
        vkCmdDispatch(commandBuffer.GetVkCommandBuffer(), 64, 1, 1);
        commandBuffer.End();

        // Submit command buffer and copy data from src buffer to a vertex buffer
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0]);
            
        if (vkQueueSubmit(queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[0]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit texture copy command buffer!");
        }
        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0], VK_TRUE, UINT64_MAX);
        void* dstBufferMemoryPointer = dstBuffer.Map();
        char* dstBufferMemoryCharPointer = (char*)dstBufferMemoryPointer;

        dstBuffer.Unmap();

        vkDeviceWaitIdle(m_pInstance->GetVkDevice());
        commandBuffer.Cleanup(queue);
    }
    //// End copy memory with shaders ////

    //// Copy memory INDIRECT with shaders ////
    else if (copy_mode == 2)
    {
        static constexpr uint8_t copymemory_shader[] =
        {
        #include "shaders/copymemory/copymemory.comp"
        };
        std::vector<uint8_t> copymemory_shader_code(copymemory_shader, copymemory_shader + sizeof(copymemory_shader));

        Shader copymemory(GrShaderStage::GR_COMPUTE, GrShaderType::GR_GLSL, copymemory_shader_code);

        Program program;
        program.Init(m_pInstance);
        program.AddShader(copymemory);
        program.Compile();
        program.CreateShaderModules();

        Queue queue;
        queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

        std::vector<VkDescriptorPoolSize> poolSizes =
        {
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 2 }
        };
        DescriptorPool descriptorPool;
        descriptorPool.Init(m_pInstance, poolSizes, 1);

        Buffer srcBuffer;
        Buffer dstBuffer;

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
        */
        srcBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);
        dstBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);

        uint32_t* srcBufferMemoryPointer = (uint32_t*)srcBuffer.Map();

        for (uint32_t i = 0; i < 64; i++)
        {
            srcBufferMemoryPointer[i] = i;
        }

        srcBuffer.Flush();
        srcBuffer.Unmap();

        VkBufferDeviceAddressInfo srcBufferDeviceAddressInfo{};
        srcBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        srcBufferDeviceAddressInfo.pNext = nullptr;
        srcBufferDeviceAddressInfo.buffer = srcBuffer.GetVkBuffer();

        VkDeviceAddress srcAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &srcBufferDeviceAddressInfo);

        VkBufferDeviceAddressInfo dstBufferDeviceAddressInfo{};
        dstBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        dstBufferDeviceAddressInfo.pNext = nullptr;
        dstBufferDeviceAddressInfo.buffer = dstBuffer.GetVkBuffer();

        VkDeviceAddress dstAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &dstBufferDeviceAddressInfo);

        Buffer parameterBuffer;

        // Provided by VK_NV_copy_memory_indirect
        typedef struct VkCopyMemoryIndirectCommandKHR {
            VkDeviceAddress    srcAddress;
            VkDeviceAddress    dstAddress;
            VkDeviceSize       size;
        } VkCopyMemoryIndirectCommandKHR;

        // Provided by VK_NV_copy_memory_indirect
        VkCopyMemoryIndirectCommandKHR copyMemoryIndirectCommand;
        copyMemoryIndirectCommand.srcAddress = srcAddress;
        copyMemoryIndirectCommand.dstAddress = dstAddress;
        copyMemoryIndirectCommand.size = 64;

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used 
        to retrieve a buffer device address via vkGetBufferDeviceAddress and use that 
        address to access the buffer�s memory from a shader.
        */
        parameterBuffer.Init(m_pInstance, sizeof(VkCopyMemoryIndirectCommandKHR), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        void* parameterBufferMemoryPointer = parameterBuffer.Map();
        memcpy( parameterBufferMemoryPointer, &copyMemoryIndirectCommand, sizeof(VkCopyMemoryIndirectCommandKHR));

        parameterBuffer.Flush();
        parameterBuffer.Unmap();

        std::shared_ptr<IndirectStorageDescriptorSet> pIndirectStorageDescriptorSet = std::make_shared<IndirectStorageDescriptorSet>();
        pIndirectStorageDescriptorSet->Init(m_pInstance, descriptorPool);
        pIndirectStorageDescriptorSet->Update(parameterBuffer);

        std::vector<std::shared_ptr<DescriptorSet>> descriptorSets;
        descriptorSets.push_back(pIndirectStorageDescriptorSet);

        ComputePipeline pipeline;
        pipeline.Init(m_pInstance, &program, descriptorSets);
        pipeline.Create();

        std::vector<VkDescriptorSet> vkDescriptorSets;
        for (auto descriptorSet : descriptorSets)
        {
            vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        }

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, queue);
        commandBuffer.Begin();
        pipeline.Bind(&commandBuffer);
        vkCmdBindDescriptorSets(commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.GetVkPipelineLayout(), 0, vkDescriptorSets.size(), vkDescriptorSets.data(), 0, nullptr);
        vkCmdDispatch(commandBuffer.GetVkCommandBuffer(), 64, 1, 1);
        commandBuffer.End();

        // Submit command buffer and copy data from src buffer to a vertex buffer
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0]);

        if (vkQueueSubmit(queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[0]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit texture copy command buffer!");
        }
        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0], VK_TRUE, UINT64_MAX);
        uint32_t* dstBufferMemoryPointer = (uint32_t*)dstBuffer.Map();

        dstBuffer.Unmap();

        vkDeviceWaitIdle(m_pInstance->GetVkDevice());
        commandBuffer.Cleanup(queue);
    }
    //// End copy memory INDIRECT with shaders ////

    //// Copy memory with copy_memory_indirect ////
    else if (copy_mode == 3)
    {
        Queue queue;
        queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

        uint32_t copyCount = 1;
        uint32_t stride = 0;

        Buffer srcBuffer;
        Buffer dstBuffer;

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
        */
        srcBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);
        dstBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);

        uint32_t* srcBufferMemoryPointer = (uint32_t*)srcBuffer.Map();

        for (uint32_t i = 0; i < 64; i++)
        {
            srcBufferMemoryPointer[i] = i;
        }

        srcBuffer.Flush();
        srcBuffer.Unmap();

        VkBufferDeviceAddressInfo srcBufferDeviceAddressInfo{};
        srcBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        srcBufferDeviceAddressInfo.pNext = nullptr;
        srcBufferDeviceAddressInfo.buffer = srcBuffer.GetVkBuffer();

        VkDeviceAddress srcAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &srcBufferDeviceAddressInfo);

        VkBufferDeviceAddressInfo dstBufferDeviceAddressInfo{};
        dstBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        dstBufferDeviceAddressInfo.pNext = nullptr;
        dstBufferDeviceAddressInfo.buffer = dstBuffer.GetVkBuffer();

        VkDeviceAddress dstAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &dstBufferDeviceAddressInfo);

        Buffer copyBuffer;

        // Provided by VK_NV_copy_memory_indirect
        typedef struct VkCopyMemoryIndirectCommandKHR {
            VkDeviceAddress    srcAddress;
            VkDeviceAddress    dstAddress;
            VkDeviceSize       size;
        } VkCopyMemoryIndirectCommandKHR;

        // Provided by VK_NV_copy_memory_indirect
        VkCopyMemoryIndirectCommandKHR copyMemoryIndirectCommand;
        copyMemoryIndirectCommand.srcAddress = srcAddress;
        copyMemoryIndirectCommand.dstAddress = dstAddress;
        copyMemoryIndirectCommand.size = 64;

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used
        to retrieve a buffer device address via vkGetBufferDeviceAddress and use that
        address to access the buffer�s memory from a shader.
        */
        copyBuffer.Init(m_pInstance, sizeof(VkCopyMemoryIndirectCommandKHR), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        void* copyBufferMemoryPointer = copyBuffer.Map();
        memcpy(copyBufferMemoryPointer, &copyMemoryIndirectCommand, sizeof(VkCopyMemoryIndirectCommandKHR));

        copyBuffer.Flush();
        copyBuffer.Unmap();

        VkBufferDeviceAddressInfo copyBufferDeviceAddressInfo{};
        copyBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        copyBufferDeviceAddressInfo.pNext = nullptr;
        copyBufferDeviceAddressInfo.buffer = copyBuffer.GetVkBuffer();

        VkDeviceAddress copyBufferAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &copyBufferDeviceAddressInfo);

        typedef void (VKAPI_PTR* PFN_vkCmdCopyMemoryIndirectKHR)(
            VkCommandBuffer commandBuffer,
            VkDeviceAddress copyBufferAddress,
            uint32_t copyCount,
            uint32_t stride);

        PFN_vkCmdCopyMemoryIndirectKHR vkCmdCopyMemoryIndirectKHR = (PFN_vkCmdCopyMemoryIndirectKHR)vkGetDeviceProcAddr(
            m_pInstance->GetVkDevice(),
            "vkCmdCopyMemoryIndirectKHR");

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, queue);
        commandBuffer.Begin();
        vkCmdCopyMemoryIndirectKHR(
            commandBuffer.GetVkCommandBuffer(),
            copyBufferAddress,
            copyCount,
            stride);
        commandBuffer.End();

        // Submit command buffer and copy data from src buffer to a vertex buffer
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0]);

        if (vkQueueSubmit(queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[0]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit texture copy command buffer!");
        }
        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0], VK_TRUE, UINT64_MAX);
        uint32_t* dstBufferMemoryPointer = (uint32_t*)dstBuffer.Map();

        dstBuffer.Unmap();

        vkDeviceWaitIdle(m_pInstance->GetVkDevice());
        commandBuffer.Cleanup(queue);
    }
    //// End copy memory with copy_memory_indirect ////

    //// Copy memory to image INDIRECT with shaders ////
    else if (copy_mode == 4)
    {
        static constexpr uint8_t copymemory_shader[] =
        {
        #include "shaders/copymemory/copymemorytoimage.comp"
        };
        std::vector<uint8_t> copymemory_shader_code(copymemory_shader, copymemory_shader + sizeof(copymemory_shader));

        Shader copymemory(GrShaderStage::GR_COMPUTE, GrShaderType::GR_GLSL, copymemory_shader_code);

        Program program;
        program.Init(m_pInstance);
        program.AddShader(copymemory);
        program.Compile();
        program.CreateShaderModules();

        Queue queue;
        queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

        std::vector<VkDescriptorPoolSize> poolSizes =
        {
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 2 }
        };
        DescriptorPool descriptorPool;
        descriptorPool.Init(m_pInstance, poolSizes, 1);

        Buffer srcBuffer;
        Buffer dstBuffer;

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
        */
        srcBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);
        dstBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);

        uint32_t* srcBufferMemoryPointer = (uint32_t*)srcBuffer.Map();

        for (uint32_t i = 0; i < 64; i++)
        {
            srcBufferMemoryPointer[i] = i;
        }

        srcBuffer.Flush();
        srcBuffer.Unmap();

        VkBufferDeviceAddressInfo srcBufferDeviceAddressInfo{};
        srcBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        srcBufferDeviceAddressInfo.pNext = nullptr;
        srcBufferDeviceAddressInfo.buffer = srcBuffer.GetVkBuffer();

        VkDeviceAddress srcAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &srcBufferDeviceAddressInfo);

        VkBufferDeviceAddressInfo dstBufferDeviceAddressInfo{};
        dstBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        dstBufferDeviceAddressInfo.pNext = nullptr;
        dstBufferDeviceAddressInfo.buffer = dstBuffer.GetVkBuffer();

        VkDeviceAddress dstAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &dstBufferDeviceAddressInfo);

        Buffer parameterBuffer;

        // Provided by VK_NV_copy_memory_indirect
        typedef struct VkCopyMemoryIndirectCommandKHR {
            VkDeviceAddress    srcAddress;
            VkDeviceAddress    dstAddress;
            VkDeviceSize       size;
        } VkCopyMemoryIndirectCommandKHR;

        // Provided by VK_NV_copy_memory_indirect
        VkCopyMemoryIndirectCommandKHR copyMemoryIndirectCommand;
        copyMemoryIndirectCommand.srcAddress = srcAddress;
        copyMemoryIndirectCommand.dstAddress = dstAddress;
        copyMemoryIndirectCommand.size = 64;

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used
        to retrieve a buffer device address via vkGetBufferDeviceAddress and use that
        address to access the buffer�s memory from a shader.
        */
        parameterBuffer.Init(m_pInstance, sizeof(VkCopyMemoryIndirectCommandKHR), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        void* parameterBufferMemoryPointer = parameterBuffer.Map();
        memcpy(parameterBufferMemoryPointer, &copyMemoryIndirectCommand, sizeof(VkCopyMemoryIndirectCommandKHR));

        parameterBuffer.Flush();
        parameterBuffer.Unmap();

        std::shared_ptr<IndirectStorageDescriptorSet> pIndirectStorageDescriptorSet = std::make_shared<IndirectStorageDescriptorSet>();
        pIndirectStorageDescriptorSet->Init(m_pInstance, descriptorPool);
        pIndirectStorageDescriptorSet->Update(parameterBuffer);

        std::vector<std::shared_ptr<DescriptorSet>> descriptorSets;
        descriptorSets.push_back(pIndirectStorageDescriptorSet);

        ComputePipeline pipeline;
        pipeline.Init(m_pInstance, &program, descriptorSets);
        pipeline.Create();

        std::vector<VkDescriptorSet> vkDescriptorSets;
        for (auto descriptorSet : descriptorSets)
        {
            vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        }

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, queue);
        commandBuffer.Begin();
        pipeline.Bind(&commandBuffer);
        vkCmdBindDescriptorSets(commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.GetVkPipelineLayout(), 0, vkDescriptorSets.size(), vkDescriptorSets.data(), 0, nullptr);
        vkCmdDispatch(commandBuffer.GetVkCommandBuffer(), 64, 1, 1);
        commandBuffer.End();

        // Submit command buffer and copy data from src buffer to a vertex buffer
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0]);

        if (vkQueueSubmit(queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[0]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit texture copy command buffer!");
        }
        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0], VK_TRUE, UINT64_MAX);
        uint32_t* dstBufferMemoryPointer = (uint32_t*)dstBuffer.Map();

        dstBuffer.Unmap();

        vkDeviceWaitIdle(m_pInstance->GetVkDevice());
        commandBuffer.Cleanup(queue);
    }
    //// End copy memory to image INDIRECT with shaders ////

    //// Copy memory to image with copy_memory_indirect ////
    else if (copy_mode == 5)
    {
        Queue queue;
        queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

        uint32_t copyCount = 1;
        uint32_t stride = 0;

        Buffer srcBuffer;
        Image dstImage;

        int width = 4, height = 4, data_size = width * height * 4;
        std::vector<char> imageData;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                imageData.push_back(255); //red
                imageData.push_back(0); //green
                imageData.push_back(0); //blue
                imageData.push_back(0); //alpha
            }
        }

        //initialize dstImage to red, so we can see if copying the data, which represents green, changes the image.
        dstImage.Init(m_pInstance, width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_COLOR_BIT);
        dstImage.CreateStagingBuffer(data_size);
        dstImage.Upload(queue, imageData.data());

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used to retrieve a buffer device address via vkGetBufferDeviceAddress and use that address to access the buffer�s memory from a shader.
        */
        srcBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);
        //dstBuffer.Init(m_pInstance, sizeof(uint32_t) * 64, VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_SHARING_MODE_EXCLUSIVE);

        uint32_t* srcBufferMemoryPointer = (uint32_t*)srcBuffer.Map();

        //buffer is initialized to green, so we can see if it replaces the red.
        int k = 0;
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                srcBufferMemoryPointer[k++] = 0; //red
                srcBufferMemoryPointer[k++] = 255; //green
                srcBufferMemoryPointer[k++] = 0; //blue
                srcBufferMemoryPointer[k++] = 0; //alpha
            }
        }

        srcBuffer.Flush();
        srcBuffer.Unmap();

        VkBufferDeviceAddressInfo srcBufferDeviceAddressInfo{};
        srcBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        srcBufferDeviceAddressInfo.pNext = nullptr;
        srcBufferDeviceAddressInfo.buffer = srcBuffer.GetVkBuffer();

        VkDeviceAddress srcAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &srcBufferDeviceAddressInfo);

        //VkBufferDeviceAddressInfo dstBufferDeviceAddressInfo{};
        //dstBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        //dstBufferDeviceAddressInfo.pNext = nullptr;
        //dstBufferDeviceAddressInfo.buffer = dstBuffer.GetVkBuffer();

        //VkDeviceAddress dstAddress = vkGetBufferDeviceAddress(
        //    m_pInstance->GetVkDevice(),
        //    &dstBufferDeviceAddressInfo);

        Buffer copyBuffer;

        // Provided by VK_NV_copy_memory_indirect
        typedef struct VkCopyMemoryToImageIndirectCommandKHR {
            VkDeviceAddress srcAddress;
            uint32_t bufferRowLength;
            uint32_t bufferImageHeight;
            VkImageSubresourceLayers imageSubresource;
            VkOffset3D imageOffset;
            VkExtent3D imageExtent;
        } VkCopyMemoryToImageIndirectCommandKHR;

        std::vector<VkImageSubresourceLayers> imageSubresourceLayers{};
        VkImageSubresourceLayers imageSubresourceLayer;
        imageSubresourceLayer.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageSubresourceLayer.mipLevel = 0;
        imageSubresourceLayer.baseArrayLayer = 0;
        imageSubresourceLayer.layerCount = 1;
        imageSubresourceLayers.push_back(imageSubresourceLayer);

        // Provided by VK_NV_copy_memory_indirect
        VkCopyMemoryToImageIndirectCommandKHR copyMemoryToImageIndirectCommand;
        copyMemoryToImageIndirectCommand.srcAddress = srcAddress;
        copyMemoryToImageIndirectCommand.bufferRowLength = 4;
        copyMemoryToImageIndirectCommand.bufferImageHeight = 4;
        copyMemoryToImageIndirectCommand.imageSubresource = imageSubresourceLayers[0];
        copyMemoryToImageIndirectCommand.imageOffset = { 0, 0, 0 };
        copyMemoryToImageIndirectCommand.imageExtent = { 4, 4, 1 };

        /* VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT sounds like something relevant...:
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT specifies that the buffer can be used
        to retrieve a buffer device address via vkGetBufferDeviceAddress and use that
        address to access the buffer�s memory from a shader.
        */
        copyBuffer.Init(m_pInstance, sizeof(VkCopyMemoryToImageIndirectCommandKHR), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

        void* copyBufferMemoryPointer = copyBuffer.Map();
        memcpy(copyBufferMemoryPointer, &copyMemoryToImageIndirectCommand, sizeof(VkCopyMemoryToImageIndirectCommandKHR));

        copyBuffer.Flush();
        copyBuffer.Unmap();

        VkBufferDeviceAddressInfo copyBufferDeviceAddressInfo{};
        copyBufferDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
        copyBufferDeviceAddressInfo.pNext = nullptr;
        copyBufferDeviceAddressInfo.buffer = copyBuffer.GetVkBuffer();

        VkDeviceAddress copyBufferAddress = vkGetBufferDeviceAddress(
            m_pInstance->GetVkDevice(),
            &copyBufferDeviceAddressInfo);

        typedef void (VKAPI_PTR* PFN_vkCmdCopyMemoryToImageIndirectKHR)(
            VkCommandBuffer commandBuffer,
            VkDeviceAddress copyBufferAddress,
            uint32_t copyCount,
            uint32_t stride,
            VkImage dstImage,
            VkImageLayout dstImageLayout,
            const VkImageSubresourceLayers* pImageSubresources);

        PFN_vkCmdCopyMemoryToImageIndirectKHR vkCmdCopyMemoryToImageIndirectKHR = (PFN_vkCmdCopyMemoryToImageIndirectKHR)vkGetDeviceProcAddr(
            m_pInstance->GetVkDevice(),
            "vkCmdCopyMemoryToImageIndirectKHR");

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, queue);
        commandBuffer.Begin();
        vkCmdCopyMemoryToImageIndirectKHR(
            commandBuffer.GetVkCommandBuffer(),
            copyBufferAddress,
            copyCount,
            stride,
            dstImage.GetVkImage(),
            dstImage.GetImageLayout(),
            imageSubresourceLayers.data());
        commandBuffer.End();

        // Submit command buffer and copy data from src buffer to a vertex buffer
        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 0;
        submitInfo.pWaitSemaphores = nullptr;
        submitInfo.pWaitDstStageMask = nullptr;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = commandBuffer.GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;
        submitInfo.signalSemaphoreCount = 0;
        submitInfo.pSignalSemaphores = nullptr;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0]);

        if (vkQueueSubmit(queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[0]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit texture copy command buffer!");
        }
        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[0], VK_TRUE, UINT64_MAX);

        //uint32_t* dstImageMemoryPointer = (uint32_t*)dstImage.Map();
        //dstImage.Unmap();

        vkDeviceWaitIdle(m_pInstance->GetVkDevice());
        commandBuffer.Cleanup(queue);
    }
    //// End copy memory to image with copy_memory_indirect ////

    return result;
}

GrResult CopyMemoryApplication::Run()
{
    GrResult result = GR_SUCCESS;

    glfwSetKeyCallback(m_window, [](GLFWwindow * window, int key, int scancode, int action, int mods)
    {
        if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS))
        {
            g_terminate = true;
            //glfwDestroyWindow(window);
        }
    });

    glfwPollEvents();

    while (!glfwWindowShouldClose(m_window))
    {
        if (g_terminate)
            break;

        glfwPollEvents();

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    return result;
}

GrResult CopyMemoryApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    result = Application::Cleanup();

    return result;
}

GrResult CopyMemoryApplication::CreateLogicalDevice()
{
    GrResult result = GR_SUCCESS;
    float queuePriority = 1.0f;
    std::vector<uint32_t> queueFamilyIndices = GetQueueFamilyIndices();

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = queueFamilyIndices[0];
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures deviceFeatures{}; //dont need anything specific yet
    deviceFeatures.shaderInt64 = 1;

    // Enable the extension features
    VkPhysicalDeviceFeatures2 physicalDeviceFeatures2{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 };
    VkPhysicalDeviceBufferDeviceAddressFeatures physicalDeviceBufferDeviceAddressFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES };
    //VkPhysicalDeviceRayTracingPipelineFeaturesKHR physicalDeviceRayTracingPipelineFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR };
    //VkPhysicalDeviceAccelerationStructureFeaturesKHR physicalDeviceAccelerationStructureFeatures{ VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR };

    physicalDeviceBufferDeviceAddressFeatures.bufferDeviceAddress = VK_TRUE;
    //physicalDeviceRayTracingPipelineFeatures.rayTracingPipeline = VK_TRUE;
    //physicalDeviceAccelerationStructureFeatures.accelerationStructure = VK_TRUE;

    physicalDeviceFeatures2.pNext = &physicalDeviceBufferDeviceAddressFeatures;
    physicalDeviceFeatures2.features = deviceFeatures;
    //physicalDeviceBufferDeviceAddressFeatures.pNext = &physicalDeviceRayTracingPipelineFeatures;
    //physicalDeviceRayTracingPipelineFeatures.pNext = &physicalDeviceAccelerationStructureFeatures;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = nullptr;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(m_requiredDeviceExtensionNames.size());
    createInfo.ppEnabledExtensionNames = m_requiredDeviceExtensionNames.data();
    createInfo.pNext = &physicalDeviceFeatures2;

    VkDevice device{};
    if (vkCreateDevice(m_pInstance->GetVkPhysicalDevice(), &createInfo, nullptr, &device) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create logical device!");
    }
    else
    {
        m_pInstance->SetVkDevice(device);

        VK_KHR_buffer_device_address_enabled = true;
    }

    return result;
}

