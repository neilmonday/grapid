#include <string>

#include <copymemoryapplication.h>

void main()
{
    std::string name = "CopyMemory";
    CopyMemoryApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}