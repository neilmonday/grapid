#include "storagedescriptorset.h"

StorageDescriptorSet::StorageDescriptorSet()
{
}

GrResult StorageDescriptorSet::Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool)
{
    GrResult result = DescriptorSet::Init(pInstance, descriptorPool);

    VkDescriptorSetLayoutBinding srcDescriptorSetLayoutBinding{};
    srcDescriptorSetLayoutBinding.binding = 0;
    srcDescriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    srcDescriptorSetLayoutBinding.descriptorCount = 1;
    srcDescriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    srcDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutBinding dstDescriptorSetLayoutBinding{};
    dstDescriptorSetLayoutBinding.binding = 1;
    dstDescriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    dstDescriptorSetLayoutBinding.descriptorCount = 1;
    dstDescriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    dstDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;

    std::vector< VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings;
    descriptorSetLayoutBindings.push_back(srcDescriptorSetLayoutBinding);
    descriptorSetLayoutBindings.push_back(dstDescriptorSetLayoutBinding);

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.flags = 0;
    descriptorSetLayoutCreateInfo.bindingCount = descriptorSetLayoutBindings.size();
    descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

    if (vkCreateDescriptorSetLayout(m_pInstance->GetVkDevice(), &descriptorSetLayoutCreateInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create descriptor set layout!");
    }

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = m_descriptorPool.GetVkDescriptorPool();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &m_descriptorSetLayout;

    if (vkAllocateDescriptorSets(m_pInstance->GetVkDevice(), &descriptorSetAllocateInfo, &m_descriptorSet) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to allocate descriptor set!");
    }

    return result;
}

GrResult StorageDescriptorSet::Cleanup()
{
    GrResult result = GR_SUCCESS;
    //vkResetDescriptorPool will free all of the DescriptorSets in the pool. 
    vkDestroyDescriptorSetLayout(m_pInstance->GetVkDevice(), m_descriptorSetLayout, nullptr);
    return result;
}

GrResult StorageDescriptorSet::Update(Buffer srcBuffer, Buffer dstBuffer)
{
    GrResult result = GR_SUCCESS;

    VkDescriptorBufferInfo srcBufferInfo{};
    srcBufferInfo.buffer = srcBuffer.GetVkBuffer();
    srcBufferInfo.offset = 0;
    srcBufferInfo.range = srcBuffer.GetVkSize();

    VkDescriptorBufferInfo dstBufferInfo{};
    dstBufferInfo.buffer = dstBuffer.GetVkBuffer();
    dstBufferInfo.offset = 0;
    dstBufferInfo.range = dstBuffer.GetVkSize();

    std::vector<VkDescriptorBufferInfo> descriptorBufferInfos;
    descriptorBufferInfos.push_back(srcBufferInfo);
    descriptorBufferInfos.push_back(dstBufferInfo);

    VkWriteDescriptorSet writeDescriptorSet{};
    writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSet.dstSet = m_descriptorSet;
    writeDescriptorSet.dstBinding = 0;
    writeDescriptorSet.dstArrayElement = 0;
    writeDescriptorSet.descriptorCount = descriptorBufferInfos.size();
    writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    writeDescriptorSet.pImageInfo = nullptr; // Optional
    writeDescriptorSet.pBufferInfo = descriptorBufferInfos.data();
    writeDescriptorSet.pTexelBufferView = nullptr;

    std::vector< VkWriteDescriptorSet> writeDescriptorSets;
    writeDescriptorSets.push_back(writeDescriptorSet);

    vkUpdateDescriptorSets(m_pInstance->GetVkDevice(), writeDescriptorSets.size(), writeDescriptorSets.data(), 0, nullptr);
    return result;
}
