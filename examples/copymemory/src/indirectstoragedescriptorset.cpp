#include "indirectstoragedescriptorset.h"

IndirectStorageDescriptorSet::IndirectStorageDescriptorSet()
{
}

GrResult IndirectStorageDescriptorSet::Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool)
{
    GrResult result = DescriptorSet::Init(pInstance, descriptorPool);

    VkDescriptorSetLayoutBinding parameterDescriptorSetLayoutBinding{};
    parameterDescriptorSetLayoutBinding.binding = 0;
    parameterDescriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    parameterDescriptorSetLayoutBinding.descriptorCount = 1;
    parameterDescriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    parameterDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;

    std::vector< VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings;
    descriptorSetLayoutBindings.push_back(parameterDescriptorSetLayoutBinding);

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.flags = 0;
    descriptorSetLayoutCreateInfo.bindingCount = descriptorSetLayoutBindings.size();
    descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

    if (vkCreateDescriptorSetLayout(m_pInstance->GetVkDevice(), &descriptorSetLayoutCreateInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create descriptor set layout!");
    }

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = m_descriptorPool.GetVkDescriptorPool();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &m_descriptorSetLayout;

    if (vkAllocateDescriptorSets(m_pInstance->GetVkDevice(), &descriptorSetAllocateInfo, &m_descriptorSet) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to allocate descriptor set!");
    }

    return result;
}

GrResult IndirectStorageDescriptorSet::Cleanup()
{
    GrResult result = GR_SUCCESS;
    //vkResetDescriptorPool will free all of the DescriptorSets in the pool. 
    vkDestroyDescriptorSetLayout(m_pInstance->GetVkDevice(), m_descriptorSetLayout, nullptr);
    return result;
}

GrResult IndirectStorageDescriptorSet::Update(Buffer parameterBuffer)
{
    GrResult result = GR_SUCCESS;

    VkDescriptorBufferInfo parameterBufferInfo{};
    parameterBufferInfo.buffer = parameterBuffer.GetVkBuffer();
    parameterBufferInfo.offset = 0;
    parameterBufferInfo.range = parameterBuffer.GetVkSize();

    std::vector<VkDescriptorBufferInfo> descriptorBufferInfos;
    descriptorBufferInfos.push_back(parameterBufferInfo);

    VkWriteDescriptorSet writeDescriptorSet{};
    writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writeDescriptorSet.dstSet = m_descriptorSet;
    writeDescriptorSet.dstBinding = 0;
    writeDescriptorSet.dstArrayElement = 0;
    writeDescriptorSet.descriptorCount = descriptorBufferInfos.size();
    writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writeDescriptorSet.pImageInfo = nullptr; // Optional
    writeDescriptorSet.pBufferInfo = descriptorBufferInfos.data();
    writeDescriptorSet.pTexelBufferView = nullptr;

    std::vector< VkWriteDescriptorSet> writeDescriptorSets;
    writeDescriptorSets.push_back(writeDescriptorSet);

    vkUpdateDescriptorSets(m_pInstance->GetVkDevice(), writeDescriptorSets.size(), writeDescriptorSets.data(), 0, nullptr);
    return result;
}
