R"=====(#version 460
layout(local_size_x = 64) in;

layout(std140, binding = 0) readonly buffer SrcBuffer
{
    uint data[64];
} srcBuffer;

layout(std140, binding = 1) writeonly buffer DstBuffer
{
    uint data[64];
} dstBuffer;

void main()
{
    dstBuffer.data[gl_LocalInvocationID.x] = srcBuffer.data[gl_LocalInvocationID.x];
    return;
}
)====="
