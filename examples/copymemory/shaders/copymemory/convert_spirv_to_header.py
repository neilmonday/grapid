import sys

def convert_spirv_to_header(input_file, output_file):
    # Read the binary data from the SPIR-V file
    with open(input_file, 'rb') as f:
        binary_data = f.read()

    # Generate the header file content
    with open(output_file, 'w') as f:
        # Write the data in the format suitable for a C++ header file
        num_bytes = len(binary_data)
        for i, byte in enumerate(binary_data):
            f.write(f"0x{byte:02X}")
            if i < num_bytes - 1:
                f.write(", ")
            if (i + 1) % 16 == 0 or i == num_bytes - 1:
                f.write("\n")

    print(f"Header file '{output_file}' has been created.")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python convert_spirv_to_header.py <input_spirv_file> <output_header_file>")
    else:
        input_spirv_file = sys.argv[1]
        output_header_file = sys.argv[2]
        convert_spirv_to_header(input_spirv_file, output_header_file)
