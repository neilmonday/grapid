#ifndef COPYMEMORYAPPLICATION_H
#define COPYMEMORYAPPLICATION_H

#include <fstream>
#include <vector>

#include <vk_mem_alloc.h>

#include <application.h>
#include <program.h>
#include <computepipeline.h>
#include <descriptorpool.h>

#include "storagedescriptorset.h"
#include "indirectstoragedescriptorset.h"

class CopyMemoryApplication : public Application
{
public:
    CopyMemoryApplication(std::string name);

    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult LoadVolkDevice() override;
    GrResult CreateLogicalDevice() override;

private:

    bool m_terminate;
};

#endif //COPYMEMORYAPPLICATION_H
