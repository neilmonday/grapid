#ifndef INDIRECTSTORAGEDESCRIPTORSET_H
#define INDIRECTSTORAGEDESCRIPTORSET_H

#include <descriptorset.h>
#include <buffer.h>

class IndirectStorageDescriptorSet : public DescriptorSet
{
public:
    IndirectStorageDescriptorSet();

    GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool) override;
    GrResult Cleanup() override;

    GrResult Update(Buffer parameterBuffer);
};

#endif //INDIRECTSTORAGEDESCRIPTORSET_H
