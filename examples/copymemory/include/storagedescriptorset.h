#ifndef STORAGEDESCRIPTORSET_H
#define STORAGEDESCRIPTORSET_H

#include <descriptorset.h>
#include <buffer.h>

class StorageDescriptorSet : public DescriptorSet
{
public:
    StorageDescriptorSet();

    GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool) override;
    GrResult Cleanup() override;

    GrResult Update(Buffer srcBuffer, Buffer dstBuffer);
};

#endif //STORAGEDESCRIPTORSET_H
