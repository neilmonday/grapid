***  NOTE  ***
Due to me discovering 
https://github.com/KhronosGroup/GLSL/blob/main/extensions/ext/GLSL_EXT_buffer_reference.txt
and
https://docs.vulkan.org/samples/latest/samples/extensions/buffer_device_address/README.html

we no longer need to do this crazy SPIR-V coding manually. But I will leave it here for future reference.
***  END NOTE  ***

This project uses a SPIR-V shader which needs to be compiled before building.

1. Generate the SPIR-V from the GLSL compute shader, so we can then modify it
    *This will overwrite compute.spv
    glslang -V --target-env spirv1.5 --spirv-dis -o compute.spv compute.comp

    and copy the disassembly from the command prompt to compute.spvasm

2. Modify the SPIR-V disassembly

3. Assemble the SPIR-V binary from the disassembled shader compute.spvasm (this is the file we code by hand to use the instructions that aren't available to GLSL)
    spirv-as copymemory.spvasm -o copymemory.spv

4. Convert the binary file into a header file to be built into the CopyMemory example program
    python convert_spirv_to_header.py copymemory.spv copymemory.spv.h