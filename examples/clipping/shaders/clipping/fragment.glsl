R"=====(#version 310 es
#extension GL_EXT_clip_cull_distance : enable
precision highp float;

in float gl_ClipDistance[gl_MaxClipDistances];

layout(location = 0) out vec4 color;

void main()
{
    for(int i = 0; i < gl_MaxClipDistances; i++)
    {
        if(abs(gl_ClipDistance[i] - float(i + 1) / float(gl_MaxClipDistances)) > 0.0125)
        {
            discard;
        }
    }

    color = vec4(1.0, 0.0, 0.0, 1.0);
}

)====="
