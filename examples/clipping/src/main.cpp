#include <clippingapplication.h>

void main()
{
    std::string name = "Clipping";
    ClippingApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}