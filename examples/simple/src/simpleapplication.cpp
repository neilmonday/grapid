#include "simpleapplication.h"

#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#define VMA_IMPLEMENTATION
#define VMA_VULKAN_VERSION 1003000 // Vulkan 1.3
#include "vk_mem_alloc.h"

#define VOLK_IMPLEMENTATION
#include <volk.h>

bool g_terminate = false;

SimpleApplication::SimpleApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
}

GrResult SimpleApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult SimpleApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult SimpleApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult SimpleApplication::Init()
{
    GrResult result = Application::Init();

    Swapchain swapchain = GetSwapchain();

    static constexpr uint8_t vertex_shader[] =
    #include "shaders/simple/vertex.glsl"
    ;

    std::vector<uint8_t> vertex_shader_code(
        vertex_shader, vertex_shader + sizeof(vertex_shader) - 1);

    static constexpr uint8_t fragment_shader[] =
    #include "shaders/simple/fragment.glsl"
    ;

    std::vector<uint8_t> fragment_shader_code(
        fragment_shader, fragment_shader + sizeof(fragment_shader) - 1);

    Shader vertex(GrShaderStage::GR_VERTEX, GrShaderType::GR_GLSL, vertex_shader_code);
    Shader fragment(GrShaderStage::GR_FRAGMENT, GrShaderType::GR_GLSL, fragment_shader_code);

    m_program.Init(m_pInstance);
    m_program.AddShader(vertex);
    m_program.AddShader(fragment);
    m_program.Compile();
    m_program.CreateShaderModules();

    m_renderpass.Init(m_pInstance, swapchain.GetVkFormat());

    std::vector<std::shared_ptr<DescriptorSet> > descriptorSets;

    m_pipeline.Init(m_pInstance,
        m_renderpass,
        swapchain.GetVkExtent2D(),
        &m_program, 
        VertexInput::getAttributeDescriptions(), 
        VertexInput::getBindingDescription(), 
        descriptorSets,
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
    m_pipeline.Create();

    m_queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

    const std::vector<Vertex> vertices = {
        {{ 0.7f,  0.7f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{-0.7f,  0.7f, 0.0f}, {0.0f, 0.0f, 1.0f}},
        {{ 0.0f, -0.7f, 0.0f}, {1.0f, 0.0f, 0.0f}}
    };

    std::shared_ptr<Buffer> vertexBuffer = std::make_shared<Buffer>();
    vertexBuffer->Init(m_pInstance, vertices.size() * sizeof(Vertex), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    vertexBuffer->Update((char*)vertices.data(), 0, vertices.size() * sizeof(Vertex));
    m_vertexBuffers.push_back(vertexBuffer);

    for (uint32_t i = 0; i < swapchain.GetImageViewCount(); i++)
    {
        Image depthImage;
        VkFormat depthFormat = m_pInstance->FindDepthFormat();
        depthImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

        //probably need to be consistent with pointers
        std::vector<VkImageView> attachments = {
            swapchain.GetVkImageView(i),
            depthImage.GetImageView()
        };
        m_depthImages.push_back(depthImage);

        Framebuffer framebuffer;
        framebuffer.Init(m_pInstance, swapchain.GetVkExtent2D(), m_renderpass.GetVkRenderPass(), attachments);
        m_framebuffers.push_back(framebuffer);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, m_queue);
        m_commandBuffers.push_back(commandBuffer);

        commandBuffer.Begin();
        m_renderpass.Begin(swapchain.GetVkExtent2D(), &framebuffer, &commandBuffer);
        m_pipeline.Bind(&commandBuffer);
        std::vector<VkDeviceSize> offsets(m_vertexBuffers.size(), 0);
        BindVertexBuffers(&commandBuffer, m_vertexBuffers, offsets);
        vkCmdDraw(commandBuffer.GetVkCommandBuffer(), vertices.size(), 1, 0, 0);
        m_renderpass.End(&commandBuffer);
        commandBuffer.End();
    }

    return result;
}

GrResult SimpleApplication::Run()
{
    GrResult result = GR_SUCCESS;

    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetKeyCallback(m_window, [](GLFWwindow* window, int key, int scancode, int action, int mods)
        {
            if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS))
            {
                g_terminate = true;
                //glfwDestroyWindow(window);
            }
        });

    while (!glfwWindowShouldClose(m_window))
    {
        if (g_terminate)
            break;

        glfwPollEvents();

        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

        uint32_t imageIndex;
        vkAcquireNextImageKHR(m_pInstance->GetVkDevice(), GetSwapchain().GetVkSwapchain(), UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);
        
        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_inFlightImages[imageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightImages[imageIndex], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        m_inFlightImages[imageIndex] = m_inFlightFences[m_currentFrame];

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = m_commandBuffers[imageIndex].GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;

        VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame]);

        if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[m_currentFrame]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkSwapchainKHR swapChains[] = { GetSwapchain().GetVkSwapchain() };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr; // Optional

        vkQueuePresentKHR(m_queue.GetVkQueue(), &presentInfo);

        //This is lazy: vkQueueWaitIdle(queue.GetVkQueue());

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    return result;
}

GrResult SimpleApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    for (auto framebuffer : m_framebuffers)
    {
        framebuffer.Cleanup();
    }
    for (auto depthImage : m_depthImages)
    {
        depthImage.Cleanup();
    }
    for (auto commandBuffer : m_commandBuffers)
    {
        commandBuffer.Cleanup(m_queue);
    }
    for (auto vertexBuffer : m_vertexBuffers)
    {
        vertexBuffer->Cleanup();
    }

    m_pipeline.Cleanup();
    m_renderpass.Cleanup();
    m_queue.Cleanup();
    m_program.Cleanup();
    result = Application::Cleanup();
    return result;
}