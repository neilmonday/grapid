#include <string>

#include <simpleapplication.h>

void main()
{
    std::string name = "Simple";
    SimpleApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}