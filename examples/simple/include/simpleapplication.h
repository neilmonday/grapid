#ifndef SIMPLEAPPLICATION_H
#define SIMPLEAPPLICATION_H

#include <application.h>
#include <shader.h>
#include <program.h>
#include <graphicspipeline.h>
#include <queue.h>
#include <renderpass.h>
#include <commandbuffer.h>
#include <framebuffer.h>
#include <glm/glm.hpp>

#include "vertexinput.h"

class SimpleApplication : public Application
{
public:
    SimpleApplication(std::string name);
    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult LoadVolkDevice() override;

private:
    std::vector<CommandBuffer> m_commandBuffers;
    std::vector<Framebuffer> m_framebuffers;
    Program m_program;
    GraphicsPipeline m_pipeline;
    Renderpass m_renderpass;
    Queue m_queue;

    std::vector<std::shared_ptr<Buffer> > m_vertexBuffers{};
    std::vector<Image> m_depthImages{};

};

#endif //SIMPLEAPPLICATION_H
