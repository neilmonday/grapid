cmake_minimum_required(VERSION 3.29.0)

set(CMAKE_CXX_STANDARD 23)

project( Space CXX )

include( FindVulkan )

# Include directories
include_directories(${Space_SOURCE_DIR}/../../include)#grapid
include_directories(${Space_SOURCE_DIR}/../../external/glm)
include_directories(${Space_SOURCE_DIR}/../../external/glfw/install/include)
include_directories(${Space_SOURCE_DIR}/include)
include_directories(${Space_SOURCE_DIR}/)

set(SOURCES 
src/spaceapplication.cpp
src/main.cpp
src/transformdescriptorset.cpp
src/vertexinput.cpp
include/spaceapplication.h
include/transformdescriptorset.h
include/vertexinput.h
shaders/space/vertex.glsl
shaders/space/geometry.glsl
shaders/space/tess_ctrl.glsl
shaders/space/tess_eval.glsl
shaders/space/fragment.glsl)
add_executable(Space ${SOURCES})

IF (MSVC)
include_directories($ENV{VULKAN_SDK}/Include)
add_definitions(-D_CRT_SECURE_NO_WARNINGS)
target_link_libraries(Space Grapid)
ENDIF ()
