R"=====(#version 450 core

layout(location = 3) in vec3 g_color;

layout(location = 0)out vec4 color;

void main()
{
    color = vec4(g_color ,1.0);
}
)====="
