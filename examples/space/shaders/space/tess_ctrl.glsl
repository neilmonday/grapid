R"=====(#version 450 core

layout(vertices = 3) out;

layout(binding = 0) uniform TessLevelBlock {
    float tess_level_inner;
    float tess_level_outer;
};

layout(location = 0) in vec3 vs_position[];
layout(location = 1) in vec3 vs_color[];
layout(location = 2) in int vs_instance_id[];

layout(location = 0) out vec3 tc_position[];
layout(location = 1) out vec3 tc_color[];
layout(location = 2) out int tc_instance_id[];

void main()
{
    tc_position[gl_InvocationID] = vs_position[gl_InvocationID];
    tc_color[gl_InvocationID] = vs_color[gl_InvocationID];
    tc_instance_id[gl_InvocationID] = vs_instance_id[gl_InvocationID];

    if (gl_InvocationID == 0) {
        gl_TessLevelInner[0] = tess_level_inner;
        gl_TessLevelOuter[0] = tess_level_outer;
        gl_TessLevelOuter[1] = tess_level_outer;
        gl_TessLevelOuter[2] = tess_level_outer;
    }
}
)====="
