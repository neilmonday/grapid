R"=====(#version 450 core

layout(binding = 1) uniform TransformationBlock {
    uniform mat4 transformation;
};

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

layout(location = 0) in vec3 te_position[3];
layout(location = 1) in vec3 te_patch_distance[3];
layout(location = 2) in vec3 te_color[3];
layout(location = 3) in int te_instance_id[3];

layout(location = 0) out vec3 g_facet_normal;
layout(location = 1) out vec3 g_patch_distance;
layout(location = 2) out vec3 g_tri_distance;
layout(location = 3) out vec3 g_color;

uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    //return f - 1.0;                        // Range [0:1]
    return ((f - 1.0) * 2.0) - 1.0;        // Range [-1:1]
}

// Pseudo-random value in half-open range [0:1].
float random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }

void main()
{
    vec3 A = te_position[2] - te_position[0];
    vec3 B = te_position[1] - te_position[0];
    g_facet_normal = normalize(cross(A, B));

    vec4 offset = vec4(random(float(te_instance_id[0])) * 1000.0, random(float(te_instance_id[0]+1)) * 1000.0,random(float(te_instance_id[0]+2))  * 1000.0, 1.0);
    
    g_patch_distance = te_patch_distance[0];
    g_tri_distance = vec3(1, 0, 0);
    gl_Position = transformation * (gl_in[0].gl_Position + offset);
    g_color = te_color[0];
    EmitVertex();

    g_patch_distance = te_patch_distance[1];
    g_tri_distance = vec3(0, 1, 0);
    gl_Position = transformation * (gl_in[1].gl_Position + offset);
    g_color = te_color[1];
    EmitVertex();

    g_patch_distance = te_patch_distance[2];
    g_tri_distance = vec3(0, 0, 1);
    gl_Position = transformation * (gl_in[2].gl_Position + offset);
    g_color = te_color[2];
    EmitVertex();

    EndPrimitive();
}
)====="
