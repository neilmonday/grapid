R"=====(#version 450 core
#extension GL_ARB_separate_shader_objects : enable

layout(location=0) in vec3 position;
layout(location=1) in vec3 color;

layout(location = 0) out vec3 vs_position;
layout(location = 1) out vec3 vs_color;
layout(location = 2) out int vs_instance_id;

void main()
{
	vs_instance_id = gl_InstanceIndex;
	vs_position = position;
	vs_color = color;
}
)====="
