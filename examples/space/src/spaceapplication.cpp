#include "spaceapplication.h"

SpaceApplication::SpaceApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
}

GrResult SpaceApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult SpaceApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult SpaceApplication::CreateLogicalDevice()
{
    GrResult result = GR_SUCCESS;
    float queuePriority = 1.0f;
    std::vector<uint32_t> queueFamilyIndices = GetQueueFamilyIndices();

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = queueFamilyIndices[0];
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.tessellationShader = 1;
    deviceFeatures.geometryShader = 1;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(m_requiredDeviceExtensionNames.size());
    createInfo.ppEnabledExtensionNames = m_requiredDeviceExtensionNames.data();

    VkDevice device{};
    if (vkCreateDevice(m_pInstance->GetVkPhysicalDevice(), &createInfo, nullptr, &device) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create logical device!");
    }
    else
    {
        m_pInstance->SetVkDevice(device);
    }

    return result;
}

GrResult SpaceApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult SpaceApplication::Init()
{
    GrResult result = Application::Init();

    Swapchain swapchain = GetSwapchain();

    static constexpr uint8_t vertex_shader[] =
#include "shaders/space/vertex.glsl"
        ;

    static constexpr uint8_t tess_ctrl_shader[] =
#include "shaders/space/tess_ctrl.glsl"
        ;

    static constexpr uint8_t tess_eval_shader[] =
#include "shaders/space/tess_eval.glsl"
        ;

    static constexpr uint8_t geometry_shader[] =
#include "shaders/space/geometry.glsl"
        ;

    static constexpr uint8_t fragment_shader[] =
#include "shaders/space/fragment.glsl"
        ;

    Shader vertex(GrShaderStage::GR_VERTEX, GrShaderType::GR_GLSL, vertex_shader);
    Shader tess_ctrl(GrShaderStage::GR_TESSELLATION_CONTROL, GrShaderType::GR_GLSL, tess_ctrl_shader);
    Shader tess_eval(GrShaderStage::GR_TESSELLATION_EVALUATION, GrShaderType::GR_GLSL, tess_eval_shader);
    Shader geometry(GrShaderStage::GR_GEOMETRY, GrShaderType::GR_GLSL, geometry_shader);
    Shader fragment(GrShaderStage::GR_FRAGMENT, GrShaderType::GR_GLSL, fragment_shader);

    m_program.Init(m_pInstance);
    m_program.AddShader(vertex);
    m_program.AddShader(tess_ctrl);
    m_program.AddShader(tess_eval);
    m_program.AddShader(geometry);
    m_program.AddShader(fragment);
    m_program.Compile();
    m_program.CreateShaderModules();

    m_queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

    m_renderpass.Init(m_pInstance, swapchain.GetVkFormat());

    std::vector<VkDescriptorPoolSize> poolSizes =
    {
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2 },
    };
    m_descriptorPool.Init(m_pInstance, poolSizes, 1);

    m_tessCtrlUniformBuffer.Init(m_pInstance, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    m_geomUniformBuffer.Init(m_pInstance, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);

    std::shared_ptr<TransformDescriptorSet> pTransformDescriptorSet = std::make_shared<TransformDescriptorSet>();
    pTransformDescriptorSet->Init(m_pInstance, m_descriptorPool);
    pTransformDescriptorSet->Update(m_tessCtrlUniformBuffer, m_geomUniformBuffer);

    m_descriptorSets.push_back(pTransformDescriptorSet);

    m_pipeline.Init(m_pInstance, 
        m_renderpass,
        swapchain.GetVkExtent2D(),
        &m_program, 
        VertexInput::getAttributeDescriptions(), 
        VertexInput::getBindingDescription(), 
        m_descriptorSets,
        VK_PRIMITIVE_TOPOLOGY_PATCH_LIST);
    m_pipeline.Create();

    const std::vector<Vertex> vertices = {
        {{ 0.0f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}},
        {{ 0.5f,  0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{-0.5f,  0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}}
    };

    std::shared_ptr<Buffer> pVertexBuffer = std::make_shared<Buffer>();
    pVertexBuffer->Init(m_pInstance, vertices.size() * sizeof(Vertex), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    pVertexBuffer->Update((char*)vertices.data(), 0, vertices.size() * sizeof(Vertex));

    m_vertexBuffers.push_back(pVertexBuffer);

    for (uint32_t i = 0; i < swapchain.GetImageViewCount(); i++)
    {
        Image depthImage;
        VkFormat depthFormat = m_pInstance->FindDepthFormat();
        depthImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

        //probably need to be consistent with pointers
        std::vector<VkImageView> attachments = {
            swapchain.GetVkImageView(i),
            depthImage.GetImageView()
        };
        m_depthImages.push_back(depthImage);

        Framebuffer framebuffer;
        framebuffer.Init(m_pInstance, swapchain.GetVkExtent2D(), m_renderpass.GetVkRenderPass(), attachments);
        m_framebuffers.push_back(framebuffer);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, m_queue);
        m_commandBuffers.push_back(commandBuffer);

        commandBuffer.Begin();
        m_renderpass.Begin(swapchain.GetVkExtent2D(), &framebuffer, &commandBuffer);
        m_pipeline.Bind(&commandBuffer);
        std::vector<VkDeviceSize> offsets(m_vertexBuffers.size(), 0);
        BindVertexBuffers(&commandBuffer, m_vertexBuffers, offsets);
        vkCmdDraw(commandBuffer.GetVkCommandBuffer(), vertices.size(), 1, 0, 0);
        m_renderpass.End(&commandBuffer);
        commandBuffer.End();
    }

    return result;
}

GrResult SpaceApplication::Run()
{
    GrResult result = GR_SUCCESS;

    while (!glfwWindowShouldClose(m_window))
    {
        glfwPollEvents();

        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

        uint32_t imageIndex;
        vkAcquireNextImageKHR(m_pInstance->GetVkDevice(), GetSwapchain().GetVkSwapchain(), UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);
        
        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_inFlightImages[imageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightImages[imageIndex], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        m_inFlightImages[imageIndex] = m_inFlightFences[m_currentFrame];

        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = m_commandBuffers[imageIndex].GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;

        VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame]);

        if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[m_currentFrame]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkSwapchainKHR swapChains[] = { GetSwapchain().GetVkSwapchain() };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr; // Optional

        vkQueuePresentKHR(m_queue.GetVkQueue(), &presentInfo);

        //This is lazy: vkQueueWaitIdle(queue.GetVkQueue());

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    return result;
}

GrResult SpaceApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    for (auto framebuffer : m_framebuffers)
    {
        framebuffer.Cleanup();
    }
    m_tessCtrlUniformBuffer.Cleanup();
    m_geomUniformBuffer.Cleanup();
    for (auto vertexBuffer : m_vertexBuffers)
    {
        vertexBuffer->Cleanup();
    }
    for (auto depthImage : m_depthImages)
    {
        depthImage.Cleanup();
    }
    for (auto commandBuffer : m_commandBuffers)
    {
        commandBuffer.Cleanup(m_queue);
    }
    m_pipeline.Cleanup();
    m_renderpass.Cleanup();
    for (auto descriptorSet : m_descriptorSets)
    {
        descriptorSet->Cleanup();
    }
    m_descriptorPool.Cleanup();
    m_queue.Cleanup();
    m_program.Cleanup();
    result = Application::Cleanup();

    return result;
}
