#include <spaceapplication.h>

void main()
{
    std::string name = "Space";
    SpaceApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}