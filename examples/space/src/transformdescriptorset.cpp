#include "TransformDescriptorSet.h"

TransformDescriptorSet::TransformDescriptorSet()
{
}

GrResult TransformDescriptorSet::Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool)
{
    GrResult result = DescriptorSet::Init(pInstance, descriptorPool);

    std::vector<VkDescriptorSetLayoutBinding> descriptorSetLayoutBindings{};

    VkDescriptorSetLayoutBinding tessCtrlDescriptorSetLayoutBinding{};
    tessCtrlDescriptorSetLayoutBinding.binding = 0;
    tessCtrlDescriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    tessCtrlDescriptorSetLayoutBinding.descriptorCount = 1;
    tessCtrlDescriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    tessCtrlDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;
    descriptorSetLayoutBindings.push_back(tessCtrlDescriptorSetLayoutBinding);

    VkDescriptorSetLayoutBinding geomDescriptorSetLayoutBinding{};
    geomDescriptorSetLayoutBinding.binding = 1;
    geomDescriptorSetLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    geomDescriptorSetLayoutBinding.descriptorCount = 1;
    geomDescriptorSetLayoutBinding.stageFlags = VK_SHADER_STAGE_GEOMETRY_BIT;
    geomDescriptorSetLayoutBinding.pImmutableSamplers = nullptr;
    descriptorSetLayoutBindings.push_back(geomDescriptorSetLayoutBinding);

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.flags = 0;
    descriptorSetLayoutCreateInfo.bindingCount = descriptorSetLayoutBindings.size();
    descriptorSetLayoutCreateInfo.pBindings = descriptorSetLayoutBindings.data();

    if (vkCreateDescriptorSetLayout(m_pInstance->GetVkDevice(), &descriptorSetLayoutCreateInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create descriptor set layout!");
    }

    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.descriptorPool = m_descriptorPool.GetVkDescriptorPool();
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &m_descriptorSetLayout;

    if (vkAllocateDescriptorSets(m_pInstance->GetVkDevice(), &descriptorSetAllocateInfo, &m_descriptorSet) != VK_SUCCESS)
    {
        throw std::runtime_error("failed to allocate descriptor set!");
    }

    return result;
}

GrResult TransformDescriptorSet::Cleanup()
{
    GrResult result = GR_SUCCESS;
    //vkResetDescriptorPool will free all of the DescriptorSets in the pool. 
    vkDestroyDescriptorSetLayout(m_pInstance->GetVkDevice(), m_descriptorSetLayout, nullptr);
    return result;
} 

GrResult TransformDescriptorSet::Update(Buffer tessCtrlBuffer, Buffer geomBuffer)
{
    GrResult result = GR_SUCCESS;

    VkDescriptorBufferInfo tessCtrlBufferInfo{};
    tessCtrlBufferInfo.buffer = tessCtrlBuffer.GetVkBuffer();
    tessCtrlBufferInfo.offset = 0;
    tessCtrlBufferInfo.range = tessCtrlBuffer.GetVkSize();

    VkWriteDescriptorSet tessCtrlWriteDescriptorSet{};
    tessCtrlWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    tessCtrlWriteDescriptorSet.dstSet = m_descriptorSet;
    tessCtrlWriteDescriptorSet.dstBinding = 0;
    tessCtrlWriteDescriptorSet.dstArrayElement = 0;
    tessCtrlWriteDescriptorSet.descriptorCount = 1;
    tessCtrlWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    tessCtrlWriteDescriptorSet.pImageInfo = nullptr; // Optional
    tessCtrlWriteDescriptorSet.pBufferInfo = &tessCtrlBufferInfo;
    tessCtrlWriteDescriptorSet.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(m_pInstance->GetVkDevice(), 1, &tessCtrlWriteDescriptorSet, 0, nullptr);

    VkDescriptorBufferInfo geomBufferInfo{};
    geomBufferInfo.buffer = tessCtrlBuffer.GetVkBuffer();
    geomBufferInfo.offset = 0;
    geomBufferInfo.range = tessCtrlBuffer.GetVkSize();

    VkWriteDescriptorSet geomWriteDescriptorSet{};
    geomWriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    geomWriteDescriptorSet.dstSet = m_descriptorSet;
    geomWriteDescriptorSet.dstBinding = 0;
    geomWriteDescriptorSet.dstArrayElement = 0;
    geomWriteDescriptorSet.descriptorCount = 1;
    geomWriteDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    geomWriteDescriptorSet.pImageInfo = nullptr; // Optional
    geomWriteDescriptorSet.pBufferInfo = &tessCtrlBufferInfo;
    geomWriteDescriptorSet.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(m_pInstance->GetVkDevice(), 1, &geomWriteDescriptorSet, 0, nullptr);
    return result;
}
