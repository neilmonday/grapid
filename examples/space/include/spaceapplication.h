#ifndef SPACEAPPLICATION_H
#define SPACEAPPLICATION_H

#include <application.h>

#include <shader.h>
#include <program.h>
#include <graphicspipeline.h>
#include <queue.h>
#include <renderpass.h>
#include <commandbuffer.h>
#include <framebuffer.h>
#include <camera.h>

#include <glm/glm.hpp>

#include "vertexinput.h"
#include "transformdescriptorset.h"

class SpaceApplication : public Application
{
public:
    SpaceApplication(std::string name);

    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult CreateLogicalDevice() override;
    GrResult LoadVolkDevice() override;

private:
    std::unique_ptr<Camera> m_pCamera{};

    Buffer m_tessCtrlUniformBuffer{};
    Buffer m_geomUniformBuffer{};

    //UniformBufferObject m_tessCtrlUniformBufferObject;
    //UniformBufferObject m_geomUniformBufferObject;

    std::vector<CommandBuffer> m_commandBuffers;
    std::vector<Framebuffer> m_framebuffers;
    Program m_program;
    std::vector<std::shared_ptr<DescriptorSet>> m_descriptorSets;
    std::vector<std::shared_ptr<Buffer> > m_vertexBuffers{};
    GraphicsPipeline m_pipeline;
    Renderpass m_renderpass;
    Queue m_queue;
    std::vector<Image> m_depthImages{};
    DescriptorPool m_descriptorPool;
};

#endif //SPACEAPPLICATION_H
