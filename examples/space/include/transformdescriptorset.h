#ifndef TRANSFORMDESCRIPTORSET_H
#define TRANSFORMDESCRIPTORSET_H

#include <descriptorset.h>
#include <buffer.h>

class TransformDescriptorSet : public DescriptorSet
{
public:
    TransformDescriptorSet();

    GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool) override;
    GrResult Cleanup() override;

    GrResult Update(Buffer tessCtrlBuffer, Buffer geomBuffer);
};

#endif //TRANSFORMDESCRIPTORSET_H
