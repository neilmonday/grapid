#ifndef VERTEXINPUT_H
#define VERTEXINPUT_H

#include <vector>
#include <memory>

#include <volk.h>

#include <glm/glm.hpp>

#include <export.h>
#include <types.h>

struct Vertex
{
    glm::vec3 position;
    glm::vec3 color;
};

struct VertexInput
{
    static std::shared_ptr<std::vector<VkVertexInputBindingDescription> > getBindingDescription();
    static std::shared_ptr<std::vector<VkVertexInputAttributeDescription> > getAttributeDescriptions();
};

#endif //VERTEXINPUT_H
