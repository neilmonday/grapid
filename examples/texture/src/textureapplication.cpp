#include "TextureApplication.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

TextureApplication::TextureApplication(std::string name) :
    Application(name, VK_API_VERSION_1_3)
{
    m_windowSize = { 2560, 1440 };
}

GrResult TextureApplication::InitDependencies()
{
    GrResult result = Application::InitDependencies();

    if (!glfwInit())
    {
        result = GR_ERROR;
        throw std::runtime_error("glfwInit has failed!");
    }

    result = VkResultToGrResult(volkInitialize());

    if (result != GR_SUCCESS)
    {
        result = GR_ERROR;
        throw std::runtime_error("volkInitialize has failed!");
    }
    return result;
}

GrResult TextureApplication::LoadVolkInstance()
{
    GrResult result = Application::LoadVolkInstance();
    volkLoadInstance(m_pInstance->GetVkInstance());
    return result;
}

GrResult TextureApplication::LoadVolkDevice()
{
    GrResult result = Application::LoadVolkDevice();
    volkLoadDevice(m_pInstance->GetVkDevice());
    return result;
}

GrResult TextureApplication::Init()
{
    GrResult result = Application::Init();

    Swapchain swapchain = GetSwapchain();

    static constexpr uint8_t vertex_shader[] =
#include "shaders/texture/vertex.glsl"
        ;

    std::vector<uint8_t> vertex_shader_code(
        vertex_shader, vertex_shader + sizeof(vertex_shader) - 1);

    static constexpr uint8_t fragment_shader[] =
#include "shaders/texture/fragment.glsl"
        ;

    std::vector<uint8_t> fragment_shader_code(
        fragment_shader, fragment_shader + sizeof(fragment_shader) - 1);

    Shader vertex(GrShaderStage::GR_VERTEX, GrShaderType::GR_GLSL, vertex_shader_code);
    Shader fragment(GrShaderStage::GR_FRAGMENT, GrShaderType::GR_GLSL, fragment_shader_code);

    m_program.Init(m_pInstance);
    m_program.AddShader(vertex);
    m_program.AddShader(fragment);
    m_program.Compile();
    m_program.CreateShaderModules();

    int width = 0, height = 0, data_size = 0;
    std::vector<char> imageData = GetImageData("../../../../data/big_grid.png", 4, &width, &height, nullptr, &data_size);

    m_image.Init(m_pInstance, width, height, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_IMAGE_ASPECT_COLOR_BIT);
    m_image.CreateStagingBuffer(data_size);

    std::vector<VkDescriptorPoolSize> poolSizes =
    {
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 }
    };
    m_descriptorPool.Init(m_pInstance, poolSizes, 1);

    std::shared_ptr<TextureDescriptorSet> pImageDescriptorSet = std::make_shared<TextureDescriptorSet>();
    pImageDescriptorSet->Init(m_pInstance, m_descriptorPool);
    pImageDescriptorSet->Update(m_image);

    m_descriptorSets.push_back(pImageDescriptorSet);

    m_renderpass.Init(m_pInstance, swapchain.GetVkFormat());

    m_pipeline.Init(m_pInstance,
        m_renderpass,
        swapchain.GetVkExtent2D(),
        &m_program,
        VertexInput::getAttributeDescriptions(),
        VertexInput::getBindingDescription(),
        m_descriptorSets,
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);

    m_pipeline.Create();

    std::vector<CommandBuffer> commandBuffers;
    std::vector<Framebuffer> framebuffers;

    m_queue.Init(m_pInstance, GetQueueFamilyIndices()[0]);

    m_image.Upload(m_queue, imageData.data());

    //vertex, texture
    std::vector<Vertex> vertices = {
        {{-0.7f, -0.7f, 0.0f}, {0.0f, 0.0f}},
        {{ 0.7f, -0.7f, 0.0f}, {1.0f, 0.0f}},
        {{ 0.7f,  0.7f, 0.0f}, {1.0f, 1.0f}},

        {{-0.7f, -0.7f, 0.0f}, {0.0f, 0.0f}},
        {{ 0.7f,  0.7f, 0.0f}, {1.0f, 1.0f}},
        {{-0.7f,  0.7f, 0.0f}, {0.0f, 1.0f}}
    };

    std::shared_ptr<Buffer> pVertexPositionBuffer = std::make_shared<Buffer>();
    pVertexPositionBuffer->Init(m_pInstance, vertices.size() * sizeof(Vertex), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_SHARING_MODE_EXCLUSIVE);
    pVertexPositionBuffer->Update((char*)vertices.data(), 0, vertices.size() * sizeof(Vertex));
    m_vertexBuffers.push_back(pVertexPositionBuffer);

    for (uint32_t i = 0; i < swapchain.GetImageViewCount(); i++)
    {
        //I should modify the renderpass and framebuffer to be able to accept no depth
        Image depthImage;
        VkFormat depthFormat = m_pInstance->FindDepthFormat();
        depthImage.Init(m_pInstance, m_windowSize.width, m_windowSize.height, depthFormat, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

        //probably need to be consistent with pointers
        std::vector<VkImageView> attachments = {
            swapchain.GetVkImageView(i),
            depthImage.GetImageView()
        };
        m_depthImages.push_back(depthImage);

        Framebuffer framebuffer;
        framebuffer.Init(m_pInstance, swapchain.GetVkExtent2D(), m_renderpass.GetVkRenderPass(), attachments);
        m_framebuffers.push_back(framebuffer);

        CommandBuffer commandBuffer;
        commandBuffer.Init(m_pInstance, m_queue);
        m_commandBuffers.push_back(commandBuffer);

        commandBuffer.Begin();
        m_renderpass.Begin(swapchain.GetVkExtent2D(), &framebuffer, &commandBuffer);
        m_pipeline.Bind(&commandBuffer);
        std::vector<VkDeviceSize> offsets(m_vertexBuffers.size(), 0);
        BindVertexBuffers(&commandBuffer, m_vertexBuffers, offsets);
        std::vector<VkDescriptorSet> vkDescriptorSets;
        for (auto descriptorSet : m_descriptorSets)
        {
            vkDescriptorSets.push_back(descriptorSet->GetVkDescriptorSet());
        }
        vkCmdBindDescriptorSets(commandBuffer.GetVkCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline.GetVkPipelineLayout(), 0, 1, vkDescriptorSets.data(), 0, nullptr);
        vkCmdDraw(commandBuffer.GetVkCommandBuffer(), vertices.size(), 1, 0, 0);
        m_renderpass.End(&commandBuffer);
        commandBuffer.End();
    }

    return result;
}

GrResult TextureApplication::Run()
{
    GrResult result = GR_SUCCESS;

    glm::vec3 position = { 0.0f, 0.0f, 10.0f };
    glm::vec3 up = { 0.0f, 1.0f, 0.0f };

    while (!glfwWindowShouldClose(m_window))
    {
        int width = 0, height = 0;
        glfwGetWindowSize(m_window, &width, &height);

        glfwPollEvents();

        vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

        uint32_t imageIndex;
        vkAcquireNextImageKHR(m_pInstance->GetVkDevice(), GetSwapchain().GetVkSwapchain(), UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);
        
        // Check if a previous frame is using this image (i.e. there is its fence to wait on)
        if (m_inFlightImages[imageIndex] != VK_NULL_HANDLE)
        {
            vkWaitForFences(m_pInstance->GetVkDevice(), 1, &m_inFlightImages[imageIndex], VK_TRUE, UINT64_MAX);
        }
        // Mark the image as now being in use by this frame
        m_inFlightImages[imageIndex] = m_inFlightFences[m_currentFrame];

        glm::vec4 forward = { 0.0f, 0.0f, 1.0f, 1.0f };
        glm::vec3 forward3 = { 0.0f, 0.0f, 1.0f };
        glm::vec3 right = glm::cross(up, forward3);
        glm::vec2 rotation = CalculateRotation(m_window);
        glm::mat4 rotation_matrix = glm::rotate(glm::identity<glm::mat4>(), rotation.x, up);
        rotation_matrix = glm::rotate(rotation_matrix, rotation.y, right);
        forward = rotation_matrix * forward;

        CalculateTranslation(m_window, rotation, &position);

        //0.785398 radians == 45 degrees.
        //1.5708 radians == 90 degrees.
        glm::mat4 projection = glm::perspective(1.5708f, (float)width / (float)height, 0.5f, 10000.0f);
        glm::vec3 position_plus_forward = { position.x + forward.x, position.y + forward.y, position.z + forward.z };
        glm::vec3 position_minus_forward = { position.x - forward.x, position.y - forward.y, position.z - forward.z };
        glm::mat4 view = glm::lookAt(position, position_plus_forward, up);
        glm::mat4 model = glm::identity<glm::mat4>();
        glm::mat4 model_view = model * view;
        glm::mat4 model_view_projection = model_view * projection;
        //FIX glUniformMatrix4fv(transformation_handle, 1, GL_FALSE, &model_view_projection);


        VkSubmitInfo submitInfo{};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        submitInfo.commandBufferCount = 1;
        VkCommandBuffer vkCommandBuffer = m_commandBuffers[imageIndex].GetVkCommandBuffer();
        submitInfo.pCommandBuffers = &vkCommandBuffer;

        VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;

        vkResetFences(m_pInstance->GetVkDevice(), 1, &m_inFlightFences[m_currentFrame]);

        if (vkQueueSubmit(m_queue.GetVkQueue(), 1, &submitInfo, m_inFlightFences[m_currentFrame]) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkSwapchainKHR swapChains[] = { GetSwapchain().GetVkSwapchain() };
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = swapChains;
        presentInfo.pImageIndices = &imageIndex;
        presentInfo.pResults = nullptr; // Optional

        vkQueuePresentKHR(m_queue.GetVkQueue(), &presentInfo);

        //This is lazy: vkQueueWaitIdle(queue.GetVkQueue());

        glfwSwapBuffers(m_window);
        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
        
        std::vector<char> textureData;
        textureData.resize(m_image.GetSize());
        m_image.Download(m_queue, textureData.data());
        int i = 0;
    }

    return result;
}

GrResult TextureApplication::Cleanup()
{
    GrResult result = GR_SUCCESS;

    vkDeviceWaitIdle(m_pInstance->GetVkDevice());

    for (auto framebuffer : m_framebuffers)
    {
        framebuffer.Cleanup();
    }
    for (auto vertexBuffer : m_vertexBuffers)
    {
        vertexBuffer->Cleanup();
    }
    for (auto depthImage : m_depthImages)
    {
        depthImage.Cleanup();
    }
    for (auto commandBuffer : m_commandBuffers)
    {
        commandBuffer.Cleanup(m_queue);
    }
    m_pipeline.Cleanup();
    m_renderpass.Cleanup();
    for (auto descriptorSet : m_descriptorSets)
    {
        descriptorSet->Cleanup();
    }
    m_descriptorPool.Cleanup();
    m_image.Cleanup();
    m_queue.Cleanup();
    m_program.Cleanup();
    result = Application::Cleanup();
    return result;
}

glm::vec2 TextureApplication::CalculateRotation(GLFWwindow* window)
{
    glm::dvec2 cursor = { 0.0, 0.0 };
    glm::vec2 rotation = { 0.0f, 0.0f };

    glfwGetCursorPos(window, &(cursor.x), &(cursor.y));

    rotation.y = cursor.x / 300.0;
    rotation.x = -cursor.y / 300.0;

    if (rotation.x > 1.570)
        rotation.x = 1.570;
    if (rotation.x < -1.570)
        rotation.x = -1.570;
    /*printf("*****************\n");
    printf("Mouse Up:\n");
    printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
    printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
    printf("*****************\n");*/

    return rotation;
}

GrResult TextureApplication::CalculateTranslation(GLFWwindow* window, const glm::vec2 rotation, glm::vec3* position)
{
    GrResult result = GR_SUCCESS;

    GLdouble sensitivity = 0.02;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        position->x -= sensitivity * sin(rotation.y) * cos(rotation.x);
        position->y -= sensitivity * 1 * -sin(rotation.x);
        position->z -= sensitivity * -cos(rotation.y) * cos(rotation.x);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        position->x += sensitivity * cos(rotation.y);
        //*y -=;
        position->z += sensitivity * sin(rotation.y);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        position->x += sensitivity * sin(rotation.y) * cos(rotation.x);
        position->y += sensitivity * 1 * -sin(rotation.x);
        position->z += sensitivity * -cos(rotation.y) * cos(rotation.x);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        position->x -= sensitivity * cos(rotation.y);
        //*y += ;
        position->z -= sensitivity * sin(rotation.y);
    }
    return result;
}

// ************************************************************ //
// GetBinaryFileContents                                        //
//                                                              //
// Function reading binary contents of a file                   //
// ************************************************************ //
std::vector<char> GetBinaryFileContents(std::string const& filename) {

    std::ifstream file(filename, std::ios::binary);
    if (file.fail()) {
        std::cout << "Could not open \"" << filename << "\" file!" << std::endl;
        return std::vector<char>();
    }

    std::streampos begin, end;
    begin = file.tellg();
    file.seekg(0, std::ios::end);
    end = file.tellg();

    std::vector<char> result(static_cast<size_t>(end - begin));
    file.seekg(0, std::ios::beg);
    file.read(&result[0], end - begin);
    file.close();

    return result;
}

// ************************************************************ //
// GetImageData                                                 //
//                                                              //
// Function loading image (texture) data from a specified file  //
// ************************************************************ //
std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size) {
    std::vector<char> file_data = GetBinaryFileContents(filename);
    if (file_data.size() == 0) {
        return std::vector<char>();
    }

    int tmp_width = 0, tmp_height = 0, tmp_components = 0;
    unsigned char* image_data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(&file_data[0]), static_cast<int>(file_data.size()), &tmp_width, &tmp_height, &tmp_components, requested_components);
    if ((image_data == nullptr) ||
        (tmp_width <= 0) ||
        (tmp_height <= 0) ||
        (tmp_components <= 0)) {
        std::cout << "Could not read image data!" << std::endl;
        return std::vector<char>();
    }

    int size = (tmp_width) * (tmp_height) * (requested_components <= 0 ? tmp_components : requested_components);
    if (data_size) {
        *data_size = size;
    }
    if (width) {
        *width = tmp_width;
    }
    if (height) {
        *height = tmp_height;
    }
    if (components) {
        *components = tmp_components;
    }

    std::vector<char> output(size);
    memcpy(&output[0], image_data, size);

    stbi_image_free(image_data);
    return output;
}