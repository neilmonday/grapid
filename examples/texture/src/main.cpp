#include <textureapplication.h>

void main()
{
    std::string name = "Texture";
    TextureApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}