#include "vertex.h"

VkVertexInputBindingDescription Vertex::getBindingDescription()
{
    VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(Vertex);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    return bindingDescription;
}

std::vector<VkVertexInputAttributeDescription> Vertex::getAttributeDescriptions()
{
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};

    VkVertexInputAttributeDescription positionDescription;
    positionDescription.binding = 0;
    positionDescription.location = 0;
    positionDescription.format = VK_FORMAT_R32G32_SFLOAT;
    positionDescription.offset = offsetof(Vertex, position);
    attributeDescriptions.push_back(positionDescription);

    VkVertexInputAttributeDescription colorDescription;
    colorDescription.binding = 0;
    colorDescription.location = 1;
    colorDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    colorDescription.offset = offsetof(Vertex, color);
    attributeDescriptions.push_back(positionDescription);

    return attributeDescriptions;
}
