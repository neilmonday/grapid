#ifndef TEXTUREAPPLICATION_H
#define TEXTUREAPPLICATION_H

#include <fstream>

#include <application.h>
#include <shader.h>
#include <program.h>
#include <graphicspipeline.h>
#include <queue.h>
#include <renderpass.h>
#include <commandbuffer.h>
#include <framebuffer.h>
#include <glm/glm.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "stb_image.h"

#include "vertexinput.h"
#include "texturedescriptorset.h"

std::vector<char> GetBinaryFileContents(std::string const& filename);
std::vector<char> GetImageData(std::string const& filename, int requested_components, int* width, int* height, int* components, int* data_size);

class TextureApplication : public Application
{
public:
    TextureApplication(std::string name);
    GrResult Init() override;
    GrResult Run() override;
    GrResult Cleanup() override;

protected:
    GrResult InitDependencies() override;
    GrResult LoadVolkInstance() override;
    GrResult LoadVolkDevice() override;

private:
    glm::vec2 CalculateRotation(GLFWwindow* window);
    GrResult CalculateTranslation(GLFWwindow* window, const glm::vec2 rotation, glm::vec3* position);

    std::vector<CommandBuffer> m_commandBuffers;
    std::vector<Framebuffer> m_framebuffers;
    Program m_program;
    GraphicsPipeline m_pipeline;
    Image m_image;
    Renderpass m_renderpass;
    DescriptorPool m_descriptorPool;
    std::vector<std::shared_ptr<DescriptorSet>> m_descriptorSets;
    Queue m_queue;

    std::vector<std::shared_ptr<Buffer> > m_vertexBuffers{};
    std::vector<Image> m_depthImages{};
};

#endif //TEXTUREAPPLICATION_H
