#ifndef TEXTUREDESCRIPTORSET_H
#define TEXTUREDESCRIPTORSET_H

#include <descriptorset.h>

class TextureDescriptorSet : public DescriptorSet
{
public:
    TextureDescriptorSet();

    GrResult Init(std::shared_ptr<Instance> pInstance, const DescriptorPool& descriptorPool) override;
    GrResult Cleanup() override;

    GrResult Update(Image image);
};

#endif //TEXTUREDESCRIPTORSET_H
