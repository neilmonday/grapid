#include <string>

#include <rebarapplication.h>

void main()
{
    std::string name = "ReBAR";
    ReBARApplication app(name);
    app.Init();
    app.Run();
    app.Cleanup();

    return;
}