1. git clone
2. cd into dir
3. git submodule init
4. git submodule update
5. Run CMake in the base dir, and set the output dir to 'build/vs17-x64' (loading the mesh might fail if the depth of this output dir is different)
6. Configure, Generate, and Open the solution (I've only tested recently with Visual Studio)
7. In Visual Studio, set the Examples/Car project as the startup project
8. Build and Run